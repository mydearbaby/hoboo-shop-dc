# 框架使用指南

> 这是一个框架使用的基础信息介绍文档

## 框架目录结构

```markdown
|-- Project
    |-- Guide.framework.md
    |-- README.md
    |-- babel.config.js
    |-- package.json
    |-- vue.config.js
    |-- yarn.lock
    |-- public
    |   |-- index.html （入口页面）
    |-- src（开发目录）
        |-- App.vue （内置封装：导航菜单）
        |-- actions.js
        |-- axios.js
        |-- dialogs.js （内置封装：弹窗规则）
        |-- layouts.js （内置封装：布局规则）
        |-- main.js（入口js文件）
        |-- services.js（内置封装：业务规则）
        |-- store.js（内置封装：状态规则）
        |-- widgets.js（内置封装：组件规则）
        |-- actions （事件目录）
        |   |-- filter.js
        |   |-- print.js
        |   |-- service.js
        |   |-- set.js
        |   |-- dialog
        |   |   |-- show.js
        |   |-- form
        |   |   |-- reset.js
        |   |   |-- value.js
        |   |-- grid
        |       |-- reload.js
        |-- components （内置：框架公共组件封装）
        |   |-- View.js
        |   |-- View.vue
        |   |-- Widget.js
        |   |-- Widget.vue
        |   |-- View
        |   |   |-- Component.js
        |   |   |-- Normal.vue
        |   |   |-- Router.vue
        |   |-- Widget
        |   |   |-- Component.js
        |   |-- app
        |   |   |-- Dialog.vue
        |   |   |-- Header.vue
        |   |   |-- Menu.vue
        |   |   |-- Header
        |   |       |-- button
        |   |           |-- Exit.vue
        |   |           |-- Home.vue
        |   |-- router
        |       |-- view
        |           |-- Child.vue
        |-- dialogs
        |   |-- grid
        |       |-- item
        |           |-- Detail.view.js
        |-- layouts （布局）
        |   |-- Auto.vue
        |   |-- Fit.vue
        |   |-- Flex.vue
        |   |-- ThreeColumns.vue
        |   |-- flex
        |       |-- Vertical.vue
        |-- router （前端路由）
        |   |-- index.js
        |   |-- routes.js
        |-- services （业务数据目录）
        |   |-- dictionary （内置：缓存数据映射规则）
        |   |   |-- division.js
        |   |-- grid
        |   |   |-- normal
        |   |       |-- list.js
        |   |       |-- list
        |   |           |-- params.js
        |   |-- list
        |   |   |-- river.js
        |   |-- yps
        |       |-- list.js
        |       |-- region
        |           |-- list.js
        |-- views （页面目录）
        |   |-- Form.view.js
        |   |-- FormInputNumber.view.js
        |   |-- Grid.vue
        |   |-- GridAndForm.view.js
        |   |-- Index.module.scss
        |   |-- Index.view.js
        |   |-- Login.vue
        |   |-- OneColumns.view.js
        |   |-- ThreeColumns.module.scss
        |   |-- ThreeLevel.vue
        |   |-- TwoColumns.view.js
        |   |-- Grid
        |   |   |-- Normal.view.js
        |   |   |-- Pagination.view.js
        |   |-- ThreeLevel
        |   |   |-- A.vue
        |   |   |-- B.vue
        |   |   |-- A
        |   |       |-- 1.vue
        |   |       |-- 2.vue
        |   |       |-- 3.vue
        |   |-- TwoLevel
        |       |-- A.vue
        |       |-- B.vue
        |-- widgets （基础组件目录）
            |-- Button.vue
            |-- CheckboxGroup.vue
            |-- Container.vue
            |-- Form.vue
            |-- Grid.vue
            |-- InputNumber.vue
            |-- RadioGroup.vue
            |-- Search.vue
            |-- Select.vue
            |-- Switch.vue
            |-- Text.vue
```
