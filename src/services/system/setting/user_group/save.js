export const OPERATION = 'SUBMIT';

export default async (params,{
    
    axios
}) => await axios({
    url: '/userGroup/setItem',
    method: 'post',
    params
});