export const OPERATION = 'SUBMIT' ;

export default async ({
    id
} , {
    axios
}) => {

    await axios({
        url:'/userGroup/delItem',
        method:'post',
        params:{
            id
        }
    }) ;

} ;
