export const OPERATION = 'SUBMIT' ;

export default async ({
    ids
} , {
    axios
}) => {

    await axios({
        url:'/userGroup/delItem',
        method:'post',
        params:{
            ids
        }
    }) ;

} ;
