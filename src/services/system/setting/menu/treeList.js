export default (_ , {
    axios
}) => axios({
    url:'systemMenus/getMenuList'
}) ;

export const LOAD_DATA_CACHE_MODE = 'CACHE' ;

export const LOAD_DATA_LIST_TYPE = {
    id:'id'
} ;