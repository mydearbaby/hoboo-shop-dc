export const OPERATION = 'SUBMIT';

export default async ({
    id,
    pid,
    name,
    icon,
    sort,
    is_show,
    path,
    is_head,
    menu_name,
    module,
}, {
    axios
}) => {

    await axios({
        url: '/systemMenus/setItem',
        method: 'post',
        params: {
            id,
            pid,
            name,
            icon,
            sort,
            is_show,
            path,
            is_head,
            menu_name,
            module,
        }
    });

};