export const OPERATION = 'SUBMIT' ;

export default async ({
    id
} , {
    axios
}) => {

    await axios({
        url:'/systemMenus/delItem',
        method:'post',
        params:{
            id
        }
    }) ;

} ;