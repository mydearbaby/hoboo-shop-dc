export const OPERATION = 'SUBMIT' ;

export default async ({
    id
} , {
    axios
}) => {

    await axios({
        url:'/UserRole/delItem',
        method:'post',
        params:{
            id
        }
    }) ;

} ;
