export const OPERATION = 'SUBMIT' ;

export default async ({
    uid
} , {
    axios
}) => {

    await axios({
        url:'/User/delItem',
        method:'post',
        params:{
            uid
        }
    }) ;

} ;
