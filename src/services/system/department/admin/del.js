export const OPERATION = 'SUBMIT' ;

export default async ({
    id
} , {
    axios
}) => {

    await axios({
        url:'/SystemAdmin/delItem',
        method:'post',
        params:{
            id
        }
    }) ;

} ;
