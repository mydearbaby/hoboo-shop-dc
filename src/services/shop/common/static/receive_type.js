
export default {
    data: [
        {
            'id': 1,
            'value': 1,
            'text': '手动领取',
        },{
            'id': 2,
            'value': 2,
            'text': '新人券',
        },{
            'id': 3,
            'value': 3,
            'text': '赠送券',
        },{
            'id': 4,
            'value': 4,
            'text': '会员券',
        }
    ]
};
