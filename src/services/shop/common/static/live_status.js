
export default {
    data: [
        {
            'id': 101,
            'value': 101,
            'text': '直播中',
        },{
            'id': 102,
            'value': 102,
            'text': '未开始',
        },{
            'id': 103,
            'value': 103,
            'text': '已结束',
        },{
            'id': 104,
            'value': 104,
            'text': '禁播',
        },{
            'id': 105,
            'value': 105,
            'text': '暂停',
        },{
            'id': 106,
            'value': 106,
            'text': '异常',
        },{
            'id': 107,
            'value': 107,
            'text': '已过期',
        }
    ]
};
