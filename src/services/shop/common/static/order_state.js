export default {
    data: [
        {
            'id': 0,
            'value': -1,
            'text': '申请退款',
        }, {
            'id': 1,
            'value': -2,
            'text': '退货成功',
        },{
            'id': 2,
            'value': 0,
            'text': '待发货',
        },{
            'id': 3,
            'value': 1,
            'text': '待收货',
        },{
            'id': 4,
            'value': 2,
            'text': '已收货',
        },{
            'id': 5,
            'value': 3,
            'text': '待评价',
        }

    ]
};