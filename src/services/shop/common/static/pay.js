
export default {
    data: [
        {
            'id': 1,
            'value': 1,
            'text': '全部',
        },{
            'id': 2,
            'value': 2,
            'text': '微信支付',
        },{
            'id': 3,
            'value': 3,
            'text': '支付宝支付',
        },{
            'id': 4,
            'value': 4,
            'text': '余额支付',
        },{
            'id': 5,
            'value': 5,
            'text': '线下支付',
        }
    ]
};
