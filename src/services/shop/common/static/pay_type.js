
export default {
    data: [
        {
            'id': 0,
            'value': "yue",
            'text': '余额支付',
        },{
            'id': 1,
            'value': "xianxia",
            'text': '线下支付',
        },{
            'id': 2,
            'value': "wechat",
            'text': '微信支付',
        },{
            'id': 3,
            'value': "alipay",
            'text': '支付宝支付',
        }
    ]
};
