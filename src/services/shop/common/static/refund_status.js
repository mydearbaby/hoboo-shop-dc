
export default {
    data: [
        {
            'id': 0,
            'value': 0,
            'text': '未退款',
        },{
            'id': 1,
            'value': 1,
            'text': '申请中',
        },{
            'id': 2,
            'value': 2,
            'text': '已退款',
        }
    ]
};
