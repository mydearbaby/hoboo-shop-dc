
export default {
    data: [
        {
            'id': 0,
            'value': 0,
            'text': '未审核',
        },{
            'id': 1,
            'value': 1,
            'text': '审核中',
        },{
            'id': 2,
            'value': 2,
            'text': '审核通过',
        },{
            'id': 3,
            'value': 3,
            'text': '审核失败）',
        }
    ]
};
