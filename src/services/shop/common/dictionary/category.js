export default {
    url:'StoreCategory/getList',
    fields:[{
        name:'value',
        mapping:'id'
    },{
        name:'text',
        mapping:'cate_name'
    }]
} ;
