export default {
    url:'StoreBrand/getList',
    fields:[{
        name:'value',
        mapping:'id'
    },{
        name:'text',
        mapping:'name'
    }]
} ;
