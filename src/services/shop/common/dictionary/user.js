export default {
    url:'user/getList',
    fields:[{
        name:'value',
        mapping:'id'
    },{
        name:'text',
        mapping:'nickname'
    }]
} ;
