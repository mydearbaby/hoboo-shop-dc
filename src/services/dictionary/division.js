export default (_ , {
    axios
}) => axios({
    url:'division/method/get.list'
}) ;

export const LOAD_DATA_CACHE_MODE = 'CACHE' ;

export const LOAD_DATA_TYPE = 'DICTIONARY' ;

export const LOAD_DATA_DICTIONARY_TYPE = {
    value:'division_id',
    text:'name'
} ;