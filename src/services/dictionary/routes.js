import {
    ALL_ROUTES
} from 'hb-full-cli' ;

const pathPrefixRe = /^\/modules/ ;

function processRoutes(routes , paths){

    return routes.map(({
        title,
        fullPath:path,
        children = []
    }) => {

        let route = {
            title,
            path:path.replace(pathPrefixRe , ''),
            children:processRoutes(children , paths)
        } ;

        if(paths.hasOwnProperty(path)){

            Object.assign(route , paths[path]) ;
        
        }

        return route ;

    }) ;
}

export default {
    async fn(_ , {
        axios
    }){
    
        return processRoutes(ALL_ROUTES() , getPaths(await axios({
            url:'systemMenus/getList'
        }))) ;
    
    },
    cache:'view'
} ;

function getPaths(data){

    let paths = {} ;

    data.forEach(({
        path,
        id,
        is_show,
        is_header,
        module
    }) => paths[`/modules${path}`] = {
        id,
        path,
        is_header,
        is_show,
        module
    }) ;

    return paths ;
}