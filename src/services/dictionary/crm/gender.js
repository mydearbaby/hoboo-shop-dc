export default () => [{
    "gender": 0,
    "name": "未知",
},{
    "gender": 1,
    "name": "男",
},{
    "gender": 2,
    "name": "女",
}]
;

export const LOAD_DATA_CACHE_MODE = 'CACHE' ;

export const LOAD_DATA_TYPE = 'DICTIONARY' ;

export const LOAD_DATA_DICTIONARY_TYPE = {
value:'gender',
text:'name'
} ;