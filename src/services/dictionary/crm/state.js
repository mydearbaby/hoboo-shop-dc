export default () => [{
    "state": 1,
    "name": "在售",
},{
    "state": 0,
    "name": "禁售",
}]
;

export const LOAD_DATA_CACHE_MODE = 'CACHE' ;

export const LOAD_DATA_TYPE = 'DICTIONARY' ;

export const LOAD_DATA_DICTIONARY_TYPE = {
value:'state',
text:'name'
} ;