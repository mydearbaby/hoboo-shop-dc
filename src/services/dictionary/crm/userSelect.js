export default async (params , {
    axios,
}) => (await axios({
    url:'/User/getList',
    method:'post',
    params
})).items ;

export const LOAD_DATA_TYPE = 'DICTIONARY' ;

export const LOAD_DATA_DICTIONARY_TYPE = {
    value:'uid',
    text:'real_name'
} ;