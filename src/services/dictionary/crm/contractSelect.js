export default async (params , {
    axios,
}) => (await axios({
    url:'/CrmCustomer/getList',
    method:'post',
    params
})).items ;

export const LOAD_DATA_TYPE = 'DICTIONARY' ;

export const LOAD_DATA_DICTIONARY_TYPE = {
    value:'uuid',
    text:'name'
} ;