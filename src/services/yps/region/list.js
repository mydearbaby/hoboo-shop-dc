import axios from 'axios';

export default async () => (await axios.get('https://hb.demo.hobooa.com/api/v2/division/method/get.list/?token=568c7b726479eb2ab4e1de71dd3ec37e&scope=scope1')).data.data.items.map(({
    division_id:value,
    name:text
}) => ({
    value,
    text
})) ;