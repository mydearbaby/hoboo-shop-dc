export const OPERATION = 'SUBMIT' ;

export default async ({
    water_user_maintenance_id,
    division
} , {
    axios
}) => {

    await axios({
        url:'water_user_maintenance/method/set.Item',
        method:'post',
        params:{
            water_user_maintenance_id,
            division_id:division.value
        }
    }) ;

} ;