export const OPERATION = 'SUBMIT' ;

export default async ({
    id
} , {
    axios
}) => {

    await axios({
        url:'/user/delItem',
        method:'post',
        params:{
            id
        }
    }) ;

} ;
