export const OPERATION = 'SUBMIT' ;

export default async ({
    uid,
    nickname,
    password,
    real_name,
    birthday,
    card_id,
    phone,
    address,
    record_phone,
} , {
    axios
}) => {

    await axios({
        url:'/user/setItem',
        method:'post',
        params:{
            uid,
            nickname,
            password,
            real_name,
            birthday,
            card_id,
            phone,
            address,
            record_phone,
        }
    }) ;

} ;
