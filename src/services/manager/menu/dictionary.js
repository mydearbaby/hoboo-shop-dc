export default {
    url:'systemMenus/getList',
    fields:[{
        name:'value',
        mapping:'id'
    },{
        name:'text',
        mapping:'name'
    }]
} ;