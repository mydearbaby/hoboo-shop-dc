export const OPERATION = 'SUBMIT' ;

export default async ({
    id,
} , {
    axios
}) => {

    await axios({
        url:'/EimsTurnoverArea/delItem',
        method:'post',
        params:{
            id,
        }
    }) ;

} ;
