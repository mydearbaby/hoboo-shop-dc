export const OPERATION = 'SUBMIT' ;

export default async ({
    id,
} , {
    axios
}) => {

    await axios({
        url:'/EimsWarehouse/delItem',
        method:'post',
        params:{
            id,
        }
    }) ;

} ;
