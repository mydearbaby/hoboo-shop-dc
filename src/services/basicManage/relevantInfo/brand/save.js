export const OPERATION = 'SUBMIT' ;

export default async ({
    values,
    module,
} , {
    axios
}) => {

    await axios({
        url:'/EimsConfig/setItem',
        method:'post',
        params:{
            ...values,
            module,
        }
    }) ;

} ;
