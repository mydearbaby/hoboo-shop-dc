export default {
    data: [
        {
            'id': 1,
            'value': 1,
            'name': '需求',
        }, {
            'id': 2,
            'value': 2,
            'name': '项目',
        },{
            'id': 3,
            'value': 3,
            'name': '计划',
        }, {
            'id': 4,
            'value': 4,
            'name': '沟通',
        },{
            'id': 5,
            'value': 5,
            'name': '文件',
        }
    ]
};
