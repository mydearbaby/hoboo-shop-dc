export default {
    url:'CrmSalesman/getProjectMember',
    fields:[{
        name:'value',
        mapping:'uuid'
    },{
        name:'text',
        mapping:'real_name'
    }]
} ;
