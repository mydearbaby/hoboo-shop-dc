
export default {
    url:'systemConfig/getList',
    cache:'view',
    fields:[{
        name:'value',
        mapping:'uuid'
    },{
        name:'text',
        mapping:'value'
    }]
} ;