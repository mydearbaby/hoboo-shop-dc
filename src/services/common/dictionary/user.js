export default {
    url:'user/getList',
    fields:[{
        name:'value',
        mapping:'uuid'
    },{
        name:'text',
        mapping:'real_name'
    }]
} ;
