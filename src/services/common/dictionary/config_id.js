
export default {
    url:'systemConfig/getList',
    cache:'view',
    fields:[{
        name:'value',
        mapping:'id'
    },{
        name:'text',
        mapping:'value'
    }]
} ;