export default {
    url:'CrmCustomerContacts/getList',
    fields:[{
        name:'value',
        mapping:'customer_uuid'
    },{
        name:'text',
        mapping:'name'
    }]
} ;
