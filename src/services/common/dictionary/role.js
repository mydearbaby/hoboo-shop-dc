export default {
    url:'userRole/getList',
    fields:[{
        name:'value',
        mapping:'id'
    },{
        name:'text',
        mapping:'name'
    }]
} ;
