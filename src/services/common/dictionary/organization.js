// export default 'Organization/getList' ;
export default {
    url:'Organization/getList',
    fields:[{
        name:'value',
        mapping:'uuid'
    },{
        name:'text',
        mapping:'name'
    }]
} ;
