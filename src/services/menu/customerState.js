export default () => [{
    "id": 1,
    "name": "启用",
},
{
    "id": 2,
    "name": "禁用",
}]
;

export const LOAD_DATA_CACHE_MODE = 'CACHE' ;

export const LOAD_DATA_TYPE = 'DICTIONARY' ;

export const LOAD_DATA_DICTIONARY_TYPE = {
value:'id',
text:'name'
} ;