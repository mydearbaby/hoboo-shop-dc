import {
    center,
    right,
    grid as gridClass,
    formInput,
    header,
    button,
    search,
    rightContent
} from '@/css/VueCss.module.scss' ;

export default ({
    grid,
    form,
    services
}) => {

    return {
        items: [
            {
                region: 'center',
                class: center,
                layout: 'vertical',
                items: [
                    {
                        items: [
                            {
                                id: 'search',
                                class: search,
                                selects: [
                                    'state'
                                ],
                                widget: 'search',
                                onSearch: [
                                    {
                                        action: 'grid-load',
                                        target: '$widgets.grid',
                                        params: '$params'
                                    }
                                ],
                            }, {
                                class: header,
                                layout: 'horizontal',
                                items: [
                                    {
                                        class: button,
                                        widget: 'button',
                                        value: '新增',
                                        onClick: [
                                            {
                                                action:'call',
                                                target:'$widgets.form',
                                                method:'reset'
                                            }, {
                                                action:'call',
                                                target:'$widgets.grid',
                                                method:'clearSelection'
                                            }
                                        ]
                                    }, {
                                        class: button,
                                        widget: 'button',
                                        value: '删除',
                                        onClick: [
                                            {
                                                action: 'confirm',
                                                title: '删除数据',
                                                message: '是否删除勾选数据?',
                                                confirm: [
                                                    {
                                                        var: 'checkedItemIds',
                                                        action: 'array-value',
                                                        target: '$widgets.grid.checkedItems',
                                                        key: 'id'
                                                    }, {
                                                        action: 'print',
                                                        description: '列表勾选项',
                                                        value: '$vars.checkedItemIds'
                                                    }, {
                                                        action: 'service',
                                                        path:services.remove,
                                                        params: {
                                                            id: '$vars.checkedItemIds'
                                                        }
                                                    }, {
                                                        action: 'call',
                                                        target: '$widgets.grid',
                                                        method: 'reload'
                                                    }
                                                ]
                                            }
                                        ]
                                    }, {
                                        class: button,
                                        widget: 'basic-groupbutton',
                                        value: '删除',
                                        onClick: [
                                            {
                                                action: 'dialog-show',
                                                path: 'grid/item/detail/Drawer',
                                                params: {
                                                    value: '`第 ${$view.count ++} 次点击`'
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }, {
                        id: 'grid',
                        class: gridClass,
                        widget: 'grid',
                        service: {
                            path: services.list,
                            params: {
                                is_sub_admin: 0,
                            },
                        },
                        pageSize: 20,
                        columns:grid.columns,
                        buttons: [
                            {
                                value: '编辑',
                                onClick: [{
                                    action:'dialog-show',
                                    path:services.dialog,
                                    params: {
                                        title:"'新增通讯录'",
                                        form: {
                                            group_uuid:'$widgets.grid.selectedItem.uuid'
                                        }
                                    },
                                    applyActions: [{
                                        action: 'call',
                                        target: '$widgets.grid',
                                        method: 'reload'
                                    }],
                                    cancelActions:[{
                                        action:'print',
                                        description:'取消对话框'
                                    }]
                                }]
                            }, {
                                value: '删除',
                                type: 'danger',
                                onClick: [
                                    {
                                        action: 'confirm',
                                        title: '删除数据',
                                        message: '是否删除该条数据?',
                                        confirm: [
                                            {
                                                action: 'service',
                                                path:services.remove,
                                                params: {
                                                    id:'$params.id'
                                                }
                                            },{
                                                action:'call',
                                                target:'$widget',
                                                method:'reload'
                                            }
                                        ]
                                    }
                                ]
                            },
                            ...(grid.buttons || [])
                        ],
                        flex: true,
                        enableCheckboxColumn: true,
                        onRowselect: [
                            {
                                action:'call',
                                target:'$widgets.form',
                                method:'setValue',
                                params:'$params'
                            }
                        ],
                    }
                ]
            }
              
        ]
    } ;
} ;