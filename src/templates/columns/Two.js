import {
    center,
    right,
    grid as gridClass,
    formInput,
    header,
    button,
    search as searchClass,
    rightContent
} from '@/css/VueCss.module.scss' ;


export default ({
    grid,
    form,
    services,
    search
}) => {

    return {
        items: [
            {
                region: 'center',
                class: center,
                layout: 'vertical',
                items: [
                    {
                        items: [
                            {
                                id: 'search',
                                class: searchClass,
                                selects:search.selects || [
                                    'state'
                                ],
                                widget: 'search',
                                onSearch: [
                                    {
                                        action: 'grid-load',
                                        target: '$widgets.grid',
                                        params: {
                                            '...':'$params',
                                            ...(search.extraParams || {})
                                        }
                                    }
                                ],
                            }, {
                                class: header,
                                layout: 'horizontal',
                                items: [
                                    {
                                        class: button,
                                        widget: 'button',
                                        value: '新增',
                                        onClick: [
                                            {
                                                action:'call',
                                                target:'$widgets.form',
                                                method:'reset'
                                            }, {
                                                action:'call',
                                                target:'$widgets.grid',
                                                method:'clearSelection'
                                            }
                                        ]
                                    }, {
                                        class: button,
                                        widget: 'button',
                                        value: '删除',
                                        onClick: [
                                            {
                                                action: 'confirm',
                                                title: '删除数据',
                                                message: '是否删除勾选数据?',
                                                confirm: [
                                                    {
                                                        var: 'checkedItemIds',
                                                        action: 'array-value',
                                                        target: '$widgets.grid.checkedItems',
                                                        key: 'id'
                                                    }, {
                                                        action: 'print',
                                                        description: '列表勾选项',
                                                        value: '$vars.checkedItemIds'
                                                    }, {
                                                        action: 'service',
                                                        path:services.allremove,
                                                        params: {
                                                            id: '$vars.checkedItemIds'
                                                        }
                                                    }, {
                                                        action: 'call',
                                                        target: '$widgets.grid',
                                                        method: 'reload'
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }, {
                        id: 'grid',
                        class: gridClass,
                        widget: 'grid',
                        service: {
                            path: services.list.path,
                            params: {
                                ...services.list.params
                            },
                        },
                        pageSize: 20,
                        columns:grid.columns,
                        operationWidth:150,
                        buttons: [
                            {
                                value: '编辑',
                            }, {
                                value: '删除',
                                type: 'danger',
                                onClick: [
                                    {
                                        action: 'confirm',
                                        title: '删除数据',
                                        message: '是否删除该条数据?',
                                        confirm: [
                                            {
                                                action: 'service',
                                                path:services.remove,
                                                params: {
                                                    id:'$params.id'
                                                }
                                            },{
                                                action:'call',
                                                target:'$widget',
                                                method:'reload'
                                            }
                                        ]
                                    }
                                ]
                            },
                            ...(grid.buttons || [])
                        ],
                        flex: true,
                        enableCheckboxColumn: true,
                        onRowselect: [
                            {
                                action:'call',
                                target:'$widgets.form',
                                method:'setValue',
                                params:'$params'
                            }
                        ],
                    }
                ]
            }, {
                region: 'right',
                class: right,
                items: [
                    {
                        id: 'title',
                        widget: 'public-titleright',
                        title: '编辑修改',
                    }, {
                        class: rightContent,
                        items: [
                            {
                                id: 'form',
                                widget: 'form',
                                fields: form.fields.map((field , index) => index === 0 ? field : {
                                    ...field,
                                    class:formInput
                                }),
                                buttons: [
                                    {
                                        value: '保存',
                                        type: 'primary',
                                        onClick: [
                                            {
                                                action: 'confirm',
                                                title: '表单保存',
                                                message: '是否保存表单?',
                                                confirm: [
                                                    {
                                                        var:'value',
                                                        target:'$widget',
                                                        action:'call',
                                                        method:'getValue'
                                                    },{
                                                        action: 'service',
                                                        path: services.save,
                                                        params:'$vars.value'
                                                    },{
                                                        action:'call',
                                                        target:'$widgets.grid',
                                                        method:'reload'
                                                    }, {
                                                        action: 'call',
                                                        target: '$widgets.form',
                                                        method: 'reset'
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                flex: true
                            }
                        ]
                    }
                ]
            }
        ]
    } ;
} ;
