export default {
    title:'单选按钮组',
    items:[{
        region:'center',
        items:[{
            widget:'radiogroup',
            margin:{
                top:10,
                left:10
            },
            onChange:[{
                action:'print',
                description:'单选按钮组',
                value:'$params'
            }],
            button:true,
            direction:'vertical',
            options:[{
                value:'proposal',
                text:'建议'
            },{
                value:'option1',
                text:'选项1'
            },{
                value:'option2',
                text:'选项2'
            },{
                value:'star',
                text:'已加星标'
            }]
        }]
    }]
} ;