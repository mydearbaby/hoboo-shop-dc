export default {
    title:'三栏模板',
    template:{
        path:'columns/onedialogs',
        params:{
            services:{
                save:'system/admin/save',
                remove:'crm/customer/remove',
                list:'crm/customer/list',
                dialog:'Customer/CustomerList/Edit'
            },
            grid:{
                columns: [{
                    prop: 'name',
                    label: '客户名称(公司)',
                    minWidth: 220
                }, {
                    prop: 'category_uuid',
                    label: '客户分类',
                    dictionary: {
                        path: 'common/dictionary/config',
                        params: {
                            name: 'crm_customer_category',
                        },
                    },
                }, {
                    prop: 'industry_uuid',
                    dictionary: {
                        path: 'common/dictionary/config',
                        params: {
                            name: 'crm_customer_industry',
                        },
                    },
                    label: '所属行业',
                }, {
                    prop: 'source_uuid',
                    label: '客户来源',
                    dictionary: {
                        path: 'common/dictionary/config',
                        params: {
                            name: 'crm_customer_source',
                        },
                    },
                }, {
                    prop: 'status_uuid',
                    label: '客户状态',
                    dictionary: {
                        path: 'common/dictionary/config',
                        params: {
                            name: 'crm_customer_status',
                        },
                    },
                }, {
                    prop: 'level_uuid',
                    label: '客户等级',
                    dictionary: {
                        path: 'common/dictionary/config',
                        params: {
                            name: 'crm_customer_level',
                        },
                    },
                }, {
                    prop: 'stage_uuid',
                    label: '当前阶段',
                    dictionary: {
                        path: 'common/dictionary/config',
                        params: {
                            name: 'crm_sale_stage',
                        },
                    },
                }, {
                    prop: 'label_uuid',
                    label: '客户标签',
                    dictionary: {
                        path: 'common/dictionary/config',
                        params: {
                            name: 'crm_customer_label',
                        },
                    },
                },{
                    prop: 'salesman',
                    label: '销售联系人',
                    width: 150
                }, {
                    prop: 'contact',
                    label: '客户联系人',
                    width: 150
                }, {
                    prop: 'phone',
                    label: '手机号码',
                    width: 150
                }, {
                    prop: 'last_follow_time',
                    label: '最近联系',
                    width: 180
                }, {
                    prop: 'next_plan_time',
                    label: '下次跟进',
                    width: 180
                }],
                buttons:[{
                    value: '详情',
                    type: 'danger',
                    onClick: [
                        {
                            action: 'dialog-show',
                            path: 'Customer/CustomerList/Details',
                            params: '$params',
                        }
                    ]
                }]
            },
            
        }
    }
} ;