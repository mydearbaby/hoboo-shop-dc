export default {
    title:'三栏模板',
    template:{
        path:'columns/two',
        params:{
            services:{
                save:{
                    path:'system/admin/save',
                    params:{
                       
                    }
                },
                remove:{
                    path:'system/admin/remove',
                    params:{

                    }
                },
                list:{
                    path:'system/admin/list',
                    params:{
                        
                    }
                }
            },
            search:{
                selects:[
                    'state'
                    // 这里增加与原来一样的搜索 selects 配置
                ],
                extraParams:{
                   // 这里添加额外参数
                }
            },
            form:{
                fields:[
                    {
                        itemId: 'is_sub_admin',
                        value: 0,
                    }, {
                        itemId: 'account',
                        placeholder: '请输入账号',
                        widget: 'text',
                        label: '账号',
                    }, {
                        itemId: 'real_name',
                        placeholder: '请输入真实姓名',
                        widget: 'text',
                        label: '真实姓名',
                    }, {
                        itemId: 'state',
                        widget: 'select',
                        label: '管理员状态',
                        placeholder: '请选择管理员状态',
                        dictionary:'common/static/state',
                    }
                ]
            },
            grid:{
                columns:[
                    {
                        prop: 'account',
                        label: '账号',
                        minWidth: 120
                    }, {
                        prop: 'real_name',
                        label: '真实姓名',
                        minWidth: 150
                    }, {
                        prop: 'last_ip',
                        label: '最后登录IP',
                    }, {
                        prop: 'last_time',
                        label: '最后登录时间',
                        minWidth: 120
                    }, {
                        prop: 'create_time',
                        label: '添加时间',
                        minWidth: 120
                    }, {
                        prop: 'login_count',
                        label: '登录次数',
                        minWidth: 120
                    }, 'state'
                ],
                buttons:[{
                    value: '重置密码',
                    type: 'danger',
                    onClick: [
                        {
                            action: 'dialog-show',
                            path: 'System/Department/AdminPopup',
                            params: '$params',
                            applyActions: [{
                                action: 'call',
                                target: '$widgets.grid',
                                method: 'reload'
                            }],
                            cancelActions: [{
                                action: 'print',
                                description: '取消对话框'
                            }]
                        }
                    ]
                }]
            },
            
        }
    }
} ;