import {
    h,
    resolveComponent
} from 'vue' ;


function processItems(items = []){

    return items.map(({
        fullPath,
        title,
        icon:iconPath,
        children,
        name
    }) => h(resolveComponent(children ? 'el-sub-menu' : 'el-menu-item') , {
        index:fullPath,
        class:[
            name
        ]
    } , {
        title(){

            let nodes = [] ;

            if(iconPath && children){

                nodes.push(h(resolveComponent('el-icon') , null , {
                    default(){

                        return h(require(`@/${iconPath}`).default)
                    }
                })) ;
            }

            nodes.push(h('span' , {} , title)) ;

            return nodes;
        },

        default(){

            let nodes = [] ;

            if(iconPath && !children){

                nodes.push(h(resolveComponent('el-icon') , null , {
                    default(){

                        return h(require(`@/${iconPath}`).default)
                    }
                })) ;
            }

            nodes.push(...processItems(children));

            return nodes ;
        }
    })) ;
}

export default ({
    items
}) => processItems(items) ;