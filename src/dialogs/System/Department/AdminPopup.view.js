import {
    formInput,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'重置密码',
    dialog:{
        type:'dialog'
    },
    onMounted:[{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params'
    }],
    items:[{
        region: 'center',
        layout:'vertical',
        items:[{
            id: 'form',
            widget: 'form',
            fields:[{
                class: formInput,
                itemId: 'account',
                widget:'text',
                label:'账号',
            },{
                class: formInput,
                itemId: 'password',
                placeholder:'请输入新密码',
                widget:'text',
                label:'新密码',
            },{
                class: formInput,
                itemId: 'password',
                placeholder:'请再次输入新密码',
                widget:'text',
                label:'确认密码',
            }],
            buttons: [{
                value: '保存',
                type: 'primary',
                onClick: [{
                    action: 'confirm',
                    title: '表单保存',
                    message: '是否保存表单?',
                    confirm: [{
                        var:'value',
                        target:'$widget',
                        action:'call',
                        method:'getValue'
                    },{
                        action: 'service',
                        path: 'system/admin/reset',
                        params: '$vars.value'
                    },{
                        action:'dialog-hide',
                        mode:'apply'
                    }]
                }]
            }],
        }]
    }]
} ;