import {
    center,
    right,
    grid,
    input,
    button,
    search,
    header,
} from '@/css/DialogsCss.module.scss';

export default {
    title:'人员详情',
    dialog:{
        size:'50%'
    },
    onMounted: [{
        action: 'set',
        target: '$widgets.formbasic',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        span: 10,
        class: center,
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'formbasic',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:2
                },
                fields:[{
                    itemId: 'account',
                    widget:'form-text',
                    label:'用户账号',
                },{
                    itemId: 'password',
                    widget:'form-text',
                    label:'用户密码',
                },{
                    itemId: 'real_name',
                    widget:'form-text',
                    label:'真实姓名',
                },{
                    itemId: 'birthday',
                    widget:'form-text',
                    label:'生日日期',
                },{
                    itemId: 'card_id',
                    widget:'form-text',
                    label:'身份证号码',
                },{
                    itemId: 'nickname',
                    widget:'form-text',
                    label:'用户昵称',
                },{
                    itemId: 'phone',
                    widget:'form-text',
                    label:'手机号码',
                },{
                    itemId: 'user_type',
                    widget:'form-text',
                    label:'用户类型',
                },{
                    itemId: 'address',
                    widget:'form-text',
                    label:'详细地址',
                },{
                    itemId: 'record_phone',
                    widget:'form-text',
                    label:'记录临时电话',
                },{
                    itemId: 'add_time',
                    widget:'form-text',
                    label:'添加时间',
                },{
                    itemId: 'add_ip',
                    widget:'form-text',
                    label:'添加IP',
                },{
                    itemId: 'last_time',
                    widget:'form-text',
                    label:'最后登录时间',
                },{
                    itemId: 'last_ip',
                    widget:'form-text',
                    label:'最后登录IP',
                },{
                    itemId: 'mark',
                    widget:'form-text',
                    label:'用户备注',
                }],
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            action: 'service',
                            path: 'system/department/user/save',
                            params: '$widget.value'
                        }, {
                            action: 'message',
                            message: '保存成功',
                            type: 'success'
                        }, {
                            action: 'service',
                            path: 'system/department/user/list',
                            params:{
                                page_size:'$widgets.grid.pageSize',
                                page_no:'$widgets.grid.page',
                                keywords: '$widgets.search.value.keywords',
                                is_show: '$widgets.search.value.is_show',
                            },
                            actions: [{
                                action:'set',
                                target:'$widgets.grid',
                                field:'value',
                                value:'$params.data.items'
                            },{
                                action:'set',
                                target:'$widgets.grid',
                                field:'total',
                                value:'$params.data.total'
                            }]
                        },{
                            action:'dialog-hide',
                            mode:'apply'
                        }]
                    }]
                }],
                flex: true
            }]
        }]
    }]
};


