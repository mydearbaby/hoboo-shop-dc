import {
    center,
    right,
    grid,
    input,
    button,
    search,
    header,
} from '@/css/DialogsCss.module.scss';

export default {
    title:'人员管理',
    dialog:{
        size:'50%'
    },
    onMounted: [{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        span: 10,
        class: center,
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'form',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:2
                },
                fields:[{
                    class: input,
                    itemId: 'account',
                    placeholder:'请输入用户账号',
                    widget:'text',
                    label:'用户账号',
                },{
                    class: input,
                    itemId: 'password',
                    placeholder:'请输入用户密码',
                    widget:'text',
                    label:'用户密码',
                },{
                    class: input,
                    itemId: 'real_name',
                    placeholder:'请输入真实姓名',
                    widget:'text',
                    label:'真实姓名',
                },{
                    itemId: 'birthday',
                    placeholder:'请选择生日日期',
                    widget:'form-datepicker',
                    type: 'date',
                    label:'生日日期',
                },{
                    class: input,
                    itemId: 'card_id',
                    placeholder:'请输入身份证号码',
                    widget:'text',
                    label:'身份证号码',
                },{
                    class: input,
                    itemId: 'nickname',
                    placeholder:'请输入用户昵称',
                    widget:'text',
                    label:'用户昵称',
                },{
                    class: input,
                    itemId: 'phone',
                    placeholder:'请输入手机号码',
                    widget:'text',
                    label:'手机号码',
                },{
                    class: input,
                    itemId: 'user_type',
                    placeholder:'请输入用户类型',
                    widget:'text',
                    label:'用户类型',
                },{
                    class: input,
                    itemId: 'address',
                    placeholder:'请输入详细地址',
                    widget:'text',
                    label:'详细地址',
                },{
                    class: input,
                    itemId: 'record_phone',
                    placeholder:'请输入记录临时电话',
                    widget:'text',
                    label:'记录临时电话',
                },{
                    class: input,
                    itemId: 'mark',
                    placeholder:'请输入用户备注',
                    widget:'text',
                    label:'用户备注',
                }],
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            action: 'service',
                            path: 'system/department/user/save',
                            params: '$widget.value'
                        }, {
                            action: 'message',
                            message: '保存成功',
                            type: 'success'
                        }, {
                            action: 'service',
                            path: 'system/department/user/list',
                            params:{
                                page_size:'$widgets.grid.pageSize',
                                page_no:'$widgets.grid.page',
                                keywords: '$widgets.search.value.keywords',
                                is_show: '$widgets.search.value.is_show',
                            },
                            actions: [{
                                action:'set',
                                target:'$widgets.grid',
                                field:'value',
                                value:'$params.data.items'
                            },{
                                action:'set',
                                target:'$widgets.grid',
                                field:'total',
                                value:'$params.data.total'
                            }]
                        },{
                            action:'dialog-hide',
                            mode:'apply'
                        }]
                    }]
                }],
                flex: true
            }]
        }]
    }]
};


