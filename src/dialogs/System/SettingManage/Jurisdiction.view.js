import {
    center,
    right,
    grid,
    input,
    button,
    search,
    header,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'权限管理',
    dialog:{
        size:'50%'
    },
    onMounted: [{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        span: 10,
        class: center,
        layout: 'vertical',
        items: [{
            class: header,
            layout: 'horizontal',
            items: [{
                class: button,
                widget: 'button',
                type: 'primary',
                value: '保存',
                onClick: [{
                    action: 'dialog-show',
                    path: 'System/SettingManage/RoleManage',
                    applyActions: [{
                        action: 'service',
                        path: 'system/setting/role/list',
                        params: {
                            page_size: '$widgets.grid.pageSize',
                            page_no: '$widgets.grid.page',
                            keywords: '$widgets.search.value.keywords',
                            is_show: '$widgets.search.value.is_show',
                        },
                        actions: [{
                            action: 'set',
                            target: '$widgets.grid',
                            field: 'value',
                            value: '$params.data.items'
                        }, {
                            action: 'set',
                            target: '$widgets.grid',
                            field: 'total',
                            value: '$params.data.total'
                        }]
                    }],
                    cancelActions: [{
                        action: 'print',
                        description: '取消对话框'
                    }]
                }, {
                    action: 'form-reset',
                    target: '$widgets.form'
                }, {
                    action: 'grid-selection-clear',
                    target: '$widgets.grid'
                }]

            }]
        },{
            id: 'gridTree',
            class: grid,
            widget: 'grid-tree',
            pageSize: 20,
            columns: [{
                label: '',
                width: 80
            }, {
                prop: 'name',
                label: '名称'
            }, {
                prop: 'module',
                label: '模块名'
            }, {
                prop: 'is_show',
                dictionary: true,
                label: '是否启用',
                width: 100
            }],
            flex: true,
            onMounted: [{
                action: 'service',
                params: {
                    page_size: '$widget.pageSize'
                },
                path: 'system/setting/menu/treeList',
                actions: [{
                    action: 'set',
                    target: '$widget',
                    field: 'value',
                    value: '$params.data.items'
                }, {
                    action: 'set',
                    target: '$widget',
                    field: 'total',
                    value: '$params.data.total'
                }]
            }],
        }]
    }]
};
