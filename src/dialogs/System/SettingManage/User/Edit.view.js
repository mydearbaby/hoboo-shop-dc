import {
    center,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'新增编辑',
    dialog:{
        size:'50%'
    },
    onMounted: [{
        action:'print',
        value:'$params'
    },{
        action:'set',
        target: '$view',
        field:'title',
        value:'$params.title'
    },{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params.form'
    }],
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'form',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:2
                },
                fields:[{
                    itemId: 'role_id',
                    placeholder:'请选择角色',
                    print: true,
                    widget:'select',
                    dictionary: 'common/dictionary/role',
                    label:'角色名称',
                },{
                    itemId: 'account',
                    placeholder:'请输入用户账号',
                    widget:'text',
                    label:'用户账号',
                },{
                    itemId: 'real_name',
                    placeholder:'请输入真实姓名',
                    widget:'text',
                    label:'真实姓名',
                },{
                    itemId: 'birthday',
                    placeholder:'请选择生日日期',
                    widget:'form-datepicker',
                    type: 'date',
                    label:'生日日期',
                },{
                    itemId: 'card_id',
                    placeholder:'请输入身份证号码',
                    widget:'text',
                    label:'身份证号码',
                },{
                    itemId: 'nickname',
                    placeholder:'请输入用户昵称',
                    widget:'text',
                    label:'用户昵称',
                },{
                    itemId: 'phone',
                    placeholder:'请输入手机号码',
                    widget:'text',
                    label:'手机号码',
                },{
                    itemId: 'address',
                    placeholder:'请输入详细地址',
                    widget:'text',
                    label:'详细地址',
                },{
                    itemId: 'record_phone',
                    placeholder:'请输入临时电话',
                    widget:'text',
                    label:'临时电话',
                },{
                    itemId: 'remarks',
                    placeholder:'请输入用户备注',
                    widget:'text',
                    label:'用户备注',
                }],
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            var:'value',
                            target:'$widget',
                            action:'call',
                            method:'getValue'
                        },{
                            action: 'service',
                            path: 'system/role/user/save',
                            //params: '$vars.value'
                            params:{
                                role_id:'$params.role_id',
                                '...':['$vars.value']
                            }
                        },{
                            action:'dialog-hide',
                            mode:'apply'
                        }]
                    }]
                }],
                flex: true
            }]
        }]
    }]
};
