import {
    center,
} from '@/css/DialogsCss.module.scss';

export default {
    title:'用户详情',
    dialog:{
        size:'50%'
    },
    onMounted: [{
        action: 'set',
        target: '$widgets.formbasic',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'formbasic',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:2
                },
                fields:[{
                    itemId: 'role_id',
                    print: true,
                    widget:'select',
                    dictionary: 'common/dictionary/role',
                    label:'角色名称',
                },{
                    itemId: 'account',
                    widget:'form-text',
                    label:'用户账号',
                },{
                    itemId: 'real_name',
                    widget:'form-text',
                    label:'真实姓名',
                },{
                    itemId: 'birthday',
                    widget:'form-text',
                    label:'生日日期',
                },{
                    itemId: 'card_id',
                    widget:'form-text',
                    label:'身份证号码',
                },{
                    itemId: 'nickname',
                    widget:'form-text',
                    label:'用户昵称',
                },{
                    itemId: 'phone',
                    widget:'form-text',
                    label:'手机号码',
                },{
                    itemId: 'address',
                    widget:'form-text',
                    label:'详细地址',
                },{
                    itemId: 'record_phone',
                    widget:'form-text',
                    label:'临时电话',
                },{
                    itemId: 'add_time',
                    widget:'form-text',
                    label:'添加时间',
                },{
                    itemId: 'add_ip',
                    widget:'form-text',
                    label:'添加IP',
                },{
                    itemId: 'last_time',
                    widget:'form-text',
                    label:'最后登录时间',
                },{
                    itemId: 'last_ip',
                    widget:'form-text',
                    label:'最后登录IP',
                },{
                    itemId: 'remarks',
                    widget:'form-text',
                    label:'用户备注',
                }],
                flex: true
            }]
        }]
    }]
};


