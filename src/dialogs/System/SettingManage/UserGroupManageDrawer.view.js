import {
    center,
    right,
    grid,
    input,
    button,
    search,
    header,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'详情 - 抽屉式',
    dialog:{
        size:500
    },
    layout:'fit',
    onMounted:[{
        action:'set',
        target:'$widgets.form',
        field:'value',
        value:'$params'
    }],
    items:[{
        layout:'vertical',
        items:[{
            id: 'form',
            widget: 'form',
            fields: [{
                class: input,
                itemId: 'group_name',
                placeholder: '请输入用户组',
                widget: 'text',
                label: '用户组',
            }, {
                class: input,
                itemId: 'mark',
                placeholder: '请输入用户备注',
                widget: 'text',
                label: '用户备注',
            }, {
                itemId: 'status',
                widget: 'radiogroup',
                value:0,
                options: [{
                    value: 1,
                    text: '启用'
                }, {
                    value: 2,
                    text: '禁用'
                }],
                label: '状态',
            }],
            buttons: [{
                value: '保存',
                onClick: [{
                    action: 'confirm',
                    title: '表单保存',
                    message: '是否保存表单?',
                    confirm: [{
                        action: 'service',
                        path: 'system/setting/user_group/save',
                        params: {
                            values:'$widget.value',
                            module:'"business_group"'
                        
                        }
                    }, {
                        action: 'message',
                        message: '保存成功',
                        type: 'success'
                    },{
                    //     action: 'service',
                    //     path: 'system/setting/user_group/list',
                    //     params:{
                    //         // page_size:'$widgets.grid.pageSize',
                    //         // page_no:'$widgets.grid.page',
                    //         // keywords: '$widgets.search.value.keywords',
                    //         // is_show: '$widgets.search.value.is_show',
                    //         page_size:'20',
                    //         page_no:'1',
                    //         // keywords: '$widgets.search.value.keywords',
                    //         // is_show: '$widgets.search.value.is_show',
                    //     },
                    //     actions: [{
                    //         action: 'print',
                    //         description: '----------编辑内容-widgets------------------',
                    //         value: '$widget.grid'
                    //     },{
                    //         action:'set',
                    //         target:'$widgets.grid',
                    //         field:'value',
                    //         value:'$params.data.items'
                    //     },
                    //     {
                    //         action: 'set',
                    //         target: '$widget',
                    //         field: 'total',
                    //         value: '$params.data.total'
                    //     }
                    // ]
                    },{
                        action:'dialog-hide',
                        mode:'apply'
                    }]
                }]
            }],
        }]
    }]
} ;