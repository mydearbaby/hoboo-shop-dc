export default {
    title:'权限编辑',
    dialog:{
        width: 600,
    },
    onMounted: [{
        action:'print',
        value:'$params'
    },{
        action:'set',
        target: '$view',
        field:'title',
        value:'$params.title'
    },{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params.form'
    }],
    items: [{
        layout: 'vertical',
        items: [{
            id: 'gridMenu',
            widget: 'grid',
            service:'dictionary/routes',
            columns:[{
                prop: 'title',
                label: '名称'
            }],
            enableIndexColumn:false,
            enableCheckboxColumn:true,
            flex:true,
        },{
            id: 'form',
            widget: 'form',
            fields:[{
                itemId: 'role_uuid',
            }],
            buttons: [{
                value: '保存',
                type: 'primary',
                onClick: [{
                    action: 'confirm',
                    title: '权限保存',
                    message: '是否保存权限配置?',
                    confirm: [{
                        var:'checkedItemIds',
                        action:'array-value',
                        target:'$widgets.gridMenu.checkedItems',
                        key:'id'
                    },{
                        var: 'value',
                        target: '$widget',
                        action: 'call',
                        method: 'getValue'
                    },{
                        action: 'print',
                        value: {
                            '...':['$vars.value'],
                            // role_uuid: '$view.params.form.role_uuid',
                            roles: '$vars.checkedItemIds'
                        }
                    },{
                        action: 'service',
                        path: 'system/role/users/save_roles',
                        params:{
                            '...':['$vars.value'],
                            roles: '$vars.checkedItemIds'
                        }
                    },{
                        action: 'dialog-hide',
                        mode: 'apply'
                    }]
                }]
            }],
        }],
    }]
};
