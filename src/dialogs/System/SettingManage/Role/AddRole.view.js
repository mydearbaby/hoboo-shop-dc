import {
    formInput,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'新增编辑',
    dialog:{
        width: 600,
        type:'dialog',
    },
    onMounted: [{
        action:'print',
        value:'$params'
    },{
        action:'set',
        target: '$view',
        field:'title',
        value:'$params.title'
    },{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params.form'
    }],
    items:[{
        region: 'center',
        layout:'vertical',
        items:[{
            id: 'form',
            widget: 'form',
            fields: [{
                class: formInput,
                itemId: 'value',
                placeholder:'请输入角色名称',
                widget:'text',
                label:'角色名称',
            },{
                class: formInput,
                itemId: 'description',
                placeholder:'请输入角色备注',
                widget:'text',
                label:'角色备注',
            },{
                class: formInput,
                itemId:'state',
                widget:'select',
                dictionary:'common/static/state',
                placeholder:'请选择状态',
                label:'状态'
            }],
            buttons: [{
                value: '保存',
                type: 'primary',
                onClick: [{
                    action: 'confirm',
                    title: '表单保存',
                    message: '是否保存表单?',
                    confirm: [{
                        var:'value',
                        target:'$widget',
                        action:'call',
                        method:'getValue'
                    },{
                        action: 'service',
                        path: 'system/dictionary/config/save',
                        params: {
                            '...':[
                                '$vars.value'
                            ],
                            module:'"system_module"',
                            name: '"user_role"',
                        }
                    },{
                        action:'dialog-hide',
                        mode:'apply'
                    }]
                }]
            }],
        }]
    }]
} ;
