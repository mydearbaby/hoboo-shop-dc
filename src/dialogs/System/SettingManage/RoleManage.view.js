import {
    center,
    right,
    grid,
    input,
    button,
    search,
    header,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'角色管理',
    dialog:{
        size:500
    },
    onMounted: [{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        span: 10,
        class: center,
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'form',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:1
                },
                fields: [{
                    itemId: 'name',
                    placeholder:'请输入角色名称',
                    widget:'text',
                    label:'角色名称',
                },{
                    itemId: 'remark',
                    placeholder:'请输入角色备注',
                    widget:'text',
                    label:'角色备注',
                },{
                    itemId:'state',
                    widget:'select',
                    dictionary:'common/static/state',
                    placeholder:'请选择状态',
                    label:'状态'
                }],
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            action: 'service',
                            path: 'system/userrole/save',
                            params: '$widget.value'
                        }, {
                            action: 'message',
                            message: '保存成功',
                            type: 'success'
                        },{
                            action:'call',
                            target:'$widget',
                            method:'reload'
                        },{
                            action:'dialog-hide',
                            mode:'apply'
                        }]
                    }]
                }],
                flex: true
            }]
        }]
    }]
};
