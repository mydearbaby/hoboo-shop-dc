import {
    formInput,
} from '@/css/DialogsCss.module.scss';
export default {
    dialog:{
        size:500,
        type:'dialog'
    },
    onMounted: [{
        action:'print',
        value:'$params'
    },{
        action:'set',
        target: '$view',
        field:'title',
        value:'$params.title'
    },{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params.form'
    }],
    items:[{
        region: 'center',
        layout:'vertical',
        items:[{
            id: 'form',
            widget: 'form',
            fields:[{
                class: formInput,
                itemId:'title',
                placeholder:'请输入图片标题',
                widget:'text',
                label:'图片标题',
            },{
                class: formInput,
                itemId:'img',
                widget: 'image-uploader-single',
                label:'图片',
            },{
                class: formInput,
                itemId:'url',
                placeholder:'请输入链接地址',
                widget:'text',
                label:'链接地址',
            },{
                class: formInput,
                itemId:'sort',
                placeholder:'请输入排序',
                widget:'text',
                label:'排序',
            },{
                class: formInput,
                itemId:'state',
                widget: 'form-radio',
                value: 1,
                options: [{
                    value: 1,
                    text: '启用'
                }, {
                    value: 0,
                    text: '禁用'
                }],
                label:'状态',
            }],
            buttons: [{
                value: '保存',
                type: 'primary',
                onClick: [{
                    action: 'confirm',
                    title: '表单保存',
                    message: '是否保存表单?',
                    confirm: [{
                        var:'value',
                        target:'$widget',
                        action:'call',
                        method:'getValue'
                    },{
                        action: 'service',
                        path: 'system/banner/save',
                        params: '$vars.value'
                    },{
                        action:'dialog-hide',
                        mode:'apply'
                    }]
                }]
            }],
        }]
    }]
} ;
