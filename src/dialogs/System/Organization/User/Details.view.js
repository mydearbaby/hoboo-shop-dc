import {
    center,
} from '@/css/DialogsCss.module.scss';

export default {
    title:'人员详情',
    dialog:{
        size:'50%'
    },
    onMounted: [{
        action: 'print',
        value: '$params'
    },{
        action: 'set',
        target: '$widgets.formbasic',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'formbasic',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:2
                },
                fields:[{
                    itemId:'position_uuid',
                    widget:'select',
                    print: true,
                    dictionary: {
                        path: 'common/dictionary/config',
                        params: {
                            name: 'organization_position',
                        },
                    },
                    label:'职位',
                },{
                    itemId: 'work_email',
                    widget:'form-text',
                    label:'工作邮箱',
                },{
                    itemId: 'work_phone',
                    widget:'form-text',
                    label:'工作电话',
                },{
                    itemId: 'work_wechat',
                    widget:'form-text',
                    label:'工作微信',
                },{
                    itemId:'state',
                    widget:'select',
                    print: true,
                    dictionary:'common/static/state',
                    label:'状态'
                },{
                    itemId: 'remarks',
                    widget:'form-text',
                    label:'用户备注',
                }],
                flex: true
            }]
        }]
    }]
};


