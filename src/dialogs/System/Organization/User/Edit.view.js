import {
    center,
} from '@/css/DialogsCss.module.scss';

export default {
    title:'新增编辑',
    dialog:{
        size:'50%'
    },
    onMounted: [{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'form',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:2
                },
                fields:[{
                    itemId:'position_uuid',
                    placeholder:'请选择职位',
                    widget:'select',
                    dictionary: {
                        path: 'common/dictionary/config',
                        params: {
                            name: 'organization_position',
                        },
                    },
                    label:'职位',
                },{
                    itemId: 'work_email',
                    placeholder:'请输入工作邮箱',
                    widget:'text',
                    label:'工作邮箱',
                },{
                    itemId: 'work_phone',
                    placeholder:'请选择工作电话',
                    widget:'text',
                    label:'工作电话',
                },{
                    itemId: 'work_wechat',
                    placeholder:'请输入工作微信',
                    widget:'text',
                    label:'工作微信',
                },{
                    itemId:'state',
                    widget:'select',
                    dictionary:'common/static/state',
                    placeholder:'请选择状态',
                    label:'状态'
                },{
                    itemId: 'remarks',
                    placeholder:'请输入用户备注',
                    widget:'text',
                    label:'用户备注',
                }],
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            var:'value',
                            target:'$widget',
                            action:'call',
                            method:'getValue'
                        },{
                            action: 'service',
                            path: 'system/organization/user/save',
                            params: '$vars.value'
                        },{
                        //     action:'call',
                        //     target:'$widget',
                        //     method:'reload'
                        // },{
                            action:'dialog-hide',
                            mode:'apply'
                        }]
                    }]
                }],
                flex: true
            }]
        }]
    }]
};


