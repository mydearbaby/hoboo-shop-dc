import {
    center,
    formInput
} from '@/css/DialogsCss.module.scss';
export default {
    title:'成员信息',
    dialog:{
        size: '50%'
    },
    onMounted: [{
        action:'print',
        value:'$params'
    },{
        action:'set',
        target: '$view',
        field:'title',
        value:'$params.title'
    },{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params.form'
    }],
    items: [{
        region: 'center',
        class: center,
        items: [{
            items: [{
                id: 'form',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:2
                },
                fields:[{
                    class: formInput,
                    itemId: 'code',
                    placeholder:'请输入工号',
                    widget:'text',
                    label:'工号',
                },{
                    class: formInput,
                    itemId: 'organization_uuid',
                    placeholder:'请选择组织',
                    widget:'select',
                    print: true,
                    dictionary:'common/dictionary/organization',
                    label:'组织',
                },{
                    class: formInput,
                    itemId: 'real_name',
                    placeholder:'请输入真实姓名',
                    widget:'text',
                    label:'真实姓名',
                },{
                    class: formInput,
                    itemId: 'gender',
                    placeholder:'请选择性别',
                    widget:'select',
                    dictionary:'common/static/gender',
                    label:'性别',
                },{
                    class: formInput,
                    itemId: 'title_uuid',
                    placeholder:'请选择职位',
                    widget:'select',
                    dictionary: {
                        path: 'common/dictionary/config',
                        params: {
                            name: 'organization_position',
                        },
                    },
                    label:'职位',
                },{
                    class: formInput,
                    itemId: 'birthday',
                    placeholder:'请选择出生日期',
                    widget:'form-datepicker',
                    type: 'date',
                    label:'出生日期',
                },{
                    class: formInput,
                    itemId: 'work_phone',
                    placeholder:'请输入工作电话',
                    widget:'text',
                    label:'工作电话',
                },{
                    class: formInput,
                    itemId: 'work_email',
                    placeholder:'请输入工作邮箱',
                    widget:'text',
                    label:'工作邮箱',
                },{
                    class: formInput,
                    itemId: 'work_wechat',
                    placeholder:'请输入工作微信',
                    widget:'text',
                    label:'工作微信',
                },{
                    class: formInput,
                    itemId: 'state',
                    placeholder:'请选择状态',
                    widget:'select',
                    dictionary:'common/static/state',
                    label:'状态',
                }],
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            var:'value',
                            target:'$widget',
                            action:'call',
                            method:'getValue'
                        },{
                            action: 'service',
                            path: 'system/organization/mechanism/user/save',
                            params:{
                                organization_uuid:'$params.organization_uuid',
                                '...':['$vars.value']
                            }
                        },{
                            action:'dialog-hide',
                            mode:'apply'
                        }]
                    }]
                }],
                flex: true
            }]
        }]
    }]
};


