import {
    center,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'成员详情',
    dialog:{
        size:'50%'
    },
    onMounted: [{
        action: 'set',
        target: '$widgets.formbasic',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        span: 10,
        class: center,
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'formbasic',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:2
                },
                fields:[{
                    itemId: 'code',
                    widget:'form-text',
                    label:'工号',
                },{
                    itemId: 'real_name',
                    widget:'form-text',
                    label:'真实姓名',
                },{
                    itemId:'gender',
                    print: true,
                    widget:'select',
                    dictionary:'common/static/gender',
                    label:'性别',
                },{
                    itemId: 'title_uuid',
                    print: true,
                    widget:'select',
                    dictionary: {
                        path: 'common/dictionary/config',
                        params: {
                            name: 'organization_position',
                        },
                    },
                    label:'职位',
                },{
                    itemId: 'birthday',
                    widget:'form-text',
                    label:'出生日期',
                },{
                    itemId: 'work_phone',
                    widget:'form-text',
                    label:'工作电话',
                },{
                    itemId: 'work_email',
                    widget:'form-text',
                    label:'工作邮箱',
                },{
                    itemId: 'work_wechat',
                    widget:'form-text',
                    label:'工作微信',
                },{
                    itemId: 'state',
                    print: true,
                    widget:'select',
                    dictionary:'common/static/state',
                    label:'状态',
                },{
                    itemId: 'remarks',
                    widget:'form-text',
                    label:'备注说明',
                }],
                flex: true
            }]
        }]
    }]
};


