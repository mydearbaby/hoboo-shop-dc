import {
    formInput
} from '@/css/DialogsCss.module.scss';

export default {
    title: '新增',
    dialog:{
        width: 600,
        type:'dialog',
    },
    onMounted: [{
        action:'print',
        value:'$params'
    },{
        action:'set',
        target: '$view',
        field:'title',
        value:'$params.title'
    },{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params.form'
    }],
    items: [
        {
            region: 'center',
            items: [
                {
                    items: [
                        {
                            id: 'form',
                            widget: 'form',
                            fields: [
                                {
                                    class: formInput,
                                    itemId: 'name',
                                    placeholder: '请输入组织名称',
                                    widget: 'text',
                                    label: '组织名称',
                                }, {
                                    class: formInput,
                                    itemId: 'abbreviation',
                                    placeholder: '请输入组织简称',
                                    widget: 'text',
                                    label: '组织简称',
                                }, {
                                    class: formInput,
                                    itemId: 'profile',
                                    placeholder: '请输入组织简介',
                                    widget: 'text',
                                    label: '组织简介',
                                }, {
                                    class: formInput,
                                    itemId: 'state',
                                    widget: 'select',
                                    label: '状态',
                                    placeholder: '请选择状态',
                                    dictionary:'common/static/state',
                                }
                            ],
                            buttons: [
                                {
                                    value: '保存',
                                    type: 'primary',
                                    onClick: [
                                        {
                                            action: 'confirm',
                                            title: '表单保存',
                                            message: '是否保存表单?',
                                            confirm: [
                                                {
                                                    var: 'value',
                                                    target: '$widget',
                                                    action: 'call',
                                                    method: 'getValue'
                                                }, {
                                                    action: 'service',
                                                    path: 'system/organization/mechanism/list/save',
                                                    params: '$vars.value'
                                                }, {
                                                    action: 'dialog-hide',
                                                    mode: 'apply'
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ],
                            flex: true
                        }
                    ]
                }
            ]
        }
    ]
};


