import {
    input,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'不退款',
    dialog:{
        width:600,
        type:'dialog'
    },
    onMounted:[{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params'
    }],
    items:[{
        region: 'center',
        layout:'vertical',
        items:[{
            id: 'form',
            widget: 'form',
            fields:[{
                class: input,
                itemId: 'order_id',
                placeholder:'请输入退款单号',
                widget:'text',
                label:'退款单号',
            },{
                class: input,
                itemId: 'refund_reason',
                placeholder:'请输入不退款原因',
                widget:'text',
                label:'不退款原因',
            }],
            buttons: [{
                value: '保存',
                type: 'primary',
                onClick: [{
                    action: 'confirm',
                    title: '立即退款',
                    message: '确定要退款吗？',
                    confirm: [{
                        var: 'value',
                        target: '$widget',
                        action: 'call',
                        method: 'getValue'
                    }, {
                        action: 'service',
                        path: 'shop/order/list/no_refund',
                        params: {'...':[
                            '$vars.value',
                            {
                                state:-1,
                                refund_status:-1
                            }
                        ]
                        },
                    }, {
                        action: 'dialog-hide',
                        mode: 'apply'
                    }]
                }]
            }],
        }]
    }]
} ;
