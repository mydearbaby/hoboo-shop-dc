import {
    input,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'立即退款',
    dialog:{
        width:600,
        type:'dialog'
    },
    onMounted:[{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params'
    }],
    items:[{
        region: 'center',
        layout:'vertical',
        items:[{
            id: 'form',
            widget: 'form',
            fields:[{
                class: input,
                itemId: 'order_id',
                placeholder:'请输入退款单号',
                widget:'text',
                label:'退款单号',
            },{
                class: input,
                itemId: 'refund_price',
                placeholder:'请输入退款金额',
                widget:'text',
                label:'退款金额',
            }],
            buttons: [{
                value: '保存',
                type: 'primary',
                onClick: [{
                    action: 'confirm',
                    title: '立即退款',
                    message: '确定要退款吗？',
                    confirm: [{
                        var: 'value',
                        target: '$widget',
                        action: 'call',
                        method: 'getValue'
                    }, {
                        action: 'service',
                        path: 'shop/order/list/refund',
                        params: {'...':[
                            '$vars.value',
                            {
                                paid:1
                            }
                        ]
                        },
                    }, {
                        action: 'dialog-hide',
                        mode: 'apply'
                    }]
                }]
            }],
        }]
    }]
} ;
