import {
    center,
    formTitle,
    grid
} from '@/css/DialogsCss.module.scss';
export default {
    title:'订单详情',
    dialog:{
        size:'50%'
    },
    onMounted: [{
        action: 'set',
        target: '$widgets.formbasic',
        field: 'value',
        value: '$params'
    },{
        action: 'set',
        target: '$widgets.formorder',
        field: 'value',
        value: '$params'
    },{
        action: 'set',
        target: '$widgets.formlogistics',
        field: 'value',
        value: '$params'
    },{
        action: 'set',
        target: '$widgets.formrefund',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                class: center,
                items:[{
                    class: formTitle,
                    id: 'title',
                    widget: 'public-title',
                    title: '收货信息',
                },{
                    id: 'formbasic',
                    widget: 'form',
                    layout: {
                        type:'gridform',
                        column:2
                    },
                    fields:[{
                        itemId:'real_name',
                        widget:'form-text',
                        label:'用户姓名',
                    },{
                        itemId:'user_phone',
                        widget:'form-text',
                        label:'用户电话',
                    },{
                        itemId:'user_address',
                        widget:'form-text',
                        label:'收货地址',
                    }],
                }],
            },{
                class: [center, grid],
                items:[{
                    class: formTitle,
                    id: 'title',
                    widget: 'public-title',
                    title: '订单信息',
                },{
                    id: 'formorder',
                    widget: 'form',
                    layout: {
                        type:'gridform',
                        column:2
                    },
                    fields:[{
                        itemId:'order_id',
                        widget:'form-text',
                        label:'订单ID',
                    },{
                        itemId:'total_num',
                        widget:'form-text',
                        label:'商品总数',
                    },{
                        itemId:'total_price',
                        widget:'form-text',
                        label:'商品总价',
                    },{
                        itemId:'pay_postage',
                        widget:'form-text',
                        label:'邮费',
                    },{
                        itemId:'coupon_price',
                        widget:'form-text',
                        label:'优惠券金额',
                    },{
                        itemId:'deduction_price',
                        widget:'form-text',
                        label:'积分抵扣',
                    },{
                        itemId:'pay_price',
                        widget:'form-text',
                        label:'实际支付',
                    },{
                        itemId:'pay_type',
                        widget:'select',
                        print: true,
                        dictionary: 'shop/common/static/pay_type',
                        label:'支付方式',
                    },{
                        itemId:'create_time',
                        widget:'form-text',
                        label:'创建时间',
                    },{
                        itemId:'pay_time',
                        widget:'form-text',
                        label:'支付时间',
                    }],
                }],
            },{
                class: [center, grid],
                items:[{
                    class: formTitle,
                    id: 'title',
                    widget: 'public-title',
                    title: '物流信息',
                },{
                    id: 'formlogistics',
                    widget: 'form',
                    layout: {
                        type:'gridform',
                        column:2
                    },
                    fields:[{
                        itemId:'delivery_name',
                        placeholder:'请选择快递公司',
                        widget:'select',
                        print: true,
                        dictionary: 'shop/common/static/express',
                        label:'快递公司',
                    },{
                        itemId:'delivery_id',
                        widget:'form-text',
                        label:'快递单号',
                    }],
                }],
            },{
                class: [center, grid],
                items:[{
                    class: formTitle,
                    id: 'title',
                    widget: 'public-title',
                    title: '退款信息',
                },{
                    id: 'formrefund',
                    widget: 'form',
                    layout: {
                        type:'gridform',
                        column:2
                    },
                    fields:[{
                        itemId:'refund_reason_wap_explain',
                        widget:'form-text',
                        label:'退款原因',
                    },{
                        itemId:'refund_status',
                        widget:'select',
                        print: true,
                        dictionary: 'shop/common/static/refund_status',
                        label:'退款状态',
                    },{
                        itemId:'refund_reason_time',
                        widget:'form-text',
                        label:'退款时间',
                    },{
                        itemId:'refund_price',
                        widget:'form-text',
                        label:'退款金额',
                    }],
                }],
            }]
        }]
    }]
};


