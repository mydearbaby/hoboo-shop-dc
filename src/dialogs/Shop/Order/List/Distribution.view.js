import {
    grid,
    centerEdit,
    formTimeLine,
    formInput,
    centerHeader,
    centerEditPad,
    input
} from '@/css/DialogsCss.module.scss';
export default {
    title:'物流信息',
    dialog:{
        size:'50%',
    },
    onMounted:[{
        action: 'set',
        target: '$widgets.formbasic',
        field: 'value',
        value: '$params'
    }],
    items:[{
        region: 'center',
        class: centerEditPad,
        layout:'vertical',
        items:[{
            id: 'formbasic',
            widget: 'form',
            layout: {
                type:'gridform',
                column:3
            },
            fields:[{
                class: input,
                itemId: 'order_id',
                widget:'form-text',
                label:'订单号',
            },{
                class: input,
                itemId: 'delivery_name',
                placeholder:'请选择快递公司',
                widget:'select',
                print: true,
                dictionary: 'shop/common/static/express',
                label:'快递公司',
            },{
                class: input,
                itemId: 'delivery_id',
                placeholder:'请输入快递单号',
                widget:'form-text',
                label:'快递单号',
            }],
        },{
            class: [centerEdit, formTimeLine, centerHeader, grid],
            items:[{ 
                class: formInput,
                id: 'title',
                widget: 'public-title',
                title: '物流轨迹',
            },{
                class: centerEditPad,
                id: 'timeline',
                widget: 'data-timeline',
                field: 'items',
                enableCard:true,
                enableTitle:false,
                placement:'top',
                items: [{
                    timestamp: '2018-04-15',
                    size: 'large',
                    type: 'primary',
                    text:"快递已签收"
                },{
                    timestamp: '2018-04-15',
                    size: 'large',
                    type: 'primary',
                    text:"快递已到XXXXX地"
                },{
                    timestamp: '2018-04-13',
                    hollow: true,
                    color: '#0bbd87',
                    text:"快递已取件"
                },{
                    timestamp: '2018-04-11',
                    hollow: true,
                    text:"商家备货",
                }],
                flex: true
            }]
        }]
    }]
} ;
