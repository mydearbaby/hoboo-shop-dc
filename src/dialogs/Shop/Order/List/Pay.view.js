import {
    input,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'立即支付',
    dialog:{
        width:600,
        type:'dialog'
    },
    onMounted:[{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params'
    }],
    items:[{
        region: 'center',
        layout:'vertical',
        items:[{
            id: 'form',
            widget: 'form',
            fields:[{
                class: input,
                itemId: 'pay_type',
                widget: 'select',
                placeholder:'请选择支付方式',
                dictionary: 'shop/common/static/pay_type',
                label:'支付方式',
            }],
            buttons: [{
                value: '保存',
                type: 'primary',
                onClick: [{
                    action: 'confirm',
                    title: '修改立即支付',
                    message: '确定要修改立即支付吗？',
                    confirm: [{
                        var: 'value',
                        target: '$widget',
                        action: 'call',
                        method: 'getValue'
                    }, {
                        action: 'service',
                        path: 'shop/order/list/save',
                        params: {
                            '...':[{
                                id:'$vars.value.id',
                                paid:1,
                                is_backend:1,
                            }]
                        },
                    }, {
                        action: 'dialog-hide',
                        mode: 'apply'
                    }]
                }]
            }],
        }]
    }]
} ;
