import {
    input,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'订单发送货',
    dialog:{
        width:600,
        type:'dialog'
    },
    onMounted:[{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params'
    }],
    items:[{
        region: 'center',
        layout:'vertical',
        items:[{
            id: 'form',
            widget: 'form',
            fields:[{
                class: input,
                itemId: 'shipping_type',
                widget: 'form-radio',
                value: 1,
                options: [{
                    value: 1,
                    text: '快递'
                }, {
                    value: 2,
                    text: '门店自提'
                }],
                label:'配送方式',
            },{
                class: input,
                itemId: 'delivery_type',
                widget: 'form-radio',
                value: 1,
                options: [{
                    value: "express",
                    text: '手动填写'
                }, {
                    value: "express",
                    text: '电子面单打印'
                }],
                label:'发货类型',
            },{
                class: input,
                itemId: 'delivery_name',
                placeholder:'请选择快递公司',
                widget:'select',
                label:'快递公司',
                dictionary:"shop/common/static/express"
            },{
                class: input,
                itemId: 'delivery_id',
                placeholder:'请输入快递单号',
                widget:'text',
                label:'快递单号',
            }],
            buttons: [{
                value: '保存',
                type: 'primary',
                onClick: [{
                    action: 'confirm',
                    title: '表单保存',
                    message: '是否保存表单?',
                    confirm: [{
                        var: 'value',
                        target: '$widget',
                        action: 'call',
                        method: 'getValue'
                    }, {
                        action: 'service',
                        path: 'shop/order/list/save',
                        params: {
                            '...':[
                                '$vars.value',
                                {
                                    state:1,
                                    is_backend:1,
                                }
                            ]
                        }
                    }, {
                        action: 'dialog-hide',
                        mode: 'apply'
                    }]
                }]
            }],
        }]
    }]
} ;
