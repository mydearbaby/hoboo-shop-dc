import {
    center,
    input,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'新增编辑',
    dialog:{
        size: '50%',
    },
    onMounted: [{
        action:'print',
        value:'$params'
    },{
        action:'set',
        target: '$view',
        field:'title',
        value:'$params.title'
    },{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params.form'
    }],
    items:[{
        region: 'center',
        class: center,
        layout:'vertical',
        items:[{
            id: 'form',
            widget: 'form',
            layout: {
                type:'gridform',
                column:2
            },
            fields:[{
                class: input,
                itemId: 'product_id',
                placeholder:'请输入直播间名称',
                widget:'text',
                label:'直播间名称',
            },{
                class: input,
                itemId: 'cover_img',
                placeholder:'请输入背景图',
                widget:'text',
                label:'背景图',
            },{
                class: input,
                itemId: 'share_img',
                placeholder:'请输入分享图',
                widget:'text',
                label:'分享图',
            },{
                class: input,
                itemId: 'anchor_name',
                placeholder:'请输入主播昵称',
                widget:'text',
                label:'主播昵称',
            },{
                class: input,
                itemId: 'anchor_wechat',
                placeholder:'请输入主播微信号',
                widget:'text',
                label:'主播微信号',
            },{
                class: input,
                itemId: 'phone',
                placeholder:'请输入主播手机号',
                widget:'text',
                label:'主播手机号',
            },{
                class: input,
                itemId: 'start_time',
                placeholder:'请选择直播开始时间',
                widget:'form-datepicker',
                type: 'date',
                label:'直播开始时间',
            },{
                class: input,
                itemId: 'end_time',
                placeholder:'请选择直播结束时间',
                widget:'form-datepicker',
                type: 'date',
                label:'直播结束时间',
            },{
                class: input,
                itemId: 'live_status',
                widget:'select',
                dictionary: 'shop/common/static/live_status',
                label:'直播状态',
            },{
                class: input,
                itemId: 'close_like',
                widget: 'form-radio',
                options: [{
                    value: 1,
                    text: '是'
                }, {
                    value: 0,
                    text: '否'
                }],
                label:'是否关闭点赞',
            },{
                class: input,
                itemId: 'close_goods',
                widget: 'form-radio',
                options: [{
                    value: 1,
                    text: '是'
                }, {
                    value: 0,
                    text: '否'
                }],
                label:'是否关闭货架',
            },{
                class: input,
                itemId: 'close_comment',
                widget: 'form-radio',
                options: [{
                    value: 1,
                    text: '是'
                }, {
                    value: 0,
                    text: '否'
                }],
                label:'是否关闭评论',
            }],
            buttons: [{
                value: '保存',
                type: 'primary',
                onClick: [{
                    action: 'confirm',
                    title: '表单保存',
                    message: '是否保存表单?',
                    confirm: [{
                        var:'value',
                        target:'$widget',
                        action:'call',
                        method:'getValue'
                    },{
                        action: 'service',
                        path: 'shop/market/broadcast/room/save',
                        params: '$vars.value'
                    },{
                        action:'dialog-hide',
                        mode:'apply'
                    }]
                }]
            }],
        }]
    }]
} ;
