import {
    center,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'直播间详情',
    dialog:{
        size:'50%'
    },
    onMounted: [{
        action: 'set',
        target: '$widgets.formbasic',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'formbasic',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:2
                },
                fields:[{
                    itemId:'product_id',
                    widget:'form-text',
                    label:'直播间名称',
                },{
                    itemId:'cover_img',
                    widget:'form-text',
                    label:'背景图',
                },{
                    itemId: 'share_img',
                    widget:'form-text',
                    label:'分享图',
                },{
                    itemId: 'anchor_name',
                    widget:'form-text',
                    label:'主播昵称',
                },{
                    itemId: 'anchor_wechat',
                    widget:'form-text',
                    label:'主播微信号',
                },{
                    itemId: 'phone',
                    widget:'form-text',
                    label:'主播手机号',
                },{
                    itemId: 'start_time',
                    widget:'form-text',
                    label:'直播开始时间',
                },{
                    itemId: 'end_time',
                    widget:'form-text',
                    label:'直播结束时间',
                },{
                    itemId: 'live_status',
                    widget:'select',
                    print: true,
                    dictionary: 'shop/common/static/live_status',
                    label:'直播状态',
                },{
                    itemId: 'close_like',
                    widget:'select',
                    print: true,
                    dictionary: 'shop/common/static/whether',
                    label:'是否关闭点赞',
                },{
                    itemId: 'close_goods',
                    widget:'select',
                    print: true,
                    dictionary: 'shop/common/static/whether',
                    label:'是否关闭货架',
                },{
                    itemId: 'close_comment',
                    widget:'select',
                    print: true,
                    dictionary: 'shop/common/static/whether',
                    label:'是否关闭评论',
                }],
                flex: true
            }]
        }]
    }]
};


