import {
    center,
    input,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'新增编辑',
    dialog:{
        size: '50%',
    },
    onMounted: [{
        action:'print',
        value:'$params'
    },{
        action:'set',
        target: '$view',
        field:'title',
        value:'$params.title'
    },{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params.form'
    }],
    items:[{
        region: 'center',
        class: center,
        layout:'vertical',
        items:[{
            id: 'form',
            widget: 'form',
            layout: {
                type:'gridform',
                column:2
            },
            fields:[{
                class: input,
                itemId: 'product_id',
                placeholder:'请输入商品',
                widget:'select',
                dictionary: 'shop/common/dictionary/collage_product',
                label:'商品',
            },{
                class: input,
                itemId: 'mer_id',
                placeholder:'请输入商户',
                widget:'select',
                dictionary: 'shop/common/dictionary/user',
                label:'商户',
            },{
                class: input,
                itemId: 'image',
                placeholder:'请输入推荐图',
                widget:'text',
                label:'推荐图',
            },{
                class: input,
                itemId: 'images',
                placeholder:'请输入轮播图',
                widget:'text',
                label:'轮播图',
            },{
                class: input,
                itemId: 'title',
                placeholder:'请输入拼团名称',
                widget:'text',
                label:'拼团名称',
            },{
                class: input,
                itemId: 'attr',
                placeholder:'请输入拼团属性',
                widget:'text',
                label:'拼团属性',
            },{
                class: input,
                itemId: 'people',
                placeholder:'请输入参团人数',
                widget:'text',
                label:'参团人数',
            },{
                class: input,
                itemId: 'info',
                placeholder:'请输入简介',
                widget:'text',
                label:'简介',
            },{
                class: input,
                itemId: 'price',
                placeholder:'请输入价格',
                widget:'text',
                label:'价格',
            },{
                class: input,
                itemId: 'sort',
                placeholder:'请输入排序',
                widget:'text',
                label:'排序',
            },{
                class: input,
                itemId: 'stock',
                placeholder:'请输入库存',
                widget:'text',
                label:'库存',
            },{
                class: input,
                itemId: 'sales',
                placeholder:'请输入销量',
                widget:'text',
                label:'销量',
            },{
                class: input,
                itemId: 'postage',
                placeholder:'请输入邮费',
                widget:'text',
                label:'邮费',
            },{
                class: input,
                itemId: 'start_time',
                placeholder:'请选择拼团开始时间',
                widget:'form-datepicker',
                type: 'date',
                label:'拼团开始时间',
            },{
                class: input,
                itemId: 'stop_time',
                placeholder:'请选择拼团结束时间',
                widget:'form-datepicker',
                type: 'date',
                label:'拼团结束时间',
            },{
                class: input,
                itemId: 'effective_time',
                placeholder:'请选择拼团订单有效时间',
                widget:'form-datepicker',
                type: 'date',
                label:'订单有效时间',
            },{
                class: input,
                itemId: 'cost',
                placeholder:'请输入成本价',
                widget:'text',
                label:'成本价',
            },{
                class: input,
                itemId: 'browse',
                placeholder:'请输入浏览量',
                widget:'text',
                label:'浏览量',
            },{
                class: input,
                itemId: 'unit_name',
                placeholder:'请输入单位名',
                widget:'text',
                label:'单位名',
            },{
                class: input,
                itemId: 'num',
                placeholder:'请输入单次限购(个)',
                widget:'text',
                label:'单次限购(个)',
            },{
                class: input,
                itemId: 'weight',
                placeholder:'请输入商品重量',
                widget:'text',
                label:'商品重量',
            },{
                class: input,
                itemId: 'volume',
                placeholder:'请输入商品体积',
                widget:'text',
                label:'商品体积',
            },{
                class: input,
                itemId: 'quota',
                placeholder:'请输入限购总数',
                widget:'text',
                label:'限购总数',
            },{
                class: input,
                itemId: 'once_num',
                placeholder:'请输入每个订单可购买数量',
                widget:'text',
                label:'每个订单限购(个)',
            },{
                class: input,
                itemId: 'is_postage',
                widget: 'form-radio',
                options: [{
                    value: 1,
                    text: '是'
                }, {
                    value: 0,
                    text: '否'
                }],
                label:'是否包邮',
            },{
                class: input,
                itemId: 'is_show',
                widget: 'form-radio',
                options: [{
                    value: 1,
                    text: '是'
                }, {
                    value: 0,
                    text: '否'
                }],
                label:'显示',
            },{
                class: input,
                itemId: 'state',
                widget: 'form-radio',
                options: [{
                    value: 1,
                    text: '启用'
                }, {
                    value: 0,
                    text: '禁用'
                }],
                label:'状态',
            }],
            buttons: [{
                value: '保存',
                type: 'primary',
                onClick: [{
                    action: 'confirm',
                    title: '表单保存',
                    message: '是否保存表单?',
                    confirm: [{
                        var:'value',
                        target:'$widget',
                        action:'call',
                        method:'getValue'
                    },{
                        action: 'service',
                        path: 'shop/market/collage/commodity/save',
                        params: '$vars.value'
                    },{
                        action:'dialog-hide',
                        mode:'apply'
                    }]
                }]
            }],
        }]
    }]
} ;
