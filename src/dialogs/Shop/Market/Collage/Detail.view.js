import {
    center,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'拼团详情',
    dialog:{
        size:'50%'
    },
    onMounted: [{
        action: 'set',
        target: '$widgets.formbasic',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'formbasic',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:2
                },
                fields:[{
                    itemId:'product_id',
                    widget:'form-text',
                    label:'商品',
                },{
                    itemId:'mer_id',
                    widget:'select',
                    print: true,
                    dictionary: 'shop/common/dictionary/user',
                    label:'商户',
                },{
                    itemId:'image',
                    widget:'form-text',
                    label:'推荐图',
                },{
                    itemId:'images',
                    widget:'form-text',
                    label:'轮播图',
                },{
                    itemId: 'title',
                    widget:'form-text',
                    label:'拼团名称',
                },{
                    itemId: 'attr',
                    widget:'form-text',
                    label:'拼团属性',
                },{
                    itemId: 'people',
                    widget:'form-text',
                    label:'参团人数',
                },{
                    itemId: 'info',
                    widget:'form-text',
                    label:'简介',
                },{
                    itemId: 'price',
                    widget:'form-text',
                    label:'价格',
                },{
                    itemId: 'sort',
                    widget:'form-text',
                    label:'排序',
                },{
                    itemId: 'stock',
                    widget:'form-text',
                    label:'库存',
                },{
                    itemId: 'sales',
                    widget:'form-text',
                    label:'销量',
                },{
                    itemId: 'postage',
                    widget:'form-text',
                    label:'邮费',
                },{
                    itemId: 'start_time',
                    widget:'form-text',
                    label:'拼团开始时间',
                },{
                    itemId: 'stop_time',
                    widget:'form-text',
                    label:'拼团结束时间',
                },{
                    itemId: 'effective_time',
                    widget:'form-text',
                    label:'订单有效时间',
                },{
                    itemId: 'cost',
                    widget:'form-text',
                    label:'成本价',
                },{
                    itemId: 'browse',
                    widget:'form-text',
                    label:'浏览量',
                },{
                    itemId: 'unit_name',
                    widget:'form-text',
                    label:'单位名',
                },{
                    itemId: 'num',
                    widget:'form-text',
                    label:'单次限购(个)',
                },{
                    itemId: 'weight',
                    widget:'form-text',
                    label:'商品重量',
                },{
                    itemId: 'volume',
                    widget:'form-text',
                    label:'商品体积',
                },{
                    itemId: 'quota',
                    widget:'form-text',
                    label:'限购总数',
                },{
                    itemId: 'once_num',
                    widget:'form-text',
                    label:'每个订单限购(个)',
                },{
                    itemId: 'is_postage',
                    widget:'select',
                    print: true,
                    dictionary: 'shop/common/static/whether',
                    label:'是否包邮',
                },{
                    itemId: 'is_show',
                    widget:'select',
                    print: true,
                    dictionary: 'shop/common/static/whether',
                    label:'显示',
                },{
                    itemId: 'state',
                    widget:'select',
                    print: true,
                    dictionary: 'common/static/state',
                    label:'状态',
                }],
                flex: true
            }]
        }]
    }]
};


