import {
    center,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'优惠券详情',
    dialog:{
        size:'50%'
    },
    onMounted: [{
        action: 'set',
        target: '$widgets.formbasic',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'formbasic',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:2
                },
                fields:[{
                    itemId:'product_id',
                    widget:'form-text',
                    label:'商品',
                },{
                    itemId:'category_id',
                    widget:'form-text',
                    label:'商品分类',
                },{
                    itemId:'coupon_title',
                    widget:'form-text',
                    label:'优惠券名称',
                },{
                    itemId: 'start_time',
                    widget:'form-text',
                    label: '开始领取时间',
                },{
                    itemId: 'end_time',
                    widget:'form-text',
                    label: '结束领取时间',
                },{
                    itemId: 'total_count',
                    widget:'form-text',
                    label: '领取数量',
                },{
                    itemId: 'remain_count',
                    widget:'form-text',
                    label: '剩余领取数量',
                },{
                    itemId: 'full_reduction',
                    widget:'form-text',
                    label: '消费满多少赠送优惠券',
                },{
                    itemId: 'integral',
                    widget:'form-text',
                    label: '兑换消耗积分值',
                },{
                    itemId: 'coupon_price',
                    widget:'form-text',
                    label: '兑换的优惠券面值',
                },{
                    itemId: 'use_min_price',
                    widget:'form-text',
                    label: '最低消费',
                },{
                    itemId: 'coupon_time',
                    widget:'form-text',
                    label: '优惠券有效期限(天)',
                },{
                    itemId: 'start_use_time',
                    widget:'form-text',
                    label: '开始使用时间',
                },{
                    itemId: 'end_use_time',
                    widget:'form-text',
                    label: '结束使用时间',
                },{
                    itemId: 'sci_type',
                    widget:'select',
                    print: true,
                    dictionary: 'shop/common/static/coupon_type',
                    label: '优惠券类型',
                },{
                    itemId: 'receive_type',
                    widget:'select',
                    print: true,
                    dictionary: 'shop/common/static/receive_type',
                    label: '领取方式',
                }],
                flex: true
            }]
        }]
    }]
};


