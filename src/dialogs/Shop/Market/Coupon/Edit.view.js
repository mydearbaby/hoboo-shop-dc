import {
    center,
    input,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'新增编辑',
    dialog:{
        size: '50%',
    },
    onMounted: [{
        action:'print',
        value:'$params'
    },{
        action:'set',
        target: '$view',
        field:'title',
        value:'$params.title'
    },{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params.form'
    }],
    items:[{
        region: 'center',
        class: center,
        layout:'vertical',
        items:[{
            id: 'form',
            widget: 'form',
            layout: {
                type:'gridform',
                column:2
            },
            fields:[{
                class: input,
                itemId: 'product_id',
                placeholder:'请输入商品',
                widget:'select',
                dictionary: 'shop/common/dictionary/product',
                label:'商品',
            },{
                class: input,
                itemId: 'category_id',
                placeholder:'请输入商品分类',
                widget:'select',
                dictionary: 'shop/common/dictionary/category',
                label:'商品分类',
            },{
                class: input,
                itemId: 'coupon_title',
                placeholder:'请输入优惠券名称',
                widget:'text',
                label:'优惠券名称',
            },{
                class: input,
                itemId: 'start_time',
                placeholder:'请选择开始领取时间',
                widget:'form-datepicker',
                type: 'date',
                label:'开始领取时间',
            },{
                class: input,
                itemId: 'end_time',
                placeholder:'请选择结束领取时间',
                widget:'form-datepicker',
                type: 'date',
                label:'结束领取时间',
            },{
                class: input,
                itemId: 'total_count',
                placeholder:'请输入领取数量',
                widget:'text',
                label:'领取数量',
            },{
                class: input,
                itemId: 'remain_count',
                placeholder:'请输入剩余领取数量',
                widget:'text',
                label:'剩余领取数量',
            },{
                class: input,
                itemId: 'full_reduction',
                placeholder:'请输入消费满多少赠送优惠券',
                widget:'text',
                label:'消费满多少赠送优惠券',
            },{
                class: input,
                itemId: 'integral',
                placeholder:'请输入兑换消耗积分值',
                widget:'text',
                label:'兑换消耗积分值',
            },{
                class: input,
                itemId: 'coupon_price',
                placeholder:'请输入兑换的优惠券面值',
                widget:'text',
                label:'兑换的优惠券面值',
            },{
                class: input,
                itemId: 'use_min_price',
                placeholder:'请输入最低消费多少金额可用优惠券',
                widget:'text',
                label:'最低消费',
            },{
                class: input,
                itemId: 'coupon_time',
                placeholder:'请输入优惠券有效期限(天)',
                widget:'text',
                label:'优惠券有效期限(天)',
            },{
                class: input,
                itemId: 'start_use_time',
                placeholder:'请选择开始使用时间',
                widget:'form-datepicker',
                type: 'date',
                label:'开始使用时间',
            },{
                class: input,
                itemId: 'end_use_time',
                placeholder:'请选择结束使用时间',
                widget:'form-datepicker',
                type: 'date',
                label:'结束使用时间',
            },{
                class: input,
                itemId: 'sci_type',
                placeholder:'请选择优惠券类型',
                widget:'select',
                label: '优惠券类型',
                dictionary: 'shop/common/static/coupon_type'
            },{
                class: input,
                itemId: 'receive_type',
                placeholder:'请选择领取方式',
                widget:'select',
                dictionary: 'shop/common/static/receive_type',
                label:'领取方式',
            },{
                class: input,
                itemId: 'is_permanent',
                widget: 'form-radio',
                options: [{
                    value: 1,
                    text: '是'
                }, {
                    value: 0,
                    text: '否'
                }],
                label:'是否无限张数',
            },{
                class: input,
                itemId: 'is_give_subscribe',
                widget: 'form-radio',
                options: [{
                    value: 1,
                    text: '是'
                }, {
                    value: 0,
                    text: '否'
                }],
                label:'是否首次关注赠送',
            },{
                class: input,
                itemId: 'is_full_give',
                widget: 'form-radio',
                options: [{
                    value: 1,
                    text: '是'
                }, {
                    value: 0,
                    text: '否'
                }],
                label:'是否满赠',
            }],
            buttons: [{
                value: '保存',
                type: 'primary',
                onClick: [{
                    action: 'confirm',
                    title: '表单保存',
                    message: '是否保存表单?',
                    confirm: [{
                        var:'value',
                        target:'$widget',
                        action:'call',
                        method:'getValue'
                    },{
                        action: 'service',
                        path: 'shop/market/coupon/list/save',
                        params: '$vars.value'
                    },{
                        action:'dialog-hide',
                        mode:'apply'
                    }]
                }]
            }],
        }]
    }]
} ;
