import {
    input,
    grid,
    centerHeader,
    formTimeTop
} from '@/css/DialogsCss.module.scss';
export default {
    title:'新增编辑',
    dialog:{
        size:'50%'
    },
    onMounted: [{
        action:'print',
        value:'$params'
    },{
        action:'set',
        target: '$view',
        field:'title',
        value:'$params.title'
    },{
        action: 'set',
        target: '$widgets.basic',
        field: 'value',
        value: '$params.form'
    },{
        action: 'set',
        target: '$widgets.spec',
        field: 'value',
        value: {
            image: '$params.form.image',
            images: '$params.form.images',
            title: '$params.form.title',
            info: '$params.form.info',
            price: '$params.form.price',
            cost: '$params.form.cost',
            give_integral: '$params.form.give_integral',
            stock: '$params.form.stock',
            sales: '$params.form.sales',
            unit_name: '$params.form.unit_name',
            postage: '$params.form.postage',
            description: '$params.form.description',
            start_time: '$params.form.start_time',
            stop_time: '$params.form.stop_time',
            num: '$params.form.num',
            weight: '$params.form.weight',
            volume: '$params.form.volume',
            quota: '$params.form.quota',
            once_num: '$params.form.once_num',
            is_postage: '$params.form.is_postage',
            is_hot: '$params.form.is_hot',
            is_show: '$params.form.is_show',
            state: '$params.form.state',
        }
    },{
        action: 'set',
        target: '$widgets.info',
        field: 'value',
        value: {
            store_info: '$params.form.store_info',
        }
    }],
    items: [{
        region: 'center',
        items:[{
            flex:1,
            layout:'step',
            margin:20,
            id:'stepContainer',
            items:[{
                title:'选择秒杀商品',
                items:[{
                    widget:'form',
                    id:'basic',
                    class: grid,
                    fields:[{
                        class: input,
                        itemId:'product_id',
                        widget:'select',
                        dictionary: 'shop/common/dictionary/product',
                        label:'选择商品',
                    }]
                },{
                    widget:'button',
                    type: 'primary',
                    value:'下一步',
                    onClick:[{
                        action:'step-next',
                        target:'$widgets.stepContainer'
                    }]
                }]
            },{
                title:'填写基础信息',
                items:[{
                    widget:'form',
                    id:'spec',
                    layout: {
                        type:'gridform',
                        column:2
                    },
                    class: grid,
                    fields:[{
                        class: input,
                        itemId: 'image',
                        placeholder:'请输入推荐图',
                        widget:'text',
                        label:'推荐图',
                    },{
                        class: input,
                        itemId: 'images',
                        placeholder:'请输入轮播图',
                        widget:'text',
                        label:'轮播图',
                    },{
                        class: input,
                        itemId: 'title',
                        placeholder:'请输入活动名称',
                        widget:'text',
                        label:'活动名称',
                    },{
                        class: input,
                        itemId: 'info',
                        placeholder:'请输入活动简介',
                        widget:'text',
                        label:'活动简介',
                    },{
                        class: input,
                        itemId: 'price',
                        placeholder:'请输入价格',
                        widget:'text',
                        label:'价格',
                    },{
                        class: input,
                        itemId: 'cost',
                        placeholder:'请输入原价',
                        widget:'text',
                        label:'原价',
                    },{
                        class: input,
                        itemId: 'give_integral',
                        placeholder:'请输入返多少积分',
                        widget:'text',
                        label:'返多少积分',
                    },{
                        class: input,
                        itemId: 'stock',
                        placeholder:'请输入库存',
                        widget:'text',
                        label:'库存',
                    },{
                        class: input,
                        itemId: 'sales',
                        placeholder:'请输入销量',
                        widget:'text',
                        label:'销量',
                    },{
                        class: input,
                        itemId: 'unit_name',
                        placeholder:'请输入单位',
                        widget:'select',
                        dictionary: 'shop/common/dictionary/unit',
                        label:'单位',
                    },{
                        class: input,
                        itemId: 'postage',
                        placeholder:'请输入邮费',
                        widget:'text',
                        label:'邮费',
                    },{
                        class: input,
                        itemId: 'description',
                        placeholder:'请输入内容',
                        widget:'text',
                        label:'内容',
                    },{
                        class: input,
                        itemId: 'start_time',
                        placeholder:'请选择开始时间',
                        widget:'form-datepicker',
                        type: 'date',
                        label:'开始时间',
                    },{
                        class: input,
                        itemId: 'stop_time',
                        placeholder:'请选择结束时间',
                        widget:'form-datepicker',
                        type: 'date',
                        label:'结束时间',
                    },{
                        class: input,
                        itemId: 'num',
                        placeholder:'请输入限购(个)',
                        widget:'text',
                        label:'限购(个)',
                    },{
                        class: input,
                        itemId: 'weight',
                        placeholder:'请输入商品重量',
                        widget:'text',
                        label:'商品重量',
                    },{
                        class: input,
                        itemId: 'volume',
                        placeholder:'请输入商品体积',
                        widget:'text',
                        label:'商品体积',
                    },{
                        class: input,
                        itemId: 'quota',
                        placeholder:'请输入限购总数',
                        widget:'text',
                        label:'限购总数',
                    },{
                        class: input,
                        itemId: 'once_num',
                        placeholder:'请输入单次购买个数',
                        widget:'text',
                        label:'单次购买个数',
                    },{
                        class: input,
                        itemId: 'is_postage',
                        widget: 'form-radio',
                        options: [{
                            value: 1,
                            text: '是'
                        }, {
                            value: 0,
                            text: '否'
                        }],
                        label:'是否包邮',
                    },{
                        class: input,
                        itemId: 'is_hot',
                        widget: 'form-radio',
                        options: [{
                            value: 1,
                            text: '是'
                        }, {
                            value: 0,
                            text: '否'
                        }],
                        label:'热门推荐',
                    },{
                        class: input,
                        itemId: 'is_show',
                        widget: 'form-radio',
                        options: [{
                            value: 1,
                            text: '是'
                        }, {
                            value: 0,
                            text: '否'
                        }],
                        label:'显示',
                    },{
                        class: input,
                        itemId: 'state',
                        widget: 'form-radio',
                        options: [{
                            value: 1,
                            text: '启用'
                        }, {
                            value: 0,
                            text: '禁用'
                        }],
                        label:'状态',
                    }]
                },{
                    layout:'horizontal',
                    class: formTimeTop,
                    items:[{
                        class: centerHeader,
                        widget:'button',
                        type: 'warning',
                        value:'上一步',
                        onClick:[{
                            action:'step-previous',
                            target:'$widgets.stepContainer'
                        }]
                    },{
                        widget:'button',
                        type: 'primary',
                        value:'下一步',
                        onClick:[{
                            action:'step-next',
                            target:'$widgets.stepContainer'
                        }]
                    }]
                }]
            },{
                title:'修改商品详情',
                items:[{
                    widget:'form',
                    id:'info',
                    class: grid,
                    fields:[{
                        class: input,
                        itemId:'store_info',
                        widget:'text',
                        label:'商品详情',
                    }]
                },{
                    layout:'horizontal',
                    items:[{
                        class: centerHeader,
                        widget:'button',
                        type: 'warning',
                        value:'上一步',
                        onClick:[{
                            action:'step-previous',
                            target:'$widgets.stepContainer'
                        }]
                    },{
                        widget:'button',
                        type: 'primary',
                        value:'完成',
                        onClick:[{
                            action:'print',
                            value:{
                                '...':[
                                    '$widgets.basic.value',
                                    '$widgets.spec.value',
                                    '$widgets.info.value',
                                ]
                            },
                        },{
                            action:'service',
                            path: 'shop/market/seckill/commodity/save',
                            params:{
                                '...':[
                                    '$widgets.basic.value',
                                    '$widgets.spec.value',
                                    '$widgets.info.value',
                                ]
                            },
                        }, {
                            action: 'dialog-hide',
                            mode: 'apply'
                        }]
                    }]
                }]
            }]
        }]
    }]
};


