import {
    center,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'秒杀详情',
    dialog:{
        size:'50%'
    },
    onMounted: [{
        action: 'set',
        target: '$widgets.formbasic',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'formbasic',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:2
                },
                fields:[{
                    itemId:'product_id',
                    widget:'form-text',
                    label:'商品',
                },{
                    itemId:'image',
                    widget:'form-text',
                    label:'推荐图',
                },{
                    itemId: 'title',
                    widget:'form-text',
                    label:'活动名称',
                },{
                    itemId: 'info',
                    widget:'form-text',
                    label:'简介',
                },{
                    itemId: 'price',
                    widget:'form-text',
                    label:'价格',
                },{
                    itemId: 'cost',
                    widget:'form-text',
                    label:'成本',
                },{
                    itemId: 'ot_price',
                    widget:'form-text',
                    label:'原价',
                },{
                    itemId: 'give_integral',
                    widget:'form-text',
                    label:'返多少积分',
                },{
                    itemId: 'sort',
                    widget:'form-text',
                    label:'排序',
                },{
                    itemId: 'stock',
                    widget:'form-text',
                    label:'库存',
                },{
                    itemId: 'sales',
                    widget:'form-text',
                    label:'销量',
                },{
                    itemId: 'unit_name',
                    widget:'form-text',
                    label:'单位名',
                },{
                    itemId: 'postage',
                    widget:'form-text',
                    label:'邮费',
                },{
                    itemId: 'description',
                    widget:'form-text',
                    label:'内容',
                },{
                    itemId: 'start_time',
                    widget:'form-text',
                    label:'开始时间',
                },{
                    itemId: 'stop_time',
                    widget:'form-text',
                    label:'结束时间',
                },{
                    itemId: 'num',
                    widget:'form-text',
                    label:'限购(个)',
                },{
                    itemId: 'weight',
                    widget:'form-text',
                    label:'商品重量',
                },{
                    itemId: 'volume',
                    widget:'form-text',
                    label:'商品体积',
                },{
                    itemId: 'quota',
 
                    widget:'form-text',
                    label:'限购总数',
                },{
                    itemId: 'once_num',
                    widget:'form-text',
                    label:'单次购买个数',
                },{
                    itemId: 'is_postage',
                    widget:'select',
                    print: true,
                    dictionary: 'shop/common/static/whether',
                    label:'是否包邮',
                },{
                    itemId: 'is_hot',
                    widget:'select',
                    print: true,
                    dictionary: 'shop/common/static/whether',
                    label:'热门推荐',
                },{
                    itemId: 'is_show',
                    widget:'select',
                    print: true,
                    dictionary: 'shop/common/static/whether',
                    label:'显示',
                },{
                    itemId: 'state',
                    widget:'select',
                    print: true,
                    dictionary: 'common/static/state',
                    label:'状态',
                }],
                flex: true
            }]
        }]
    }]
};


