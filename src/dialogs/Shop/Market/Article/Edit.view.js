import {
    center,
    input,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'新增编辑',
    dialog:{
        size: 750,
    },
    onMounted: [{
        action:'print',
        value:'$params'
    },{
        action:'set',
        target: '$view',
        field:'title',
        value:'$params.title'
    },{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params.form'
    }],
    items:[{
        region: 'center',
        class: center,
        layout:'vertical',
        items:[{
            id: 'form',
            widget: 'form',
            layout: {
                type:'gridform',
                column:1
            },
            fields:[{
                class: input,
                itemId: 'title',
                placeholder:'请输入文章标题',
                widget:'text',
                label:'文章标题',
            },{
                class: input,
                itemId: 'author',
                placeholder:'请输入作者',
                widget:'text',
                label:'作者',
            },{
                class: input,
                itemId: 'synopsis',
                placeholder:'请输入文章简介',
                widget:'text',
                label:'文章简介',
            },{
                class: input,
                itemId: 'share_title',
                placeholder:'请输入分享标题',
                widget:'text',
                label:'分享标题',
            },{
                class: input,
                itemId: 'share_synopsis',
                placeholder:'请输入分享简介',
                widget:'text',
                label:'分享简介',
            },{
                class: input,
                itemId: 'visit',
                placeholder:'请输入浏览次数',
                widget:'text',
                label:'浏览次数',
            }],
            buttons: [{
                value: '保存',
                type: 'primary',
                onClick: [{
                    action: 'confirm',
                    title: '表单保存',
                    message: '是否保存表单?',
                    confirm: [{
                        var:'value',
                        target:'$widget',
                        action:'call',
                        method:'getValue'
                    },{
                        action: 'service',
                        path: 'shop/market/article/list/save',
                        params: '$vars.value'
                    },{
                        action:'dialog-hide',
                        mode:'apply'
                    }]
                }]
            }],
        }]
    }]
} ;
