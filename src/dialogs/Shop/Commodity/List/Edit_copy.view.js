import {
    input,
    grid,
    centerHeader
} from '@/css/DialogsCss.module.scss';
export default {
    title:'新增商品',
    dialog:{
        size:'50%'
    },
    onMounted: [{
        action:'print',
        value:'$params'
    },{
        action:'set',
        target: '$view',
        field:'title',
        value:'$params.title'
    },{
        action: 'set',
        target: '$widgets.basic',
        field: 'value',
        value: '$params.form'
    },{
        action: 'set',
        target: '$widgets.spec',
        field: 'value',
        value: {
            spec_type: '$params.form.spec_type',
            image: '$params.form.image',
            ot_price: '$params.form.ot_price',
            cost: '$params.form.cost',
            price: '$params.form.price',
            stock: '$params.form.stock',
        }
    },{
        action: 'set',
        target: '$widgets.info',
        field: 'value',
        value: {
            store_info: '$params.form.store_info',
        }
    },{
        action: 'set',
        target: '$widgets.logistics',
        field: 'value',
        value: {
            is_postage: '$params.form.is_postage',
            postage: '$params.form.postage',
        }
    },{
        action: 'set',
        target: '$widgets.market',
        field: 'value',
        value: {
            ficti: '$params.form.ficti',
            sort: '$params.form.sort',
            give_integral: '$params.form.give_integral',
            is_vip: '$params.form.is_vip',
            is_hot: '$params.form.is_hot',
            is_benefit: '$params.form.is_benefit',
            is_best: '$params.form.is_best',
            is_new: '$params.form.is_new',
            is_good: '$params.form.is_good',
        }
    },{
        action: 'set',
        target: '$widgets.other',
        field: 'value',
        value: {
            keyword: '$params.form.keyword',
            store_info: '$params.form.store_info',
            command_word: '$params.form.command_word',
            recommend_image: '$params.form.recommend_image',
        }
    }],
    items: [{
        region: 'center',
        items:[{
            flex:1,
            layout:'step',
            margin:20,
            id:'stepContainer',
            items:[{
                title:'基础信息',
                items:[{
                    widget:'form',
                    id:'basic',
                    class: grid,
                    fields:[{
                        class: input,
                        itemId:'cate_id',
                        widget:'select',
                        dictionary: 'shop/common/dictionary/category',
                        label:'商品分类',
                    },{
                        class: input,
                        itemId:'store_name',
                        widget:'text',
                        label:'商品名称',
                    },{
                        class: input,
                        itemId:'brand_id',
                        widget:'select',
                        dictionary: 'shop/common/dictionary/brand',
                        label:'商品品牌',
                    },{
                        class: input,
                        itemId:'unit_name',
                        widget:'select',
                        dictionary: 'shop/common/dictionary/unit',
                        label:'单位',
                    },{
                        class: input,
                        itemId:'bar_code',
                        widget:'text',
                        label:'商品编码',
                    },{
                        class: input,
                        itemId:'slider_image',
                        widget:'text',
                        label:'商品轮播图',
                    },{
                        class: input,
                        itemId:'label_id',
                        widget:'select',
                        label:'商品标签',
                    }]
                },{
                    widget:'button',
                    type: 'primary',
                    value:'下一步',
                    onClick:[{
                        action:'step-next',
                        target:'$widgets.stepContainer'
                    }]
                }]
            },{
                title:'规格库存',
                items:[{
                    widget:'form',
                    id:'spec',
                    class: grid,
                    fields:[{
                        class: input,
                        itemId:'spec_type',
                        widget: 'form-radio',
                        value: 0,
                        options: [{
                            value: 1,
                            text: '多'
                        }, {
                            value: 0,
                            text: '单'
                        }],
                        label:'商品规格',
                    },{
                        class: input,
                        itemId:'image',
                        widget:'text',
                        label:'商品图片',
                    },{
                        class: input,
                        itemId:'ot_price',
                        widget:'text',
                        label:'售价',
                    },{
                        class: input,
                        itemId:'cost',
                        widget:'text',
                        label:'成本价',
                    },{
                        class: input,
                        itemId:'price',
                        widget:'text',
                        label:'原价',
                    },{
                        class: input,
                        itemId:'stock',
                        widget:'text',
                        label:'库存',
                    }]
                },{
                    layout:'horizontal',
                    items:[{
                        class: centerHeader,
                        widget:'button',
                        type: 'warning',
                        value:'上一步',
                        onClick:[{
                            action:'step-previous',
                            target:'$widgets.stepContainer'
                        }]
                    },{
                        widget:'button',
                        type: 'primary',
                        value:'下一步',
                        onClick:[{
                            action:'step-next',
                            target:'$widgets.stepContainer'
                        }]
                    }]
                }]
            },{
                title:'商品详情',
                items:[{
                    widget:'form',
                    id:'info',
                    class: grid,
                    fields:[{
                        class: input,
                        itemId:'store_info',
                        widget:'text',
                        label:'商品详情',
                    }]
                },{
                    layout:'horizontal',
                    items:[{
                        class: centerHeader,
                        widget:'button',
                        type: 'warning',
                        value:'上一步',
                        onClick:[{
                            action:'step-previous',
                            target:'$widgets.stepContainer'
                        }]
                    },{
                        widget:'button',
                        type: 'primary',
                        value:'下一步',
                        onClick:[{
                            action:'step-next',
                            target:'$widgets.stepContainer'
                        }]
                    }]
                }]
            },{
                title:'物流设置',
                items:[{
                    widget:'form',
                    id:'logistics',
                    class: grid,
                    fields:[{
                        class: input,
                        itemId:'is_postage',
                        widget: 'form-radio',
                        value: 0,
                        options: [{
                            value: 0,
                            text: '是'
                        }, {
                            value: 1,
                            text: '否'
                        }],
                        label:'是否包邮',
                    },{
                        class: input,
                        itemId:'postage',
                        widget:'text',
                        label:'邮费',
                    }]
                },{
                    layout:'horizontal',
                    items:[{
                        class: centerHeader,
                        widget:'button',
                        type: 'warning',
                        value:'上一步',
                        onClick:[{
                            action:'step-previous',
                            target:'$widgets.stepContainer'
                        }]
                    },{
                        widget:'button',
                        type: 'primary',
                        value:'下一步',
                        onClick:[{
                            action:'step-next',
                            target:'$widgets.stepContainer'
                        }]
                    }]
                }]
            },{
                title:'营销设置',
                items:[{
                    widget:'form',
                    id:'market',
                    class: grid,
                    fields:[{
                        class: input,
                        itemId:'ficti',
                        widget:'text',
                        label:'虚拟销量',
                    },{
                        class: input,
                        itemId:'sort',
                        widget:'text',
                        label:'排序',
                    },{
                        class: input,
                        itemId:'give_integral',
                        widget:'text',
                        label:'赠送积分',
                    },{
                        class: input,
                        itemId:'is_vip',
                        widget: 'form-radio',
                        value: 0,
                        options: [{
                            value: 0,
                            text: '是'
                        }, {
                            value: 1,
                            text: '否'
                        }],
                        label:'是否开启会员专属',
                    },{
                        class: input,
                        itemId:'is_hot',
                        widget: 'form-radio',
                        value: 0,
                        options: [{
                            value: 0,
                            text: '是'
                        }, {
                            value: 1,
                            text: '否'
                        }],
                        label:'是否热卖',
                    },{
                        class: input,
                        itemId:'is_benefit',
                        widget: 'form-radio',
                        value: 1,
                        options: [{
                            value: 0,
                            text: '是'
                        }, {
                            value: 1,
                            text: '否'
                        }],
                        label:'是否优惠',
                    },{
                        class: input,
                        itemId:'is_best',
                        widget: 'form-radio',
                        value: 0,
                        options: [{
                            value: 0,
                            text: '是'
                        }, {
                            value: 1,
                            text: '否'
                        }],
                        label:'是否精品',
                    },{
                        class: input,
                        itemId:'is_new',
                        widget: 'form-radio',
                        value: 0,
                        options: [{
                            value: 0,
                            text: '是'
                        }, {
                            value: 1,
                            text: '否'
                        }],
                        label:'是否新品',
                    },{
                        class: input,
                        itemId:'is_good',
                        widget: 'form-radio',
                        value: 1,
                        options: [{
                            value: 0,
                            text: '是'
                        }, {
                            value: 1,
                            text: '否'
                        }],
                        label:'是否优品推荐',
                    }]
                },{
                    layout:'horizontal',
                    items:[{
                        class: centerHeader,
                        widget:'button',
                        type: 'warning',
                        value:'上一步',
                        onClick:[{
                            action:'step-previous',
                            target:'$widgets.stepContainer'
                        }]
                    },{
                        widget:'button',
                        type: 'primary',
                        value:'下一步',
                        onClick:[{
                            action:'step-next',
                            target:'$widgets.stepContainer'
                        }]
                    }]
                }]
            },{
                title:'其他设置',
                items:[
                    {
                    widget:'form',
                    id:'other',
                    class: grid,
                    fields:[{
                        class: input,
                        itemId:'keyword',
                        widget:'text',
                        label:'商品关键字',
                    },
                    // {
                    //     class: input,
                    //     itemId:'store_info',
                    //     widget:'text',
                    //     label:'商品简介',
                    // },
                    {
                        class: input,
                        itemId:'command_word',
                        widget:'text',
                        label:'商品口令',
                    },{
                        class: input,
                        itemId:'recommend_image',
                        widget:'text',
                        label:'商品推荐图',
                    }]
                },{
                    layout:'horizontal',
                    items:[{
                        class: centerHeader,
                        widget:'button',
                        type: 'warning',
                        value:'上一步',
                        onClick:[{
                            action:'step-previous',
                            target:'$widgets.stepContainer'
                        }]
                    },{
                        widget:'button',
                        type: 'primary',
                        value:'完成',
                        onClick:[{
                            action:'print',
                            value:{
                                '...':[
                                    '$widgets.basic.value',
                                    '$widgets.spec.value',
                                    '$widgets.info.value',
                                    '$widgets.logistics.value',
                                    '$widgets.market.value',
                                    '$widgets.other.value'
                                ]
                            },
                        },{
                            action:'service',
                            path: 'shop/store/product/save',
                            params:{
                                '...':[
                                    '$widgets.basic.value',
                                    '$widgets.spec.value',
                                    '$widgets.info.value',
                                    '$widgets.logistics.value',
                                    '$widgets.market.value',
                                    '$widgets.other.value'
                                ]
                            },
                        }, {
                            action: 'dialog-hide',
                            mode: 'apply'
                        }]
                    }]
                }]
            }]
        }]
    }]
};


