import {
    center,
    formTitle,
    grid
} from '@/css/DialogsCss.module.scss';
export default {
    title:'商品详情',
    dialog:{
        size:'100%'
    },
    onMounted: [{
        action: 'set',
        target: '$widgets.basic',
        field: 'value',
        value: '$params'
    },{
        action: 'set',
        target: '$widgets.spec',
        field: 'value',
        value: '$params'
    },{
        action: 'set',
        target: '$widgets.info',
        field: 'value',
        value: '$params'
    },{
        action: 'set',
        target: '$widgets.logistics',
        field: 'value',
        value: '$params'
    },{
        action: 'set',
        target: '$widgets.market',
        field: 'value',
        value: '$params'
    },{
        action: 'set',
        target: '$widgets.other',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                class: center,
                items:[{
                    class: formTitle,
                    id: 'title',
                    widget: 'public-title',
                    title: '基础信息',
                },{
                    id: 'basic',
                    widget: 'form',
                    layout: {
                        type:'gridform',
                        column:2
                    },
                    fields:[{
                        itemId:'cate_id',
                        widget:'select',
                        print: true,
                        dictionary: 'shop/common/dictionary/category',
                        label:'商品分类',
                    },{
                        itemId:'store_name',
                        widget:'form-text',
                        label:'商品名称',
                    },{
                        itemId:'brand_id',
                        widget:'select',
                        print: true,
                        dictionary: 'shop/common/dictionary/brand',
                        label:'商品品牌',
                    },{
                        itemId:'unit_name',
                        widget:'select',
                        print: true,
                        dictionary: 'shop/common/dictionary/unit',
                        label:'单位',
                    },{
                        itemId:'bar_code',
                        widget:'form-text',
                        label:'商品编码',
                    },{
                        itemId:'slider_image',
                        widget:'form-text',
                        label:'商品轮播图',
                    },{
                        itemId:'label_id',
                        widget:'form-text',
                        label:'商品标签',
                    }],
                }],
            },{
                class: [center, grid],
                items:[{
                    class: formTitle,
                    id: 'title',
                    widget: 'public-title',
                    title: '规格库存',
                },{
                    id: 'spec',
                    widget: 'form',
                    layout: {
                        type:'gridform',
                        column:2
                    },
                    fields:[{
                        itemId:'spec_type',
                        widget:'select',
                        print: true,
                        dictionary: 'shop/common/static/spec_type',
                        label:'商品规格',
                    },{
                        itemId:'image',
                        widget:'form-text',
                        label:'商品图片',
                    },{
                        itemId:'ot_price',
                        widget:'form-text',
                        label:'售价',
                    },{
                        itemId:'cost',
                        widget:'form-text',
                        label:'成本价',
                    },{
                        itemId:'price',
                        widget:'form-text',
                        label:'原价',
                    },{
                        itemId:'stock',
                        widget:'form-text',
                        label:'库存',
                    }],
                }],
            },{
                class: [center, grid],
                items:[{
                    class: formTitle,
                    id: 'title',
                    widget: 'public-title',
                    title: '商品详情',
                },{
                    id: 'info',
                    widget: 'form',
                    layout: {
                        type:'gridform',
                        column:2
                    },
                    fields:[{
                        itemId:'store_info',
                        widget:'form-text',
                        label:'商品详情',
                    }],
                }],
            },{
                class: [center, grid],
                items:[{
                    class: formTitle,
                    id: 'title',
                    widget: 'public-title',
                    title: '物流设置',
                },{
                    id: 'logistics',
                    widget: 'form',
                    layout: {
                        type:'gridform',
                        column:2
                    },
                    fields:[{
                        itemId:'is_postage',
                        widget:'select',
                        print: true,
                        dictionary: 'shop/common/static/whether',
                        label:'是否包邮',
                    },{
                        itemId:'postage',
                        widget:'form-text',
                        label:'邮费',
                    }],
                }],
            },{
                class: [center, grid],
                items:[{
                    class: formTitle,
                    id: 'title',
                    widget: 'public-title',
                    title: '营销设置',
                },{
                    id: 'market',
                    widget: 'form',
                    layout: {
                        type:'gridform',
                        column:2
                    },
                    fields:[{
                        itemId:'ficti',
                        widget:'form-text',
                        label:'虚拟销量',
                    },{
                        itemId:'sort',
                        widget:'form-text',
                        label:'排序',
                    },{
                        itemId:'give_integral',
                        widget:'form-text',
                        label:'赠送积分',
                    },{
                        itemId:'is_vip',
                        widget:'select',
                        print: true,
                        dictionary: 'shop/common/static/whether',
                        label:'是否开启会员专属',
                    },{
                        itemId:'is_hot',
                        widget:'select',
                        print: true,
                        dictionary: 'shop/common/static/whether',
                        label:'是否热卖',
                    },{
                        itemId:'is_benefit',
                        widget:'select',
                        print: true,
                        dictionary: 'shop/common/static/whether',
                        label:'是否优惠',
                    },{
                        itemId:'is_best',
                        widget:'select',
                        print: true,
                        dictionary: 'shop/common/static/whether',
                        label:'是否精品',
                    },{
                        itemId:'is_new',
                        widget:'select',
                        print: true,
                        dictionary: 'shop/common/static/whether',
                        label:'是否新品',
                    },{
                        itemId:'is_good',
                        widget:'select',
                        print: true,
                        dictionary: 'shop/common/static/whether',
                        label:'是否优品推荐',
                    }],
                }],
            },{
                class: [center, grid],
                items:[{
                    class: formTitle,
                    id: 'title',
                    widget: 'public-title',
                    title: '其他设置',
                },{
                    id: 'other',
                    widget: 'form',
                    layout: {
                        type:'gridform',
                        column:2
                    },
                    fields:[{
                        itemId:'keyword',
                        widget:'form-text',
                        label:'商品关键字',
                    },{
                        itemId:'store_info',
                        widget:'form-text',
                        label:'商品简介',
                    },{
                        itemId:'command_word',
                        widget:'form-text',
                        label:'商品口令',
                    },{
                        itemId:'recommend_image',
                        widget:'form-text',
                        label:'商品推荐图',
                    }]
                }],
            }]
        }]
    }]
};


