import {
    input,
    grid,
    centerHeader,
    center,
    formTitle,
    overflow,
    button,
    bottom,
    input_w,
    content,
    marginT,
    radius,
    padding,
    footer,
    two_footer
} from '@/css/DialogsCss.module.scss';

export default {
    title: '新增商品',
    dialog: {
        size: '100%'
    },
    onMounted: [{
        action: 'print',
        value: '$params'
    }, {
        action: 'set',
        target: '$view',
        field: 'title',
        value: '$params.title'
    }, {
        action: 'set',
        target: '$widgets.basic',
        field: 'value',
        value: '$params.form'
    }, {
        action: 'set',
        target: '$widgets.spec',
        field: 'value',
        value: {
            type_name: '$params.form.type_name',
            image: '$params.form.image',
            ot_price: '$params.form.ot_price',
            cost: '$params.form.cost',
            price: '$params.form.price',
            stock: '$params.form.stock',
            description: '$params.form.description',
        }
    }, {
        action: 'set',
        target: '$widgets.info',
        field: 'value',
        value: {
            description: '$params.form.description'
        }
    }, {
        action: 'set',
        target: '$widgets.logistics',
        field: 'value',
        value: {
            is_postage: '$params.form.is_postage',
            postage: '$params.form.postage',
        }
    }, {
        action: 'set',
        target: '$widgets.market',
        field: 'value',
        value: {
            ficti: '$params.form.ficti',
            sort: '$params.form.sort',
            give_integral: '$params.form.give_integral',
            is_vip: '$params.form.is_vip',
            is_hot: '$params.form.is_hot',
            is_benefit: '$params.form.is_benefit',
            is_best: '$params.form.is_best',
            is_new: '$params.form.is_new',
            is_good: '$params.form.is_good',
        }
    }, {
        action: 'set',
        target: '$widgets.other',
        field: 'value',
        value: {
            keyword: '$params.form.keyword',
            store_info: '$params.form.store_info',
            command_word: '$params.form.command_word',
            recommend_image: '$params.form.recommend_image',
        }
    }],
    items: [{
        region: 'center',
        items: [{
            flex: 1,
            layout: 'step',
            margin: 20,
            id: 'stepContainer',
            items: [
                {
                    title: '基础信息',
                    items: [{
                        widget: 'form',
                        id: 'basic',
                        class: [content, radius],
                        layout: {
                            type: "basicform",
                            labelPosition: "right",
                            labelWidth: 300,
                        },
                        fields: [{
                            class: input_w,
                            itemId: 'cate_id',
                            widget: 'select',
                            dictionary: 'shop/common/dictionary/category',
                            label: '商品分类',
                        }, {
                            class: input_w,
                            itemId: 'store_name',
                            widget: 'text',
                            label: '商品名称',
                        }, {
                            class: input_w,
                            itemId: 'brand_id',
                            widget: 'select',
                            dictionary: 'shop/common/dictionary/brand',
                            label: '商品品牌',
                            width: 300,
                        }, {
                            class: input_w,
                            itemId: 'unit_name',
                            widget: 'select',
                            dictionary: 'shop/common/dictionary/unit',
                            label: '单位',
                            width: 300,
                        }, {
                            class: input_w,
                            itemId: 'bar_code',
                            widget: 'text',
                            label: '商品编码',
                            width: 300,
                        }, {
                            itemId: 'slider_image',
                            widget: 'image-uploader-multiple',
                            label: '商品轮播图',
                        }, {
                            class: input_w,
                            itemId: 'label_id',
                            widget: 'select',
                            label: '商品标签',
                            width: 300,
                        }]
                    }, {
                        class: footer,
                        widget: 'button',
                        type: 'primary',
                        value: '下一步',
                        onClick: [{
                            action: 'step-next',
                            target: '$widgets.stepContainer'
                        }]
                    }]
                },{
                    title: '规格库存',
                    class: overflow,
                    items: [
                        {
                            widget: 'form',
                            id: 'spec',
                            class: [content, radius],
                            layout: {
                                type: "basicform",
                                labelPosition: "right",
                                labelWidth: 300
                            },
                            fields: [{
                                class: input_w,
                                itemId: 'ot_price',
                                widget: 'text',
                                label: '市场价',
                                width: 300,
                            }, {
                                class: input_w,
                                itemId: 'cost',
                                widget: 'text',
                                label: '成本价',
                                width: 300,
                            }, {
                                class: input_w,
                                itemId: 'price',
                                widget: 'text',
                                label: '商品价格',
                                width: 300,
                            }, {
                                class: input_w,
                                itemId: 'stock',
                                widget: 'text',
                                label: '库存',
                                width: 300,
                            }]
                            // {
                            //     widget: 'form',
                            //     id: 'spec',
                            //     class: [grid],
                            //     fields: [{
                            //         class: input,
                            //         itemId: 'type_name',
                            //         widget: 'select',
                            //         dictionary: 'shop/common/dictionary/model',
                            //         label: '商品模型',
                            //     }]
                            // }, {
                            //     class: [center],
                            //     items: [{
                            //         class: formTitle,
                            //         id: 'title',
                            //         widget: 'public-title',
                            //         title: '商品规格',
                            //     }, {
                            //         id: 'specs',
                            //         widget: 'form',
                            //         layout: {
                            //             type: 'gridform',
                            //             column: 1
                            //         },
                            //         fields: [{
                            //             class: input,
                            //             itemId: 'order_id',
                            //             widget: 'form-radio',
                            //             value: 1,
                            //             options: [{
                            //                 value: 1,
                            //                 text: '红色'
                            //             }, {
                            //                 value: 2,
                            //                 text: '黑色'
                            //             }],
                            //             label: '颜色',
                            //         }, {
                            //             class: input,
                            //             itemId: 'total_num',
                            //             widget: 'form-radio',
                            //             value: 2,
                            //             options: [{
                            //                 value: 1,
                            //                 text: 'I5'
                            //             }, {
                            //                 value: 2,
                            //                 text: 'I7'
                            //             }, {
                            //                 value: 3,
                            //                 text: 'I9'
                            //             }],
                            //             label: 'CPU',
                            //         }, {
                            //             class: input,
                            //             itemId: 'total_price',
                            //             widget: 'form-radio',
                            //             value: 1,
                            //             options: [{
                            //                 value: 1,
                            //                 text: '13寸'
                            //             }, {
                            //                 value: 2,
                            //                 text: '15寸'
                            //             }],
                            //             label: '屏幕',
                            //         }],
                            //     }],
                            // }, {
                            //     class: [center, grid],
                            //     items: [{
                            //         class: formTitle,
                            //         id: 'title',
                            //         widget: 'public-title',
                            //         title: '商品属性',
                            //     }, {
                            //         id: 'specs',
                            //         widget: 'form',
                            //         layout: {
                            //             type: 'gridform',
                            //             column: 1
                            //         },
                            //         fields: [{
                            //             class: input,
                            //             itemId: 'order_id',
                            //             widget: 'text',
                            //             label: '包装清单',
                            //         }, {
                            //             class: input,
                            //             itemId: 'total_num',
                            //             widget: 'text',
                            //             label: '型号',
                            //         }, {
                            //             class: input,
                            //             itemId: 'total_price',
                            //             widget: 'form-datepicker',
                            //             type: 'date',
                            //             label: '上市时间',
                            //         }, {
                            //             class: input,
                            //             itemId: 'total_num',
                            //             widget: 'text',
                            //             label: '入网许可证号',
                            //         }, {
                            //             class: input,
                            //             itemId: 'total_num',
                            //             widget: 'text',
                            //             label: '颜色',
                            //         }, {
                            //             class: input,
                            //             itemId: 'total_num',
                            //             widget: 'text',
                            //             label: '重量',
                            //         }, {
                            //             class: input,
                            //             itemId: 'total_num',
                            //             widget: 'text',
                            //             label: '机身长度',
                            //         }, {
                            //             class: input,
                            //             itemId: 'total_num',
                            //             widget: 'text',
                            //             label: '机身宽度',
                            //         }, {
                            //             class: input,
                            //             itemId: 'total_num',
                            //             widget: 'text',
                            //             label: '机身厚度',
                            //         }, {
                            //             class: input,
                            //             itemId: 'total_num',
                            //             widget: 'text',
                            //             label: '机身材质',
                            //         }],
                            //     }],
                        }, {
                            layout: 'horizontal',
                            class: two_footer,
                            items: [{
                                class: [centerHeader, bottom],
                                widget: 'button',
                                type: 'warning',
                                value: '上一步',
                                onClick: [{
                                    action: 'step-previous',
                                    target: '$widgets.stepContainer'
                                }]
                            }, {
                                widget: 'button',
                                type: 'primary',
                                value: '下一步',
                                onClick: [{
                                    action: 'step-next',
                                    target: '$widgets.stepContainer'
                                }]
                            }]
                        }
                    ]
                },
                {
                    title: '商品详情',
                    class: overflow,
                    items: [{
                        widget: 'form',
                        id: 'info',
                        class: [content, radius],
                        fields: [{
                            class: input_w,
                            itemId: 'description',
                            widget: 'richeditor',
                            label: '商品详情',
                            height: 900,
                        }]
                    }, {
                        layout: 'horizontal',
                        class: two_footer,
                        items: [{
                            class: [centerHeader, bottom],
                            widget: 'button',
                            type: 'warning',
                            value: '上一步',
                            onClick: [{
                                action: 'step-previous',
                                target: '$widgets.stepContainer'
                            }]
                        }, {
                            widget: 'button',
                            type: 'primary',
                            value: '下一步',
                            onClick: [{
                                action: 'step-next',
                                target: '$widgets.stepContainer'
                            }]
                        }]
                    }]
                }, {
                    title: '物流设置',
                    class: overflow,
                    items: [{
                        widget: 'form',
                        id: 'logistics',
                        class: [content, radius],
                        layout: {
                            type: "basicform",
                            labelPosition: "right",
                            labelWidth: 300
                        },
                        fields: [{
                            class: input_w,
                            itemId: 'is_postage',
                            widget: 'form-radio',
                            value: 0,
                            options: [{
                                value: 0,
                                text: '是'
                            }, {
                                value: 1,
                                text: '否'
                            }],
                            label: '是否包邮',
                        }, {
                            class: input_w,
                            itemId: 'postage',
                            widget: 'text',
                            label: '邮费',
                        }]
                    }, {
                        layout: 'horizontal',
                        class: two_footer,
                        items: [{
                            class: [centerHeader, bottom],
                            widget: 'button',
                            type: 'warning',
                            value: '上一步',
                            onClick: [{
                                action: 'step-previous',
                                target: '$widgets.stepContainer'
                            }]
                        }, {
                            widget: 'button',
                            type: 'primary',
                            value: '下一步',
                            onClick: [{
                                action: 'step-next',
                                target: '$widgets.stepContainer'
                            }]
                        }]
                    }]
                }, {
                    title: '营销设置',
                    class: overflow,
                    items: [{
                        widget: 'form',
                        id: 'market',
                        class: [content, radius],
                        layout: {
                            type: "basicform",
                            labelPosition: "right",
                            labelWidth: 300
                        },
                        fields: [{
                            class: input_w,
                            itemId: 'ficti',
                            widget: 'text',
                            label: '虚拟销量',
                        }, {
                            class: input_w,
                            itemId: 'sort',
                            widget: 'text',
                            label: '排序',
                        }, {
                            class: input_w,
                            itemId: 'give_integral',
                            widget: 'text',
                            label: '赠送积分',
                        }, {
                            class: input_w,
                            itemId: 'is_vip',
                            widget: 'form-radio',
                            value: 0,
                            options: [{
                                value: 0,
                                text: '是'
                            }, {
                                value: 1,
                                text: '否'
                            }],
                            label: '是否开启会员专属',
                        }, {
                            class: input_w,
                            itemId: 'is_hot',
                            widget: 'form-radio',
                            value: 0,
                            options: [{
                                value: 0,
                                text: '是'
                            }, {
                                value: 1,
                                text: '否'
                            }],
                            label: '是否热卖',
                        }, {
                            class: input_w,
                            itemId: 'is_benefit',
                            widget: 'form-radio',
                            value: 1,
                            options: [{
                                value: 0,
                                text: '是'
                            }, {
                                value: 1,
                                text: '否'
                            }],
                            label: '是否优惠',
                        }, {
                            class: input_w,
                            itemId: 'is_best',
                            widget: 'form-radio',
                            value: 0,
                            options: [{
                                value: 0,
                                text: '是'
                            }, {
                                value: 1,
                                text: '否'
                            }],
                            label: '是否精品',
                        }, {
                            class: input_w,
                            itemId: 'is_new',
                            widget: 'form-radio',
                            value: 0,
                            options: [{
                                value: 0,
                                text: '是'
                            }, {
                                value: 1,
                                text: '否'
                            }],
                            label: '是否新品',
                        }, {
                            class: input_w,
                            itemId: 'is_good',
                            widget: 'form-radio',
                            value: 1,
                            options: [{
                                value: 0,
                                text: '是'
                            }, {
                                value: 1,
                                text: '否'
                            }],
                            label: '是否优品推荐',
                        }]
                    }, {
                        layout: 'horizontal',
                        class: two_footer,
                        items: [{
                            class: [centerHeader, bottom],
                            widget: 'button',
                            type: 'warning',
                            value: '上一步',
                            onClick: [{
                                action: 'step-previous',
                                target: '$widgets.stepContainer'
                            }]
                        }, {
                            widget: 'button',
                            type: 'primary',
                            value: '下一步',
                            onClick: [{
                                action: 'step-next',
                                target: '$widgets.stepContainer'
                            }]
                        }]
                    }]
                }, {
                    title: '其他设置',
                    class: overflow,
                    items: [{
                        widget: 'form',
                        id: 'other',
                        class: [content, radius],
                        layout: {
                            type: "basicform",
                            labelPosition: "right",
                            labelWidth: 300
                        },
                        fields: [{
                            class: input_w,
                            itemId: 'keyword',
                            widget: 'text',
                            label: '商品关键字',
                        },
                        // {
                        //     class: input_w,
                        //     itemId:'store_info',
                        //     widget:'text',
                        //     label:'商品简介',
                        // },
                        {
                            class: input_w,
                            itemId: 'command_word',
                            widget: 'text',
                            label: '商品口令',
                        }, {
                            class: input_w,
                            itemId: 'recommend_image',
                            widget: 'text',
                            label: '商品推荐图',
                        }]
                    }, {
                        layout: 'horizontal',
                        class: two_footer,
                        items: [{
                            class: [centerHeader, bottom],
                            widget: 'button',
                            type: 'warning',
                            value: '上一步',
                            onClick: [{
                                action: 'step-previous',
                                target: '$widgets.stepContainer'
                            }]
                        }, {
                            widget: 'button',
                            type: 'primary',
                            value: '完成',
                            onClick: [{
                                action: 'print',
                                value: {
                                    '...': [
                                        '$widgets.basic.value',
                                        '$widgets.spec.value',
                                        '$widgets.info.value',
                                        '$widgets.logistics.value',
                                        '$widgets.market.value',
                                        '$widgets.other.value'
                                    ]
                                },
                            }, {
                                action: 'service',
                                path: 'shop/store/product/save',
                                params: {
                                    '...': [
                                        '$widgets.basic.value',
                                        '$widgets.spec.value',
                                        '$widgets.info.value',
                                        '$widgets.logistics.value',
                                        '$widgets.market.value',
                                        '$widgets.other.value'
                                    ]
                                },
                            }, {
                                action: 'dialog-hide',
                                mode: 'apply'
                            }]
                        }]
                    }]
                }]
        }]
    }]
};


