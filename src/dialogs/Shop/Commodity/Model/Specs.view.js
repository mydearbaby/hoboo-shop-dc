import {
    formInput,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'新增编辑',
    dialog:{
        width: 600,
        type:'dialog',
    },
    onMounted: [{
        action:'print',
        value:'$params'
    },{
        action:'set',
        target: '$view',
        field:'title',
        value:'$params.title'
    },{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params.form'
    }],
    items:[{
        region: 'center',
        layout:'vertical',
        items:[{
            id: 'form',
            widget: 'form',
            fields:[{
                class: formInput,
                itemId:'goods_type_id',
                widget:'select',
                placeholder:'请选择商品模型',
                dictionary:{
                    path:"shop/common/dictionary/goods_type",
                    
                },
                label:'所属模型',
            },{
                class: formInput,
                itemId: 'name',
                placeholder:'请输入商品规格',
                widget:'text',
                label:'商品规格',
            },{
                class: formInput,
                itemId: '',
                placeholder:'请输入规格项',
                widget:'text',
                label:'规格项',
            },{
                itemId: 'spec_type',
                widget: 'form-radio',
                value: 0,
                options: [{
                    value: 0,
                    text: '文字'
                }, {
                    value: 1,
                    text: '图片'
                }, {
                    value: 2,
                    text: '颜色'
                }],
                label:'展现方式',
            }],
            buttons: [{
                value: '保存',
                type: 'primary',
                onClick: [{
                    action: 'confirm',
                    title: '表单保存',
                    message: '是否保存表单?',
                    confirm: [{
                        var:'value',
                        target:'$widget',
                        action:'call',
                        method:'getValue'
                    },{
                        action: 'service',
                        path: 'shop/store/spec/save',
                        params:{
                            goods_type_id:'$params.goods_type_id',
                            '...':['$vars.value']
                        }
                    },{
                        action:'dialog-hide',
                        mode:'apply'
                    }]
                }]
            }],
        }]
    }]
} ;
