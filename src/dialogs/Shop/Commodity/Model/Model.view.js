import {
    formInput,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'新增编辑',
    dialog:{
        width: 600,
        type:'dialog',
    },
    onMounted: [{
        action:'print',
        value:'$params'
    },{
        action:'set',
        target: '$view',
        field:'title',
        value:'$params.title'
    },{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params.form'
    }],
    items:[{
        region: 'center',
        layout:'vertical',
        items:[{
            id: 'form',
            widget: 'form',
            fields:[{
                class: formInput,
                itemId: 'type_name',
                placeholder:'请输入商品模型',
                widget:'text',
                label:'商品模型',
            }],
            buttons: [{
                value: '保存',
                type: 'primary',
                onClick: [{
                    action: 'confirm',
                    title: '表单保存',
                    message: '是否保存表单?',
                    confirm: [{
                        var:'value',
                        target:'$widget',
                        action:'call',
                        method:'getValue'
                    },{
                        action: 'service',
                        path: 'shop/store/model/save',
                        params: '$vars.value'
                    },{
                        action:'dialog-hide',
                        mode:'apply'
                    }]
                }]
            }],
        }]
    }]
} ;
