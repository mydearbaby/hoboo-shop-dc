import {
    formInput,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'新增编辑',
    dialog:{
        width: 600,
        type:'dialog',
    },
    onMounted: [{
        action:'print',
        value:'$params'
    },{
        action:'set',
        target: '$view',
        field:'title',
        value:'$params.title'
    },{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params.form'
    }],
    items:[{
        region: 'center',
        layout:'vertical',
        items:[{
            id: 'form',
            widget: 'form',
            fields:[{
                class: formInput,
                itemId:'goods_type_id',
                widget:'select',
                placeholder:'请选择商品模型',
                dictionary:{
                    path:"shop/common/dictionary/goods_type",
                    
                },
                label:'所属模型',
            },{
                class: formInput,
                itemId: 'attr_name',
                placeholder:'请输入商品属性',
                widget:'text',
                label:'商品属性',
            },{
                class: formInput,
                itemId: 'description',
                placeholder:'请输入描述信息',
                widget:'text',
                label:'描述信息',
            },{
                itemId: 'attr_index',
                widget: 'form-radio',
                value: 0,
                options: [{
                    value: 0,
                    text: '不检索'
                }, {
                    value: 1,
                    text: '关键词'
                }, {
                    value: 2,
                    text: '范围'
                }],
                label:'检索方式',
            },{
                itemId: 'attr_input_type',
                widget: 'form-radio',
                value: 0,
                options: [{
                    value: 0,
                    text: '手工填写'
                }, {
                    value: 1,
                    text: '单选'
                }, {
                    value: 2,
                    text: '多选'
                }],
                label:'录入方式',
            },{
                class: formInput,
                itemId: 'attr_values',
                placeholder:'请输入可选值',
                widget:'text',
                label:'可选值',
            }],
            buttons: [{
                value: '保存',
                type: 'primary',
                onClick: [{
                    action: 'confirm',
                    title: '表单保存',
                    message: '是否保存表单?',
                    confirm: [{
                        var:'value',
                        target:'$widget',
                        action:'call',
                        method:'getValue'
                    },{
                        action: 'service',
                        path: 'shop/store/attribute/save',
                        params:{
                            goods_type_id:'$params.goods_type_id',
                            '...':['$vars.value']
                        }
                    },{
                        action:'dialog-hide',
                        mode:'apply'
                    }]
                }]
            }],
        }]
    }]
} ;
