import {
    center,
    input
} from '@/css/DialogsCss.module.scss';
export default {
    title:'用户详情',
    dialog:{
        size:'40%'
    },
    onMounted: [{
        action: 'set',
        target: '$widgets.formbasic',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'formbasic',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:1
                },
                fields:[{
                    itemId:'real_name',
                    widget:'form-text',
                    label:'姓名',
                },{
                    itemId:'nickname',
                    widget:'form-text',
                    label:'昵称',
                },{
                    itemId:'phone',
                    widget:'form-text',
                    label:'手机号码',
                },{
                    itemId:'now_money',
                    widget:'form-text',
                    label:'用户余额',
                },{
                    itemId:'group_id',
                    widget:'select',
                    print: true,
                    dictionary: {
                        path: 'common/dictionary/config_id',
                        params: {
                            module: 'system_module',
                            name: 'contact_group',
                        },
                    },
                    label:'用户分组',
                },{
                    itemId:'level',
                    widget:'select',
                    print: true,
                    dictionary: "shop/common/dictionary/system_user_level",
                    label:'用户等级',
                },{
                    itemId:'state',
                    widget:'select',
                    print: true,
                    dictionary: "common/static/state",
                    label:'状态',
                }],
                flex: true
            }]
        }]
    }]
};
