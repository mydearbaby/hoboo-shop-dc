import {
    center,
    input,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'新增编辑',
    dialog:{
        size: '50%',
    },
    onMounted: [{
        action:'print',
        value:'$params'
    },{
        action:'set',
        target: '$view',
        field:'title',
        value:'$params.title'
    },{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params.form'
    }],
    items:[{
        region: 'center',
        class: center,
        layout:'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'form',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:1
                },
                fields:[{
                    class: input,
                    itemId:'real_name',
                    placeholder:'请输入姓名',
                    widget:'text',
                    label:'姓名',
                },{
                    class: input,
                    itemId:'nickname',
                    placeholder:'请输入昵称',
                    widget:'text',
                    label:'昵称',
                },{
                    class: input,
                    itemId:'phone',
                    placeholder:'请输入手机号码',
                    widget:'text',
                    label:'手机号码',
                },
                // {
                //     class: input,
                //     itemId:'now_money',
                //     placeholder:'请输入要修改的用户余额',
                //     widget:'text',
                //     label:'用户余额',
                // }
                {
                    class: input,
                    itemId:'group_id',
                    placeholder:'请选择用户分组',
                    widget:'select',
                    dictionary: {
                        path: 'common/dictionary/config_id',
                        params: {
                            module: 'system_module',
                            name: 'contact_group',
                        },
                    },
                    label:'用户分组',
                },{
                    class: input,
                    itemId:'level',
                    widget:'select',
                    placeholder:'请选择用户等级',
                    dictionary: "shop/common/dictionary/system_user_level",
                    label:'用户等级',
                },{
                    class: input,
                    itemId:'state',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '启用'
                    }, {
                        value: 0,
                        text: '禁用'
                    }],
                    label:'状态',
                }],
                flex: true,
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            var: 'value',
                            target: '$widget',
                            action: 'call',
                            method: 'getValue'
                        }, {
                            action: 'service',
                            path: 'system/role/user/save',
                            params: '$vars.value'
                        }, {
                            action: 'dialog-hide',
                            mode: 'apply'
                        }]
                    }]
                }],
            }]
        }]
    }]
} ;
