import {
    formInput,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'用户充值',
    dialog:{
        width: 600,
        type:'dialog',
    },
    onMounted:[{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params'
    }],
    items:[{
        region: 'center',
        layout:'vertical',
        items:[{
            id: 'form',
            widget: 'form',
            fields:[{
                itemId: 'id',
            },{
                class: formInput,
                itemId: 'recharge',
                placeholder:'请输入用户充值金额',
                widget:'text',
                label:'充值金额',
            }],
            buttons: [{
                value: '保存',
                type: 'primary',
                onClick: [{
                    action: 'confirm',
                    title: '表单保存',
                    message: '是否保存表单?',
                    confirm: [{
                        var:'value',
                        target:'$widget',
                        action:'call',
                        method:'getValue'
                    },{
                        action: 'service',
                        path: 'user/user_recharge',
                        params: {
                            '...':[
                                // '$vars.value',
                                {
                                    uid:'$vars.value.id',
                                    recharge:"$vars.value.recharge"
                                }
                            ]
                        }
                    },{
                        action:'dialog-hide',
                        mode:'apply'
                    }]
                }]
            }],
        }]
    }]
} ;
