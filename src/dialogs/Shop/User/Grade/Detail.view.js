import {
    center,
    input
} from '@/css/DialogsCss.module.scss';
export default {
    title:'等级详情',
    dialog:{
        size:'40%'
    },
    onMounted: [{
        action: 'set',
        target: '$widgets.formbasic',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'formbasic',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:2
                },
                fields:[{
                    itemId:'name',
                    widget:'form-text',
                    label:'会员名称',
                },{
                    itemId:'money',
                    widget:'form-text',
                    label:'购买金额',
                },{
                    itemId:'valid_date',
                    widget:'form-text',
                    label:'有效时间',
                },{
                    itemId:'discount',
                    widget:'form-text',
                    label:'享受折扣',
                },{
                //     itemId:'image',
                //     widget:'form-text',
                //     label:'会员卡背景',
                // },{
                //     itemId:'icon',
                //     widget:'form-text',
                //     label:'会员图标',
                // },{
                    itemId:'explain',
                    widget:'form-text',
                    label:'等级说明',
                },{
                    itemId:'exp_num',
                    widget:'form-text',
                    label:'升级经验',
                },{
                    itemId:'is_forever',
                    widget:'select',
                    print: true,
                    dictionary: 'shop/common/static/whether',
                    label:'是否为永久会员',
                },{
                    itemId:'is_pay',
                    widget:'select',
                    print: true,
                    dictionary: 'shop/common/static/whether',
                    label:'是否购买',
                },{
                    itemId:'is_show',
                    widget:'select',
                    print: true,
                    dictionary: 'shop/common/static/whether',
                    label:'是否显示',
                }],
                flex: true
            }]
        }]
    }]
};


