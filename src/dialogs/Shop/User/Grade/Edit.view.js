import {
    center,
    input,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'新增编辑',
    dialog:{
        size: '50%',
    },
    onMounted: [{
        action:'print',
        value:'$params'
    },{
        action:'set',
        target: '$view',
        field:'title',
        value:'$params.title'
    },{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params.form'
    }],
    items:[{
        region: 'center',
        class: center,
        layout:'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'form',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:2
                },
                fields:[{
                    class: input,
                    itemId:'name',
                    placeholder:'请输入会员名称',
                    widget:'text',
                    label:'会员名称',
                },{
                    class: input,
                    itemId:'money',
                    placeholder:'请输入购买金额',
                    widget:'text',
                    label:'购买金额',
                },{
                    class: input,
                    itemId:'valid_date',
                    placeholder:'请输入有效时间',
                    widget:'text',
                    label:'有效时间',
                },{
                    class: input,
                    itemId:'discount',
                    placeholder:'请输入享受折扣',
                    widget:'text',
                    label:'享受折扣',
                },{
                    class: input,
                    itemId:'image',
                    placeholder:'请输入会员卡背景',
                    widget:'text',
                    label:'会员卡背景',
                },{
                    class: input,
                    itemId:'icon',
                    placeholder:'请输入会员图标',
                    widget:'text',
                    label:'会员图标',
                },{
                    class: input,
                    itemId:'explain',
                    placeholder:'请输入等级说明',
                    widget:'text',
                    label:'等级说明',
                },{
                    class: input,
                    itemId:'exp_num',
                    placeholder:'请输入升级经验',
                    widget:'text',
                    label:'升级经验',
                },{
                    class: input,
                    itemId:'is_forever',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '是'
                    }, {
                        value: 0,
                        text: '否'
                    }],
                    label:'是否为永久会员',
                },{
                    class: input,
                    itemId:'is_pay',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '是'
                    }, {
                        value: 0,
                        text: '否'
                    }],
                    label:'是否购买',
                },{
                    class: input,
                    itemId:'is_show',
                    widget: 'form-radio',
                    value: 1,
                    options: [{
                        value: 1,
                        text: '是'
                    }, {
                        value: 0,
                        text: '否'
                    }],
                    label:'是否显示',
                }],
                flex: true,
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            var: 'value',
                            target: '$widget',
                            action: 'call',
                            method: 'getValue'
                        }, {
                            action: 'service',
                            path: 'shop/user/grade/save',
                            params: '$vars.value'
                        }, {
                            action: 'dialog-hide',
                            mode: 'apply'
                        }]
                    }]
                }],
            }]
        }]
    }]
} ;
