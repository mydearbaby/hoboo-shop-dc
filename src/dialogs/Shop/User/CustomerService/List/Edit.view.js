import {
    center,
    formInput,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'新增客服',
    dialog:{
        size: 750,
    },
    onMounted: [{
        action:'print',
        value:'$params'
    },{
        action:'set',
        target: '$view',
        field:'title',
        value:'$params.title'
    },{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params.form'
    }],
    items:[{
        region: 'center',
        class: center,
        layout:'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'form',
                widget: 'form',
                fields:[{
                    class: formInput,
                    itemId: 'value',
                    placeholder:'请输入客服名称',
                    widget:'text',
                    label:'客服名称',
                },{
                    class: formInput,
                    itemId: 'value',
                    placeholder:'请输入联系电话',
                    widget:'text',
                    label:'联系电话',
                },{
                    class: formInput,
                    itemId: 'value',
                    placeholder:'请输入客服账号',
                    widget:'text',
                    label:'客服账号',
                },{
                    class: formInput,
                    itemId: 'description',
                    placeholder:'请输入客服密码',
                    widget:'password',
                    label:'客服密码',
                },{
                    class: formInput,
                    itemId: 'description',
                    placeholder:'请输入确认密码',
                    widget:'password',
                    label:'确认密码',
                },{
                    class: formInput,
                    itemId:'state',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '启用'
                    }, {
                        value: 0,
                        text: '禁用'
                    }],
                    label:'账号状态',
                },{
                    class: formInput,
                    itemId:'state',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '启用'
                    }, {
                        value: 0,
                        text: '禁用'
                    }],
                    label:'客服状态',
                },{
                    class: formInput,
                    itemId:'state',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '启用'
                    }, {
                        value: 0,
                        text: '禁用'
                    }],
                    label:'手机订单状态',
                },{
                    class: formInput,
                    itemId:'state',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '启用'
                    }, {
                        value: 0,
                        text: '禁用'
                    }],
                    label:'接单状态',
                }],
                flex: true,
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            var: 'value',
                            target: '$widget',
                            action: 'call',
                            method: 'getValue'
                        }, {
                            action: 'service',
                            // path: 'system/dictionary/config/save',
                            params: {
                                params: "$widget.value",
                            }
                        }, {
                            action: 'dialog-hide',
                            mode: 'apply'
                        }]
                    }]
                }],
            }]
        }]
    }]
} ;
