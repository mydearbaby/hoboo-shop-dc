import {
    center,
    right,
    grid,
    input,
    button,
    search,
    header,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'商品列表',
    dialog:{
        size:'50%'
    },
    onMounted: [{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        span: 10,
        class: center,
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'form',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:2
                },
                fields: [{
                    class: input,
                    itemId: 'name',
                    placeholder: '请输入商品名称',
                    widget: 'text',
                    label: '商品名称',
                }, {
                    itemId: 'type',
                    placeholder:'请输入商品类型',
                    widget:'text',
                    label: '商品类型',
                }, {
                    class: input,
                    itemId: 'category',
                    placeholder: '请输入商品分类',
                    widget: 'text',
                    label: '商品分类',
                }, {
                    class: input,
                    itemId: 'brand',
                    placeholder: '请输入商品品牌',
                    widget: 'text',
                    label: '商品品牌',
                }, {
                    class: input,
                    itemId: 'supplier',
                    placeholder: '请输入供应商',
                    widget: 'text',
                    label: '供应商',
                }, {
                    class: input,
                    itemId: 'description',
                    placeholder: '请输入商品描述',
                    widget: 'text',
                    label: '商品描述',
                }, {
                    class: input,
                    itemId: 'unit',
                    placeholder: '请输入商品单位',
                    widget: 'text',
                    label: '商品单位',
                }, {
                    class: input,
                    itemId: 'production',
                    placeholder: '请输入商品产地',
                    widget: 'text',
                    label: '商品产地',
                }, {
                    class: input,
                    itemId: 'price_purchase',
                    placeholder: '请输入采购价',
                    widget: 'text',
                    label: '采购价',
                }, {
                    class: input,
                    itemId: 'price_brade',
                    placeholder: '请输入批发价',
                    widget: 'text',
                    label: '批发价',
                }, {
                    class: input,
                    itemId: 'price_retail',
                    placeholder: '请输入零售价',
                    widget: 'text',
                    label: '零售价',
                }, {
                    class: input,
                    itemId: 'remark',
                    placeholder: '请输入备注',
                    widget: 'text',
                    label: '备注说明',
                }],
                buttons: [{
                    value: '保存',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            action: 'service',
                            path: 'basicManage/commodityInfo/list/save',
                            params: '$widget.value'
                        }, {
                            action: 'message',
                            message: '保存成功',
                            type: 'success'
                        }, {
                            action: 'service',
                            path: 'basicManage/commodityInfo/list/list',
                            params:{
                                page_size:'$widgets.grid.pageSize',
                                page_no:'$widgets.grid.page',
                                keywords: '$widgets.search.value.keywords',
                                is_show: '$widgets.search.value.is_show',
                            },
                            actions: [{
                                action:'set',
                                target:'$widgets.grid',
                                field:'value',
                                value:'$params.data.items'
                            },{
                                action:'set',
                                target:'$widgets.grid',
                                field:'total',
                                value:'$params.data.total'
                            }]
                        },{
                            action:'dialog-hide',
                            mode:'apply'
                        }]
                    }]
                }],
                flex: true
            }]
        }]
    }]
};


