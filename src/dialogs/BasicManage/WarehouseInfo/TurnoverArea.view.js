import {
    center,
    right,
    grid,
    input,
    button,
    search,
    header,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'周转区管理',
    dialog:{
        size:'50%'
    },
    onMounted: [{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        span: 10,
        class: center,
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'form',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:2
                },
                fields: [{
                    itemId: 'name',
                    placeholder:'请输入周转区名称',
                    widget:'text',
                    label: '周转区名称',
                }, {
                    class: input,
                    itemId: 'size',
                    placeholder: '请输入周转区尺寸',
                    widget: 'text',
                    label: '周转区尺寸',
                }, {
                    class: input,
                    itemId: 'is_free',
                    widget: 'radiogroup',
                    options: [{
                        value: 1,
                        text: '是'
                    }, {
                        value: 2,
                        text: '否'
                    }],
                    label: '是否空闲',
                }, {
                    class: input,
                    itemId: 'remark',
                    placeholder: '请输入备注',
                    widget: 'text',
                    label: '备注说明',
                }],
                buttons: [{
                    value: '保存',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            action: 'service',
                            path: 'basicManage/warehouseInfo/turnoverArea/save',
                            params: '$widget.value'
                        }, {
                            action: 'message',
                            message: '保存成功',
                            type: 'success'
                        }, {
                            action: 'service',
                            path: 'basicManage/warehouseInfo/turnoverArea/list',
                            params:{
                                page_size:'$widgets.grid.pageSize',
                                page_no:'$widgets.grid.page',
                                keywords: '$widgets.search.value.keywords',
                                is_show: '$widgets.search.value.is_show',
                            },
                            actions: [{
                                action:'set',
                                target:'$widgets.grid',
                                field:'value',
                                value:'$params.data.items'
                            },{
                                action:'set',
                                target:'$widgets.grid',
                                field:'total',
                                value:'$params.data.total'
                            }]
                        },{
                            action:'dialog-hide',
                            mode:'apply'
                        }]
                    }]
                }],
                flex: true
            }]
        }]
    }]
};


