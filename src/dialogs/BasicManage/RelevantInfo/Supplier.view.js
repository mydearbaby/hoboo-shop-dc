import {
    center,
    right,
    grid,
    input,
    button,
    search,
    header,
} from '@/css/DialogsCss.module.scss';
export default {
    title:'供应商管理',
    dialog:{
        size:'50%'
    },
    onMounted: [{
        action: 'set',
        target: '$widgets.form',
        field: 'value',
        value: '$params'
    }],
    items: [{
        region: 'center',
        span: 10,
        class: center,
        layout: 'vertical',
        items: [{
            layout: 'vertical',
            items: [{
                id: 'form',
                widget: 'form',
                layout: {
                    type:'gridform',
                    column:2
                },
                fields: [{
                    class: input,
                    itemId: 'name',
                    placeholder: '请输入供应商名称',
                    widget: 'text',
                    label: '供应商',
                }, {
                    itemId: 'city',
                    placeholder:'请输入所在城市',
                    widget:'text',
                    label: '所在城市',
                }, {
                    class: input,
                    itemId: 'address',
                    placeholder: '请输入地址',
                    widget: 'text',
                    label: '地址',
                }, {
                    class: input,
                    itemId: 'director',
                    placeholder: '请输入负责人',
                    widget: 'text',
                    label: '负责人',
                }, {
                    class: input,
                    itemId: 'gender',
                    widget: 'radiogroup',
                    options: [{
                        value: 1,
                        text: '男'
                    }, {
                        value: 2,
                        text: '女'
                    }],
                    label: '性别',
                }, {
                    class: input,
                    itemId: 'position',
                    placeholder: '请输入职位',
                    widget: 'text',
                    label: '职位',
                }, {
                    class: input,
                    itemId: 'phone',
                    placeholder: '请输入手机号码',
                    widget: 'text',
                    label: '手机号码',
                }, {
                    class: input,
                    itemId: 'tel',
                    placeholder: '请输入座机',
                    widget: 'text',
                    label: '座机',
                }, {
                    class: input,
                    itemId: 'qq',
                    placeholder: '请输入QQ',
                    widget: 'text',
                    label: 'QQ',
                }, {
                    class: input,
                    itemId: 'wechat',
                    placeholder: '请输入微信',
                    widget: 'text',
                    label: '微信',
                }, {
                    class: input,
                    itemId: 'grade',
                    placeholder: '请输入供应商等级',
                    widget: 'text',
                    label: '等级',
                }, {
                    class: input,
                    itemId: 'remark',
                    placeholder: '请输入备注',
                    widget: 'text',
                    label: '备注说明',
                }],
                buttons: [{
                    value: '保存',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            action: 'service',
                            path: 'basicManage/relevantInfo/supplier/save',
                            params: '$widget.value'
                        }, {
                            action: 'message',
                            message: '保存成功',
                            type: 'success'
                        }, {
                            action: 'service',
                            path: 'basicManage/relevantInfo/supplier/list',
                            params:{
                                page_size:'$widgets.grid.pageSize',
                                page_no:'$widgets.grid.page',
                                keywords: '$widgets.search.value.keywords',
                                is_show: '$widgets.search.value.is_show',
                            },
                            actions: [{
                                action:'set',
                                target:'$widgets.grid',
                                field:'value',
                                value:'$params.data.items'
                            },{
                                action:'set',
                                target:'$widgets.grid',
                                field:'total',
                                value:'$params.data.total'
                            }]
                        },{
                            action:'dialog-hide',
                            mode:'apply'
                        }]
                    }]
                }],
                flex: true
            }]
        }]
    }]
};

