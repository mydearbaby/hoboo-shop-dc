import {
    formInput
} from '@/css/VueCss.module.scss' ;
export default {
    title:'品牌',
    template:{
        path:'columns/two',
        params:{
            services:{
                save:{
                    path:'shop/store/brand/save',
                },
                remove:{
                    path:'shop/store/brand/remove',
                },
                list:{
                    path:'shop/store/brand/list',
                }
            },
            search:{
                selects:[
                    'state'
                    // 这里增加与原来一样的搜索 selects 配置
                ],
                extraParams:{
                    // 这里添加额外参数
                    keywords: '$params.keywords',
                }
            },
            form:{
                fields:[
                    {
                        class: formInput,
                        itemId:'name',
                        placeholder:'请输入品牌名称',
                        widget:'text',
                        label:'品牌名称',
                    },{
                        class: formInput,
                        itemId:'phonetic',
                        placeholder:'请输入品牌简称',
                        widget:'text',
                        label:'品牌简称',
                    },{
                        class: formInput,
                        itemId:'description',
                        placeholder:'请输入品牌描述',
                        widget:'text',
                        label:'品牌描述',
                    },{
                        class: formInput,
                        itemId:'state',
                        widget: 'form-radio',
                        value: 1,
                        options: [{
                            value: 1,
                            text: '启用'
                        }, {
                            value: 0,
                            text: '禁用'
                        }],
                        label:'状态',
                    }
                ]
            },
            grid:{
                columns:[
                    {
                        prop:'name',
                        label:'品牌名称',
                    },{
                        prop:'phonetic',
                        label:'品牌简称',
                    },{
                        prop:'description',
                        label:'品牌描述',
                    },'state'
                ]
            },
            
        }
    }
} ;