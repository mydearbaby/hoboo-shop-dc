import {
    formInput
} from '@/css/VueCss.module.scss' ;
export default {
    title:'单位',
    template:{
        path:'columns/two',
        params:{
            services:{
                save:{
                    path:'shop/store/unit/save',
                },
                remove:{
                    path:'shop/store/unit/remove',
                },
                list:{
                    path:'shop/store/unit/list',
                }
            },
            search:{
                selects:[
                    'state'
                    // 这里增加与原来一样的搜索 selects 配置
                ],
                extraParams:{
                    // 这里添加额外参数
                    keywords: '$params.keywords',
                }
            },
            form:{
                fields:[{
                    class: formInput,
                    itemId:'name',
                    placeholder:'请输入名称',
                    widget:'text',
                    label:'名称',
                },{
                    class: formInput,
                    itemId:'description',
                    placeholder:'请输入备注信息',
                    widget:'text',
                    label:'备注信息',
                },{
                    class: formInput,
                    itemId:'state',
                    widget: 'form-radio',
                    value: 1,
                    options: [{
                        value: 1,
                        text: '启用'
                    }, {
                        value: 0,
                        text: '禁用'
                    }],
                    label:'状态',
                }],
            },
            grid:{
                columns:[{
                    prop:'name',
                    label:'名称',
                },{
                    prop:'description',
                    label:'备注信息',
                },'state'],
            },
            
        }
    }
} ;