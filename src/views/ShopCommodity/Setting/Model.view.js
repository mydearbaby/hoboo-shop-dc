import {
    left as threecolumns_left,
    center as threecolumns_center,
    body as threecolumns_body,
    grid as threecolumns_grid,
} from '@/css/layout/ThreeColumns.module.scss' ;

import {
    button,
    search,
    girdHeader,
    formTitle,
    contact_header,
    formInput
} from '@/css/VueCss.module.scss' ;
export default {
    title: '模型',
    layout:'three-columns',
    items:[{
        region:'left',
        class:threecolumns_left,
        layout: 'vertical',
        items: [{
            id: 'search',
            class: search,
            widget: 'search',
            text:{
                field: 'keywords',
                placeholder: '请输入模型名称'
            },
            onSearch: [{
                action:'grid-load',
                target:'$widgets.grid',
                params: {
                    '...': '$params',
                    keywordsBy: '"type_name"',
                }
            }]
        },{
            layout: 'horizontal',
            class: contact_header,
            items:[{
                class: formTitle,
                id: 'title',
                widget: 'public-title',
                title: '商品模型',
            },{
                class: button,
                widget: 'buttonicon',
                type: 'text',
                layout: 'horizontal',
                onClick: [{
                    action:'dialog-show',
                    path: 'Shop/Commodity/Model/Model',
                    params: {
                        title:"'新增模型'"
                    },
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions: [{
                        action: 'print',
                        description: '取消对话框'
                    }]
                }]
            }]
        },{
            id:'grid',
            widget:'grid',
            service:'shop/store/model/list',
            pageSize:20,
            columns:[{
                prop:'type_name',
            }],
            flex:true,
            header:false,
            enableIndexColumn: false,
            operationButtonCount: 0,  
            operationWidth: 30,
            buttons: [{
                value: '编辑',
                onClick: [{
                    action: 'dialog-show',
                    path: 'Shop/Commodity/Model/Model',
                    params: {
                        title:"'编辑模型信息'",
                        form: '$params',
                    },
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions: [{
                        action: 'print',
                        description: '取消对话框'
                    }]
                }]
            },{
                value: '删除',
                type: 'danger',
                onClick: [{
                    action: 'confirm',
                    title: '删除数据',
                    message: '是否删除该条数据?',
                    confirm: [{
                        action: 'service',
                        path: 'shop/store/model/remove',
                        params: {
                            id: '$params.id'
                        }
                    }, {
                        action: 'call',
                        target: '$widget',
                        method: 'reload'
                    }]
                }]
            }],
            onRowselect:[{
                action:'print',
                value:'$params'
            },{
                action:'grid-load',
                target:'$widgets.gridSpecs',
                service:'shop/store/spec/list',
                params:{
                    goods_type_id: "$params.id",
                }
            },{
                action:'grid-load',
                target:'$widgets.gridAttribute',
                service:'shop/store/attribute/list',
                params:{
                    goods_type_id: "$params.id"
                }
            }],
        }]
    },{
        region:'center',
        items:[{
            flex:1,
            class:[threecolumns_center, threecolumns_body],
            layout:'tab',
            items:[{
                title:'商品规格',
                layout:'vertical',
                items:[{
                    items:[{
                        class: girdHeader,
                        layout:'horizontal',
                        items:[{
                            class: button,
                            widget: 'button',
                            value: '新增',
                            onClick: [{
                                action:'dialog-show',
                                path: 'Shop/Commodity/Model/Specs',
                                params: {
                                    title:"'新增规格'",
                                    form: {
                                        goods_type_id:'$widgets.grid.selectedItem.id'
                                    }
                                },
                                applyActions: [{
                                    action: 'call',
                                    target: '$widgets.gridSpecs',
                                    method: 'reload'
                                }],
                                cancelActions:[{
                                    action:'print',
                                    description:'取消对话框'
                                }]
                            }]
                        },{
                            id: 'search',
                            class: [search, formInput],
                            widget: 'search',
                            onSearch: [{
                                action:'grid-load',
                                target:'$widgets.gridSpecs',
                                params: {
                                    goods_type_id: "$widgets.grid.selectedItem.id",
                                    page_size: '$widgets.gridSpecs.pageSize',
                                    keywords: '$params.keywords',
                                }
                            }]
                        }]
                    }]
                },{
                    class: threecolumns_grid,
                    id: 'gridSpecs',
                    widget: 'grid',
                    pageSize: 20,
                    columns:[{
                        prop:'name',
                        label:'规格名称',
                    },{
                        prop:'spec_type',
                        dictionary:{
                            path:"shop/common/dictionary/spec_type"
                        },
                        label:'规格类型',
                    },{
                        prop:'sort',
                        label:'排序',
                    },{
                        prop:'last_follow_time',
                        label:'备注信息',
                    }],
                    flex: true,
                    enableEdit:true,
                    buttons:[{
                        value:'编辑',
                        onClick:[{
                            action:'dialog-show',
                            path: 'Shop/Commodity/Model/Specs',
                            params: {
                                title:"'编辑规格信息'",
                                form: '$params',
                            },
                            applyActions: [{
                                action: 'call',
                                target: '$widgets.gridSpecs',
                                method: 'reload'
                            }],
                            cancelActions:[{
                                action:'print',
                                description:'取消对话框'
                            }]
                        }]
                    },{
                        value:'删除',
                        type:'danger',
                        onClick:[{
                            action:'confirm',
                            title:'删除数据',
                            message:'是否删除该条数据?',
                            confirm:[{
                                action:'service',
                                path:'shop/store/spec/remove',
                                params: {
                                    id: '$params.id'
                                }
                            },{
                                action: 'call',
                                target: '$widget',
                                method: 'reload'
                            }]
                        }]
                    }],
                }]
            },{
                title:'商品属性',
                layout:'vertical',
                items:[{
                    items:[{
                        class: girdHeader,
                        layout:'horizontal',
                        items:[{
                            class: button,
                            widget: 'button',
                            value: '新增',
                            onClick: [{
                                action:'dialog-show',
                                path: 'Shop/Commodity/Model/Attribute',
                                params: {
                                    title:"'新增属性'",
                                    form: {
                                        goods_type_id:'$widgets.grid.selectedItem.id'
                                    }
                                },
                                applyActions: [{
                                    action: 'call',
                                    target: '$widgets.gridAttribute',
                                    method: 'reload'
                                }],
                                cancelActions:[{
                                    action:'print',
                                    description:'取消对话框'
                                }]
                            }]
                        },{
                            id: 'search',
                            class: [search, formInput],
                            widget: 'search',
                            onSearch: [{
                                action:'grid-load',
                                target:'$widgets.gridAttribute',
                                params: {
                                    goods_type_id: "$widgets.grid.selectedItem.id",
                                    page_size: '$widgets.gridAttribute.pageSize',
                                    '...':'$params',
                                    keywordsBy: '"attr_name"',
                                }
                            }]
                        }]
                    }]
                },{
                    class: threecolumns_grid,
                    id: 'gridAttribute',
                    widget: 'grid',
                    pageSize: 20,
                    columns:[{
                        prop:'attr_name',
                        label:'属性名称',
                    },{
                        prop:'description',
                        label:'属性描述',
                    },{
                        prop:'attr_index',
                        label:'是否检索',
                    },{
                        prop:'attr_input_type',
                        label:'是否填写',
                    },{
                        prop:'attr_values',
                        label:'可选值列表',
                    },{
                        prop:'is_important',
                        label:'核心',
                    }],
                    flex: true,
                    enableEdit:true,
                    buttons:[{
                        value:'编辑',
                        onClick:[{
                            action:'dialog-show',
                            path: 'Shop/Commodity/Model/Attribute',
                            params: {
                                title:"'编辑属性信息'",
                                form: '$params',
                            },
                            applyActions: [{
                                action: 'call',
                                target: '$widgets.gridAttribute',
                                method: 'reload'
                            }],
                            cancelActions:[{
                                action:'print',
                                description:'取消对话框'
                            }]
                        }]
                    },{
                        value:'删除',
                        type:'danger',
                        onClick:[{
                            action:'confirm',
                            title:'删除数据',
                            message:'是否删除该条数据?',
                            confirm:[{
                                action:'service',
                                path:'shop/store/attribute/remove',
                                params: {
                                    id: '$params.id'
                                }
                            },{
                                action: 'call',
                                target: '$widget',
                                method: 'reload'
                            }]
                        }]
                    }],
                }]
            }]
        }]
    }],
} ;
