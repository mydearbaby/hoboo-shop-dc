import {
    region,
    left,
    center,
} from '@/css/layout/ThreeColumns.module.scss' ;
import {
    // center,
    right,
    grid,
    header,
    button,
    search,
    itemInfo,
    formInput
} from '@/css/VueCss.module.scss' ;
export default {
    title:'商品分类',
    items:[{
        region:'left',
        class:[region, left],
        layout: 'vertical',
        items: [{
            items: [{
            //     id: 'search',
            //     class: search,
            //     widget: 'search',
            //     onSearch: [{
            //         action:'grid-load',
            //         target:'$widgets.tree',
            //         params: {
            //             '...': '$params',
            //             keywordsBy: '"cate_name"',
            //         }
            //     }]
            // },{
                class: header,
                layout:'horizontal',
                items:[{
                    class: button,
                    widget: 'button',
                    value: '新增主分类',
                    onClick:[{
                        action:'call',
                        target:'$widgets.form',
                        method:'reset'
                    },{
                        action:'call',
                        target:'$widgets.grid',
                        method:'clearSelection'
                    }]
                },{
                    class: button,
                    widget: 'button',
                    value: '展开',
                },{
                    class: button,
                    widget: 'button',
                    value: '刷新',
                },{
                    class: button,
                    widget: 'button',
                    value: '收起',
                },{
                    class: button,
                    widget: 'button',
                    value: '启用',
                },{
                    class: button,
                    widget: 'button',
                    value: '禁用',
                }]
            }]
        },{
            id:'tree',
            class: grid,
            widget:'tree',
            service: 'shop/store/classify/list',
            // pageSize:20,
            columns:[{
                prop:'cate_name',
                label:'分类名称',
            }],
            flex:true,
            enableCheckboxColumn:true,
            // operationWidth:210,
            // operationButtonCount:4,
            onNodeSelect:[{
                action:'print',
                value:'$params',
                description:'选定节点'
            },{
                action: 'set',
                target: '$widgets.form',
                field: 'value',
                value: {
                    id: '$params.id',
                    pid: '$params.pid',
                    cate_name: '$params.cate_name',
                    pic: '$params.pic',
                    big_pic: '$params.big_pic',
                    name_phonetic: '$params.name_phonetic',
                    alias: '$params.alias',
                    alias_phonetic: '$params.alias_phonetic',
                    keywords: '$params.keywords',
                    is_show: '$params.is_show',
                    is_navi: '$params.is_navi',
                    state: '$params.state',
                    sort: '$params.sort',
                    description: '$params.description'
                }
            }],
            buttons:[{
                value:'新增',
                onClick: [{
                    action: 'call',
                    target: '$widgets.form',
                    method: 'reset'
                }, {
                    action: 'set',
                    target: '$widgets.form',
                    field: 'value',
                    value: {
                        pid: '$params.id',
                    }
                }]
            }, {
                value:'编辑',
                onClick: [{
                    action: 'set',
                    target: '$widgets.form',
                    field: 'value',
                    value: {
                        id: '$params.id',
                        pid: '$params.pid',
                        cate_name: '$params.cate_name',
                        pic: '$params.pic',
                        big_pic: '$params.big_pic',
                        name_phonetic: '$params.name_phonetic',
                        alias: '$params.alias',
                        alias_phonetic: '$params.alias_phonetic',
                        keywords: '$params.keywords',
                        is_show: '$params.is_show',
                        is_navi: '$params.is_navi',
                        state: '$params.state',
                        sort: '$params.sort',
                        description: '$params.description'
                    }
                }]
            }, {
                value:'删除',
                onClick:[{
                    action:'confirm',
                    title:'删除数据',
                    message:'是否删除该条数据?',
                    confirm:[{
                        action:'service',
                        path: 'shop/store/classify/remove',
                        params: {
                            id: '$params.id'
                        }
                    },{
                        action: 'call',
                        target: '$widget',
                        method: 'reload'
                    }]
                }]
            }],
            height:500,
        }]
    },{
        region:'center',
        class:[region,center],
        items:[{
            id: 'title',
            widget: 'public-titleright',
            title: '编辑修改',
        },{
            class:itemInfo,
            items:[{
                id:'form',
                widget:'form',
                layout: {
                    type:'gridform',
                    column:2
                },
                fields:[{
                    class: formInput,
                    itemId:'pid',
                    placeholder:'请输入上级分类',
                    widget:'select',
                    dictionary: 'shop/common/dictionary/category',
                    label:'上级分类',
                },{
                    class: formInput,
                    itemId:'cate_name',
                    placeholder:'请输入分类名称',
                    widget:'text',
                    label:'分类名称',
                },{
                    class: formInput,
                    itemId:'name_phonetic',
                    placeholder:'请输入名称首拼',
                    widget:'text',
                    label:'名称首拼',
                },{
                    class: formInput,
                    itemId:'alias',
                    placeholder:'请输入别名',
                    widget:'text',
                    label:'别名',
                },{
                    class: formInput,
                    itemId:'alias_phonetic',
                    placeholder:'请输入别名首拼',
                    widget:'text',
                    label:'别名首拼',
                },{
                    class: formInput,
                    itemId:'keywords',
                    placeholder:'请输入关键词',
                    widget:'text',
                    label:'关键词',
                },{
                    class: formInput,
                    itemId:'pic',
                    placeholder:'请输入图标',
                    widget: 'image-uploader-single',
                    label:'图标',
                },{
                    class: formInput,
                    itemId:'big_pic',
                    placeholder:'请输入分类大图',
                    widget: 'image-uploader-single',
                    label:'分类大图',
                },{
                    class: formInput,
                    itemId:'is_show',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '是'
                    }, {
                        value: 0,
                        text: '否'
                    }],
                    label:'是否推荐',
                },{
                    class: formInput,
                    itemId:'is_navi',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '是'
                    }, {
                        value: 0,
                        text: '否'
                    }],
                    label:'导航',
                },{
                    class: formInput,
                    itemId:'state',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '启用'
                    }, {
                        value: 0,
                        text: '禁用'
                    }],
                    label:'状态',
                },{
                    class: formInput,
                    itemId:'sort',
                    placeholder:'请输入排序',
                    widget:'text',
                    label:'排序',
                    value:0
                },{
                    class: formInput,
                    itemId:'description',
                    placeholder:'请输入描述信息',
                    widget:'text',
                    label:'描述信息',
                }],
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            action: 'service',
                            path: 'shop/store/classify/save',
                            params: "$widget.value",
                        },{
                            action:'call',
                            target:'$widgets.tree',
                            method:'reload'
                        },{
                            action: 'call',
                            target: '$widgets.form',
                            method: 'reset'
                        }]
                    }]
                }],
                flex:true
            }]
        }]
    }]
} ;
