import {
    center,
    grid,
    search,
    header,
    button
} from '@/css/VueCss.module.scss';
export default {
    // title: '回收站',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search-datesearch',
                times: [{
                    label: '今日',
                    value: 'today'
                }, {
                    label: '昨日',
                    value: 'yesterday'
                },{
                    label: '近7日',
                    value: 'lately7'
                },{
                    label: '近30日',
                    value: 'lately30'
                }],
                rangeTimes: false,
                text:{
                    field: 'keywords',
                    placeholder: '请输入商品名称'
                },
                onSearch: [{
                    action:'call',
                    target:'$widgets.table',
                    method:'load',
                    params: {
                        '...': '$params',
                        state:1,
                        is_trashed:1,
                        keywordsBy: '"store_name"',
                    }
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '彻底删除',
                    onClick:[{
                        action:'confirm',
                        title:'删除数据',
                        message:'是否删除勾选数据?',
                        confirm:[{
                            var:'checkedItemIds',
                            action:'array-value',
                            target:'$widgets.table.checkedRows',
                            key:'id'
                        },{
                            action:'service',
                            path: 'shop/store/product/allremove',
                            params:{
                                id:'$vars.checkedItemIds',
                                is_forcedel:1
                            }
                        },{
                            action: 'call',
                            target: '$widgets.table',
                            method: 'reload'
                        }]
                    }]
                }]
            }]
        }, {
            id: 'table',
            class: grid,
            border:true,
            widget: 'table',
            pageSize: 20,
            services:{
                read: {
                    path: 'shop/store/product/del_list',
                    params:{
                        state:1,
                        is_trashed:1,
                    },
                },
            },
            columns: [{
                type:'selection'
            },{
                prop: 'store_name',
                label: '商品名称',
            },{
                prop: 'cate_id',
                label: '商品类型',
                width: 120,
                dictionary: 'shop/common/dictionary/category',
            },{
                prop: 'image',
                type: 'image',
                label: '商品图',
                width: 100
            },{
                prop: 'price',
                label: '商品售价',
                width: 120,
            },{
                prop: 'sales',
                label: '销量',
                width: 80,
            },{
                prop: 'stock',
                label: '库存',
                width: 80,
            },{
                prop: 'sort',
                label: '排序',
                width: 80,
            },{
                label:'操作',
                type:'operation',
                width:130,
                buttons:[{
                    value:'恢复',
                    onClick:[{
                        action:'confirm',
                        title:'恢复数据',
                        message:'是否恢复该条数据?',
                        confirm:[{
                            action:'service',
                            path:'shop/store/product/remove',
                            params: {
                                id:'$params.id',
                                is_resolve:1
                            }
                        },{
                            action:'call',
                            target:'$widget',
                            method:'reload'
                        }]
                    }]
                },{
                    value:'彻底删除',
                    onClick:[{
                        action:'confirm',
                        title:'彻底删除数据',
                        message:'是否彻底删除该条数据?',
                        confirm:[{
                            action:'service',
                            path:'shop/store/product/remove',
                            params: {
                                id:'$params.id',
                                is_forcedel:1,
                            }
                        },{
                            action:'call',
                            target:'$widget',
                            method:'reload'
                        }]
                    }]
                }]
            }],
            flex: true,
        }]
    }]
};
