import {
    center,
    grid,
    header,
    button,
    search,
} from '@/css/VueCss.module.scss';
export default {
    // title: '仓库中',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search-datesearch',
                times: [{
                    label: '今日',
                    value: 'today'
                }, {
                    label: '昨日',
                    value: 'yesterday'
                },{
                    label: '近7日',
                    value: 'lately7'
                },{
                    label: '近30日',
                    value: 'lately30'
                }],
                rangeTimes: false,
                text:{
                    field: 'keywords',
                    placeholder: '请输入商品名称'
                },
                onSearch: [{
                    action:'call',
                    target:'$widgets.table',
                    method:'load',
                    params: {
                        '...': '$params',
                        state:1,
                        is_show:0,
                        keywordsBy: '"store_name"',
                    }
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick: [{
                        action:'dialog-show',
                        path:'Shop/Commodity/List/Edit',
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.table',
                            method: 'reload'
                        }],
                        cancelActions:[{
                            action:'print',
                            description:'取消对话框'
                        }]
                    }]
                }, {
                    class: button,
                    widget: 'button',
                    value: '删除',
                    onClick:[{
                        action:'confirm',
                        title:'删除数据',
                        message:'是否删除勾选数据?',
                        confirm:[{
                            var:'checkedItemIds',
                            action:'array-value',
                            target:'$widgets.table.checkedRows',
                            key:'id'
                        },{
                            action:'service',
                            path: 'shop/store/product/allremove',
                            params:{
                                id:'$vars.checkedItemIds'
                            }
                        },{
                            action: 'call',
                            target: '$widgets.table',
                            method: 'reload'
                        }]
                    }]
                // }, {
                //     class: button,
                //     widget: 'button',
                //     type: 'danger',
                //     value: '商品采集',
                // }, {
                //     class: button,
                //     widget: 'button',
                //     type: 'danger',
                //     value: '导出',
                }]
            }]
        }, {
            iid: 'table',
            class: grid,
            border:true,
            widget: 'table',
            pageSize: 20,
            services:{
                read: {
                    path: 'shop/store/product/list',
                    params:{
                        state:1,
                        is_show:0
                    },
                },
                update: 'shop/store/product/simple_save',
            },
            columns: [{
                type:'selection'
            },{
                prop: 'store_name',
                label: '商品名称',
            },{
                prop: 'cate_id',
                label: '商品类型',
                width: 120,
                dictionary: 'shop/common/dictionary/category',
            },{
                prop: 'image',
                type: 'image',
                label: '商品图',
                width: 100
            },{
                prop: 'price',
                label: '商品售价',
                width: 120,
            },{
                prop: 'sales',
                label: '销量',
                width: 80,
            },{
                prop: 'stock',
                label: '库存',
                width: 80,
            },{
                prop: 'sort',
                label: '排序',
                width: 80,
            },{
                prop: 'is_show',
                label: '状态',
                width: 100,
                type:'switch',
                active:{
                    text:'上架',
                    value:1
                },
                inactive:{
                    text:'下架',
                    value:0
                }
            },{
                label:'操作',
                type:'operation',
                width:180,
                buttons:[{
                    value:'编辑',
                    onClick:[{
                        action:'dialog-show',
                        path:'Shop/Commodity/List/Edit',
                        params: {
                            form:'$params',
                            title:"'编辑商品信息'"
                        },
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.table',
                            method: 'reload'
                        }],
                        cancelActions:[{
                            action:'print',
                            description:'取消对话框'
                        }]
                    }]
                },{
                    value:'详情',
                    onClick:[{
                        action:'dialog-show',
                        path:'Shop/Commodity/List/Detail',
                        params: '$params',
                    }]
                },{
                    value:'移到回收站',
                    onClick:[{
                        action:'confirm',
                        title:'移入回收站',
                        message:'确定要移入回收站吗？',
                        confirm:[{
                            action:'service',
                            path:'shop/store/product/remove',
                            params: {
                                id:'$params.id',
                            }
                        },{
                            action:'call',
                            target:'$widget',
                            method:'reload'
                        }]
                    }]
                }]
            }],
            flex: true,
        }]
    }]
};
