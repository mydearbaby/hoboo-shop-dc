import {
    center,
    grid,
    header,
    button,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '仓库中',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search-datesearch',
                times: [{
                    label: '今日',
                    value: 'today'
                }, {
                    label: '昨日',
                    value: 'yesterday'
                },{
                    label: '近7日',
                    value: 'lately7'
                },{
                    label: '近30日',
                    value: 'lately30'
                }],
                rangeTimes: false,
                selects: [{
                    field: 'category_uuid',
                    placeholder: '请选择商品类型',
                    dictionary: {
                        path: 'common/dictionary/config',
                        params: {
                            name: 'crm_contract_category',
                        },
                    },
                },{
                    field: 'shop_state',
                    placeholder: '请选择商品状态',
                    dictionary: 'shop/common/static/shop_state',
                }],
                text:{
                    field: 'keywords',
                    placeholder: '请输入商品名称'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params:'$params'
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick: [{
                        action:'dialog-show',
                        // path:'Contract/Edit',
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }],
                        cancelActions:[{
                            action:'print',
                            description:'取消对话框'
                        }]
                    }]
                }, {
                    class: button,
                    widget: 'button',
                    value: '删除',
                    onClick:[{
                        action:'confirm',
                        title:'删除数据',
                        message:'是否删除勾选数据?',
                        confirm:[{
                            var:'checkedItemIds',
                            action:'array-value',
                            target:'$widgets.grid.checkedItems',
                            key:'id'
                        },{
                            action:'print',
                            description:'列表勾选项',
                            value:'$vars.checkedItemIds'
                        },{
                            action:'service',
                            path: 'crm/contract/allremove',
                            params:{
                                id:'$vars.checkedItemIds'
                            }
                        },{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }]
                    }]
                }, {
                    class: button,
                    widget: 'button',
                    value: '上架',
                    onClick:[{
                        action:'confirm',
                        title:'上架数据',
                        message:'是否上架勾选数据?',
                        confirm:[{
                            var:'checkedItemIds',
                            action:'array-value',
                            target:'$widgets.grid.checkedItems',
                            key:'id'
                        },{
                            action:'print',
                            description:'列表勾选项',
                            value:'$vars.checkedItemIds'
                        },{
                            action:'service',
                            // path: 'crm/contract/allremove',
                            params:{
                                id:'$vars.checkedItemIds'
                            }
                        },{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }]
                    }]
                }, {
                    class: button,
                    widget: 'button',
                    value: '下架',
                    onClick:[{
                        action:'confirm',
                        title:'下架数据',
                        message:'是否下架勾选数据?',
                        confirm:[{
                            var:'checkedItemIds',
                            action:'array-value',
                            target:'$widgets.grid.checkedItems',
                            key:'id'
                        },{
                            action:'print',
                            description:'列表勾选项',
                            value:'$vars.checkedItemIds'
                        },{
                            action:'service',
                            // path: 'crm/contract/allremove',
                            params:{
                                id:'$vars.checkedItemIds'
                            }
                        },{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }]
                    }]
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            pageSize: 20,
            columns: [{
                prop: 'name',
                label: '商品名称',
            },{
                prop: 'name',
                label: '商品类型',
            },{
                prop: 'name',
                label: '商品名称',
            },{
                prop: 'name',
                label: '商品售价',
            },{
                prop: 'name',
                label: '销量',
            },{
                prop: 'name',
                label: '库存',
            },{
                prop: 'shop_state',
                label: '状态',
            }],
            buttons:[{
                value:'编辑',
                onClick:[{
                    action:'dialog-show',
                    // path:'Contract/Edit',
                    params: '$params',
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            },{
                value:'详情',
                onClick:[{
                    action:'dialog-show',
                    // path:'Contract/Details',
                    params: '$params',
                }]
            },{
                value:'删除',
                type:'danger',
                onClick:[{
                    action:'confirm',
                    title:'删除数据',
                    message:'是否删除该条数据?',
                    confirm:[{
                        action:'service',
                        // path:'crm/contract/remove',
                        params: {
                            id:'$params.id'
                        }
                    },{
                        action:'call',
                        target:'$widget',
                        method:'reload'
                    }]
                }]
            }],
            flex: true,
            operationWidth:130,
            operationButtonCount:3,
            enableCheckboxColumn: true,
        }]
    }]
};
