import {
    center,
    grid,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '评论',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search-datesearch',
                times: [{
                    label: '今日',
                    value: 'today'
                }, {
                    label: '昨日',
                    value: 'yesterday'
                },{
                    label: '近7日',
                    value: 'lately7'
                },{
                    label: '近30日',
                    value: 'lately30'
                }],
                rangeTimes: true,
                // selects: [{
                //     field: 'is_reply',
                //     placeholder: '请选择评价状态',
                //     dictionary: 'shop/common/static/is_reply',
                // }],
                text:{
                    field: 'keywords',
                    placeholder: '请输入用户名称'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params: {
                        '...': '$params',
                        keywordsBy: '"nickname"',
                    }
                }]
            }]
        },{
            id:'grid',
            class: grid,
            widget:'grid',
            service: 'shop/store/comment/list',
            pageSize:20,
            columns: [{
                prop: 'nickname',
                label: '用户名称',
                width: 120,
            },{
                prop: 'store_product.store_name',
                label: '商品名称',
                minWidth: 220,
            },  {
                prop: 'comment',
                label: '评论内容',
            }, {
                prop: 'merchant_reply_content',
                label: '回复内容',
            },{
                prop: 'is_reply',
                label: '回复状态',
                dictionary: 'shop/common/static/is_reply',
                width: 80,
            }, {
                prop: 'create_time',
                label: '评论时间',
                width: 165,
            }],
            buttons:[{
                value:'回复',
                onClick:[{
                    action:'dialog-show',
                    path:'Shop/Commodity/Comment/Reply',
                    params: {
                        form:'$params',
                        title:"'回复内容'"
                    },
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            },{
                value:'删除',
                type:'danger',
                onClick:[{
                    action:'confirm',
                    title:'删除数据',
                    message:'是否删除该条数据?',
                    confirm:[{
                        action:'service',
                        path:'shop/store/comment/remove',
                        params: {
                            id:'$params.id'
                        }
                    },{
                        action:'call',
                        target:'$widget',
                        method:'reload'
                    }]
                }]
            }],
            flex:true,
        }]
    }]
} ;

