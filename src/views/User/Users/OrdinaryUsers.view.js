import {
    center,
    right,
    grid,
    input,
    header,
    button,
    search,
} from '@/css/VueCss.module.scss' ;

export default {
    title: '人员管理',
    order: 1,
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: [
                    search
                ],
                widget: 'search',
                onSearch: [{
                    action: 'service',
                    params: {
                        page_size: '$widgets.grid.pageSize',
                        keywords: '$params.keywords',
                        is_show: '$params.is_show',
                    },
                    path: 'system/department/user/list',
                    actions: [{
                        action: 'set',
                        target: '$widgets.grid',
                        field: 'value',
                        value: '$params.data.items'
                    }, {
                        action: 'set',
                        target: '$widgets.grid',
                        field: 'total',
                        value: '$params.data.total'
                    }]
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick: [{
                        action:'dialog-show',
                        path:'System/Department/User',
                        applyActions:[{
                            action: 'service',
                            path: 'system/department/user/list',
                            params: {
                                page_size: '$widgets.grid.pageSize',
                                page_no: '$widgets.grid.page',
                                keywords: '$widgets.search.value.keywords',
                                is_show: '$widgets.search.value.is_show',
                            },
                            actions: [{
                                action: 'set',
                                target: '$widgets.grid',
                                field: 'value',
                                value: '$params.data.items'
                            }, {
                                action: 'set',
                                target: '$widgets.grid',
                                field: 'total',
                                value: '$params.data.total'
                            }]
                        }],
                        cancelActions:[{
                            action:'print',
                            description:'取消对话框'
                        }]
                    }, {
                        action: 'form-reset',
                        target: '$widgets.form'
                    }, {
                        action: 'grid-selection-clear',
                        target: '$widgets.grid'
                    }]
                }, {
                    class: button,
                    widget: 'button',
                    value: '删除',
                    onClick:[{
                        action:'confirm',
                        title:'删除数据',
                        message:'是否删除勾选数据?',
                        confirm:[{
                            var:'checkedItemIds',
                            action:'array-value',
                            target:'$widgets.grid.checkedItems',
                            key:'uid'
                        },{
                            action:'print',
                            description:'列表勾选项',
                            value:'$vars.checkedItemIds'
                        },{
                            action:'service',
                            path:'system/department/user/del',
                            params:{
                                uid:'$vars.checkedItemIds'
                            }
                        },{
                            action:'message',
                            message: '删除成功',
                            type: 'success'
                        },{
                            action: 'service',
                            path: 'system/department/user/list',
                            params:{
                                page_size:'$widgets.grid.pageSize',
                                page_no:'$widgets.grid.page',
                                keywords: '$widgets.search.value.keywords',
                                is_show: '$widgets.search.value.is_show',
                            },
                            actions: [{
                                action:'set',
                                target:'$widgets.grid',
                                field:'value',
                                value:'$params.data.items'
                            },{
                                action:'set',
                                target:'$widgets.grid',
                                field:'total',
                                value:'$params.data.total'
                            }]
                        }]
                    }]
                },{
                    class: button,
                    widget: 'basic-groupbutton',
                    value: '删除',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'grid/item/detail/Drawer',
                        params: {
                            value: '`第 ${$view.count ++} 次点击`'
                        }
                    }]
                }]
            }]
        },{
            id:'grid',
            class: grid,
            widget:'grid',
            pageSize:20,
            columns:[{
                prop:'account',
                label:'用户账号',
                minWidth: 120
            },{
                prop:'password',
                label:'用户密码',
                minWidth: 120
            },{
                prop:'real_name',
                label:'真实姓名',
                minWidth: 150
            },{
                prop:'birthday',
                label:'生日日期',
                width: 120
            },{
                prop:'card_id',
                label:'身份证号码',
                width: 120
            },{
                prop:'nickname',
                label:'用户昵称',
                minWidth: 150
            },{
                prop:'phone',
                label:'手机号码',
                width: 120
            },{
                prop:'add_time',
                label:'添加时间',
                width: 180
            },{
                prop:'add_ip',
                label:'添加ip',
                width: 150
            },{
                prop:'last_time',
                label:'最后登录时间',
                width: 180
            },{
                prop:'last_ip',
                label:'最后登录IP',
                width: 150
            },{
                prop:'user_type',
                label:'用户类型',
                width: 120
            },{
                prop:'address',
                label:'详细地址',
                width: 150
            },{
                prop:'record_phone',
                label:'记录临时电话',
                width: 120
            },{
                prop:'mark',
                label:'用户备注',
                minWidth: 120
            }],
            buttons:[{
                value:'编辑',
                onClick:[{
                    action:'dialog-show',
                    path:'System/Department/User',
                    params: '$params',
                    applyActions:[{
                        action: 'service',
                        path: 'system/department/user/list',
                        params: {
                            page_size: '$widgets.grid.pageSize',
                            page_no: '$widgets.grid.page',
                            keywords: '$widgets.search.value.keywords',
                            is_show: '$widgets.search.value.is_show',
                        },
                        actions: [{
                            action: 'set',
                            target: '$widgets.grid',
                            field: 'value',
                            value: '$params.data.items'
                        }, {
                            action: 'set',
                            target: '$widgets.grid',
                            field: 'total',
                            value: '$params.data.total'
                        }]
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            },{
                value:'删除',
                type:'danger',
                onClick:[{
                    action:'confirm',
                    title:'删除数据',
                    message:'是否删除该条数据?',
                    confirm:[{
                        action:'service',
                        path:'system/department/user/del',
                        params:'$params'
                    },{
                        action:'message',
                        message: '删除成功',
                        type: 'success'
                    },{
                        action: 'service',
                        path: 'system/department/user/list',
                        params:{
                            page_size:'$widgets.grid.pageSize',
                            page_no:'$widgets.grid.page',
                            keywords: '$widgets.search.value.keywords',
                            is_show: '$widgets.search.value.is_show',
                        },
                        actions: [{
                            action:'set',
                            target:'$widgets.grid',
                            field:'value',
                            value:'$params.data.items'
                        },{
                            action:'set',
                            target:'$widgets.grid',
                            field:'total',
                            value:'$params.data.total'
                        }]
                    }]
                }]
            }],
            flex:true,
            enableCheckboxColumn:true,
            onPage:[{
                action:'service',
                params:{
                    page_size:'$widget.pageSize',
                    page_no:'$params.page',
                    keywords:'$widgets.search.value.keywords'
                },
                path:'system/department/user/list',
                actions:[{
                    action:'set',
                    target:'$widget',
                    field:'value',
                    value:'$params.data.items'
                },{
                    action:'set',
                    target:'$widget',
                    field:'total',
                    value:'$params.data.total'
                }]
            }],
            onMounted:[{
                action:'service',
                params:{
                    page_size:'$widget.pageSize'
                },
                path:'system/department/user/list',
                actions:[{
                    action:'set',
                    target:'$widget',
                    field:'value',
                    value:'$params.data.items'
                },{
                    action:'set',
                    target:'$widget',
                    field:'total',
                    value:'$params.data.total'
                }]
            }],
        }]
    }]
} ;