import {
    center,
    right,
    grid,
    formInput,
    header,
    button,
    search,
    rightContent
} from '@/css/VueCss.module.scss';
export default {
    title: '领取方式',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search',
                text:{
                    field: 'keywords',
                    placeholder: '请输入名称'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params: {
                        // name: '"crm_contract_category"',
                        keywords: '$params.keywords',
                    }
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick:[{
                        action:'call',
                        target:'$widgets.form',
                        method:'reset'
                    },{
                        action:'call',
                        target:'$widgets.grid',
                        method:'clearSelection'
                    }]
                }, {
                    class: button,
                    widget: 'button',
                    value: '删除',
                    onClick:[{
                        action:'confirm',
                        title:'删除数据',
                        message:'是否删除勾选数据?',
                        confirm:[{
                            var:'checkedItemIds',
                            action:'array-value',
                            target:'$widgets.grid.checkedItems',
                            key:'id'
                        },{
                            action:'print',
                            description:'列表勾选项',
                            value:'$vars.checkedItemIds'
                        },{
                            action:'service',
                            // path: 'system/dictionary/config/allremove',
                            params:{
                                module:'"system_module"',
                                name: '"crm_contract_category"',
                                id:'$vars.checkedItemIds'
                            }
                        },{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }]
                    }]
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            pageSize: 20,
            columns:[{
                prop:'value',
                label:'名称',
            },{
                prop:'description',
                label:'备注信息'
            }],
            flex: true,
            enableCheckboxColumn: true,
            onRowselect:[{
                action:'call',
                target:'$widgets.form',
                method:'setValue',
                params:'$params'
            }],
        }]
    },{
        region: 'right',
        class: right,
        items:[{
            id: 'title',
            widget: 'public-titleright',
            title: '编辑修改',
        },{
            class:rightContent,
            items:[{
                id:'form',
                widget:'form',
                fields:[{
                    class: formInput,
                    itemId:'value',
                    placeholder:'请输入名称',
                    widget:'text',
                    label:'名称',
                },{
                    class: formInput,
                    itemId:'value',
                    placeholder:'请输入备注信息',
                    widget:'text',
                    label:'备注信息',
                }],
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            action: 'service',
                            // path: 'system/dictionary/config/save',
                            params: "$widget.value",
                        },{
                            action:'call',
                            target:'$widgets.grid',
                            method:'reload'
                        }]
                    }]
                }],
                flex:true
            }]
        }]
    }]
};
