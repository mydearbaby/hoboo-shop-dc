import {
    center,
    grid,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '领取记录',
    region: 'center',
    class: center,
    layout: 'vertical',
    items: [{
        items: [{
            id: 'search',
            class: search,
            widget: 'search',
            text:{
                field: 'keywords',
                placeholder: '请输入优惠券名称'
            },
            onSearch: [{
                action:'grid-load',
                target:'$widgets.grid',
                params: {
                    '...':'$params',
                    keywordsBy: '"coupon_title"',
                }
            }]
        }]
    }, {
        id: 'grid',
        class: grid,
        widget: 'grid',
        pageSize: 20,
        service: 'shop/market/coupon/record/list',
        columns: [{
            prop: 'coupon_title',
            label: '优惠券名称',
        },{
            prop: 'uid',
            label: '领取人',
            dictionary: 'shop/common/dictionary/user'
        },{
            prop: 'coupon_price',
            label: '面值',
        },{
            prop: 'use_min_price',
            label: '最低消费额',
        },{
            prop: 'use_time',
            label: '开始使用时间',
        },{
            prop: 'scs_type',
            label: '获取方式',
        }],
        flex: true,
    }]
}
