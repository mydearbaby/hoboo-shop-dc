import {
    center,
    grid,
    header,
    button,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '优惠券列表',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search',
                text:{
                    field: 'keywords',
                    placeholder: '请输入优惠券名称'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params: {
                        '...':'$params',
                        keywordsBy: '"coupon_title"',
                    }
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick: [{
                        action:'dialog-show',
                        path:'Shop/Market/Coupon/Edit',
                        params: {
                            title:"'新增优惠券'"
                        },
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }],
                        cancelActions:[{
                            action:'print',
                            description:'取消对话框'
                        }]
                    }]
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            service: 'shop/market/coupon/list/list',
            pageSize: 20,
            columns: [{
                prop: 'coupon_title',
                label: '优惠券名称',
            },{
                prop: 'sci_type',
                label: '优惠券类型',
                dictionary: 'shop/common/static/coupon_type'
            },{
                prop: 'receive_type',
                label: '领取方式',
                dictionary: 'shop/common/static/receive_type'
            },{
                prop: 'start_time',
                label: '领取时间',
            },{
                prop: 'total_count',
                label: '领取数量',
            },{
                prop: 'remain_count',
                label: '剩余数量',
            },{
                prop: 'coupon_price',
                label: '兑换的面值',
            },{
                prop: 'coupon_time',
                label: '有效期限(天)',
            },{
                prop: 'start_use_time',
                label: '使用时间',
            }],
            buttons:[{
                value:'编辑',
                onClick:[{
                    action:'dialog-show',
                    path:'Shop/Market/Coupon/Edit',
                    params: {
                        title:"'编辑优惠券信息'",
                        form: '$params',
                    },
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            },{
                value:'详情',
                onClick:[{
                    action:'dialog-show',
                    path:'Shop/Market/Coupon/Detail',
                    params: '$params',
                }]
            },{
                value:'删除',
                type:'danger',
                onClick:[{
                    action:'confirm',
                    title:'删除数据',
                    message:'是否删除该条数据?',
                    confirm:[{
                        action:'service',
                        path:'shop/market/coupon/list/remove',
                        params: {
                            id:'$params.id'
                        }
                    },{
                        action:'call',
                        target:'$widget',
                        method:'reload'
                    }]
                }]
            }],
            flex: true,
            operationWidth:130,
            operationButtonCount:3,
            // enableCheckboxColumn: true,
        }]
    }]
};
