import {
    center,
    grid,
    header,
    button,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '砍价商品',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search',
                selects: [{
                    field: 'category_uuid',
                    placeholder: '请选择活动状态',
                    // dictionary: {
                    //     path: 'common/dictionary/config',
                    //     params: {
                    //         name: 'crm_contract_category',
                    //     },
                    // },
                }],
                text:{
                    field: 'keywords',
                    placeholder: '请输入砍价名称'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params:'$params'
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick: [{
                        action:'dialog-show',
                        // path:'Contract/Edit',
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }],
                        cancelActions:[{
                            action:'print',
                            description:'取消对话框'
                        }]
                    }]
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            pageSize: 20,
            columns: [{
                prop: 'name',
                label: '砍价名称',
            },{
                prop: 'name',
                label: '砍价价格',
            },{
                prop: 'name',
                label: '最低价',
            },{
                prop: 'name',
                label: '参与人数',
            },{
                prop: 'name',
                label: '帮忙砍价人数',
            },{
                prop: 'name',
                label: '砍价成功人数',
            },{
                prop: 'name',
                label: '限量',
            },{
                prop: 'name',
                label: '限量剩余',
            },{
                prop: 'name',
                label: '活动状态',
            },{
                prop: 'name',
                label: '结束时间',
            }],
            buttons:[{
                value:'编辑',
                onClick:[{
                    action:'dialog-show',
                    // path:'Contract/Edit',
                    params: '$params',
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            },{
                value:'详情',
                onClick:[{
                    action:'dialog-show',
                    // path:'Contract/Details',
                    params: '$params',
                }]
            },{
                value:'删除',
                type:'danger',
                onClick:[{
                    action:'confirm',
                    title:'删除数据',
                    message:'是否删除该条数据?',
                    confirm:[{
                        action:'service',
                        // path:'crm/contract/remove',
                        params: {
                            id:'$params.id'
                        }
                    },{
                        action:'call',
                        target:'$widget',
                        method:'reload'
                    }]
                }]
            }],
            flex: true,
            operationWidth:130,
            operationButtonCount:3,
            enableCheckboxColumn: true,
        }]
    }]
};
