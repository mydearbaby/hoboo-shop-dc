import {
    center,
    grid,
    header,
    button,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '砍价列表',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search-datesearch',
                rangeTimes: true,
                selects: [{
                    field: 'category_uuid',
                    placeholder: '请选择砍价状态',
                    // dictionary: {
                    //     path: 'common/dictionary/config',
                    //     params: {
                    //         name: 'crm_contract_category',
                    //     },
                    // },
                }],
                text:{
                    field: 'keywords',
                    placeholder: '请输入砍价商品名称'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params:'$params'
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            pageSize: 20,
            columns: [{
                prop: 'name',
                label: '发起用户',
            },{
                prop: 'name',
                label: '开启时间',
            },{
                prop: 'name',
                label: '砍价商品',
            },{
                prop: 'name',
                label: '最低价',
            },{
                prop: 'name',
                label: '当前价',
            },{
                prop: 'name',
                label: '总砍价次数',
            },{
                prop: 'name',
                label: '剩余砍价次数',
            },{
                prop: 'name',
                label: '结束时间',
            },{
                prop: 'name',
                label: '状态',
            }],
            buttons:[{
                value:'详情',
                onClick:[{
                    action:'dialog-show',
                    // path:'Contract/Details',
                    params: '$params',
                }]
            }],
            flex: true,
            enableCheckboxColumn: true,
        }]
    }]
};
