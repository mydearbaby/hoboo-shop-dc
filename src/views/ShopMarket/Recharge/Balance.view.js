import {
    center,
    right,
    grid,
    formInput,
    header,
    button,
    search,
    rightContent
} from '@/css/VueCss.module.scss';
export default {
    title: '充值余额',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick:[{
                        action:'call',
                        target:'$widgets.form',
                        method:'reset'
                    },{
                        action:'call',
                        target:'$widgets.grid',
                        method:'clearSelection'
                    }]
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            pageSize: 20,
            columns:[{
                prop:'name',
                label:'售价',
            },{
                prop:'description',
                label:'赠送'
            },{
                prop:'description',
                label:'备注信息'
            }],
            flex: true,
            enableCheckboxColumn: true,
            onRowselect:[{
                action:'call',
                target:'$widgets.form',
                method:'setValue',
                params:'$params'
            }],
        }]
    },{
        region: 'right',
        class: right,
        items:[{
            id: 'title',
            widget: 'public-titleright',
            title: '编辑修改',
        },{
            class:rightContent,
            items:[{
                id:'form',
                widget:'form',
                fields:[{
                    class: formInput,
                    itemId:'value',
                    placeholder:'请输入售价',
                    widget:'text',
                    label:'售价',
                },{
                    class: formInput,
                    itemId:'value',
                    placeholder:'请输入赠送',
                    widget:'text',
                    label:'赠送',
                },{
                    class: formInput,
                    itemId:'value',
                    placeholder:'请输入备注信息',
                    widget:'text',
                    label:'备注信息',
                }],
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            action: 'service',
                            // path: 'system/dictionary/config/save',
                            params: "$widget.value",
                        },{
                            action:'call',
                            target:'$widgets.grid',
                            method:'reload'
                        }]
                    }]
                }],
                flex:true
            }]
        }]
    }]
};
