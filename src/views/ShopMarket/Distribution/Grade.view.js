import {
    formInput
} from '@/css/VueCss.module.scss' ;
export default {
    title:'分销等级',
    template:{
        path:'columns/two',
        params:{
            services:{
                save:{
                    path:'shop/market/distribution/grade/save',
                },
                remove:{
                    path:'shop/market/distribution/grade/remove',
                },
                list:{
                    path:'shop/market/distribution/grade/list',
                }
            },
            search:{
                selects:[
                    'state'
                    // 这里增加与原来一样的搜索 selects 配置
                ],
                extraParams:{
                    // 这里添加额外参数
                    
                }
            },
            form:{
                fields:[{
                    class: formInput,
                    itemId:'name',
                    placeholder:'请输入名称',
                    widget:'text',
                    label:'名称',
                },{
                    class: formInput,
                    itemId:'grade',
                    placeholder:'请输入等级',
                    widget:'text',
                    label:'等级',
                },{
                    class: formInput,
                    itemId:'one_brokerage',
                    placeholder:'请输入一级返佣上浮比例(%)',
                    widget:'text',
                    label:'一级返佣上浮比例(%)',
                },{
                    class: formInput,
                    itemId:'two_brokerage',
                    placeholder:'请输入二级返佣上浮比例(%)',
                    widget:'text',
                    label:'二级返佣上浮比例(%)',
                },{
                    class: formInput,
                    itemId:'state',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '启用'
                    }, {
                        value: 0,
                        text: '禁用'
                    }],
                    label:'状态',
                }],
            },
            grid:{
                columns:[{
                    prop:'name',
                    label:'名称',
                },{
                    prop:'grade',
                    label:'等级',
                },{
                    prop:'one_brokerage',
                    label:'一级返佣上浮比例(%)'
                },{
                    prop:'two_brokerage',
                    label:'二级返佣上浮比例(%)'
                }, 'state'],
            },
            
        }
    }
} ;