
import {
    center,
    grid,
    search
} from '@/css/VueCss.module.scss' ;

export default {
    title: '登录日志',
    order: 1,
    items: [{
        region: 'center',
        class: center,
        items: [{
            items: [{
                id: 'search',
                class: search,
                buttons: ["今天", "昨天", "近7天", "近30天"],
                widget: 'search-datesearch',
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params:'$params'
                }]
            }]
        },{
            id:'grid',
            class: grid,
            widget:'grid',
            service: {
                path: 'shop/market/distribution/spread/list',
                params: {
                    
                },
            },
            pageSize:20,
            columns:[{
                prop:'id',
                label:'ID',
            },{
                prop:'avatar',
                label:'头像',
            },{
                prop:'phone',
                label:'用户信息'
            },{
                prop:'spread_count',
                label:'推广用户数量'
            },{
                prop:'order_count',
                label:'订单数量'
            }, {
                prop:'order_price',
                label:'订单金额'
            },{
                prop:'agent_level',
                label:'分销等级'
            },{
                prop:'brokerage_money',
                label:'账户佣金'
            },{
                prop:'extract_count_price',
                label:'已提现金额'
            },{
                prop:'extract_count_num',
                label:'提现次数'
            },{
                prop:'not_extract',
                label:'未提现金额'
            },{
                prop:'spread_name',
                label:'上级推广人'
            }],
            flex:true,
            onPage:[{
                action:'service',
                params:{
                    page_size:'$widget.pageSize',
                    page_no:'$params.page',
                    keywords:'$widgets.search.value.keywords'
                },
                path:'shop/market/distribution/spread/list',
                actions:[{
                    action:'set',
                    target:'$widget',
                    field:'value',
                    value:'$params.data.items'
                },{
                    action:'set',
                    target:'$widget',
                    field:'total',
                    value:'$params.data.total'
                }]
            }],
        }]
    }]
} ;
