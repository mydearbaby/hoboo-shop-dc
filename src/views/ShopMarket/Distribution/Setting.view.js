import {
    center,
    grid,
    header,
    button,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '分销设置',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            pageSize: 20,
            columns: [{
                prop: '',
                label: '',
            }],
            flex: true,
            operationWidth:130,
            operationButtonCount:3,
            enableCheckboxColumn: true,
        }]
    }]
};
