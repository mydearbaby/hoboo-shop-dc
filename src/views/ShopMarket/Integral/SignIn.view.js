import {
    center,
    right,
    grid,
    formInput,
    header,
    button,
    rightContent
} from '@/css/VueCss.module.scss';
export default {
    title: '积分签到',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick:[{
                        action:'call',
                        target:'$widgets.form',
                        method:'reset'
                    },{
                        action:'call',
                        target:'$widgets.grid',
                        method:'clearSelection'
                    }]
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            pageSize: 20,
            columns:[{
                prop:'value',
                label:'第几天',
            },{
                prop:'description',
                label:'获取积分'
            },{
                prop:'description',
                label:'状态'
            }],
            flex: true,
            enableCheckboxColumn: true,
            onRowselect:[{
                action:'call',
                target:'$widgets.form',
                method:'setValue',
                params:'$params'
            }],
        }]
    },{
        region: 'right',
        class: right,
        items:[{
            id: 'title',
            widget: 'public-titleright',
            title: '编辑修改',
        },{
            class:rightContent,
            items:[{
                id:'form',
                widget:'form',
                fields:[{
                    class: formInput,
                    itemId:'value',
                    placeholder:'请输入第几天',
                    widget:'text',
                    label:'第几天',
                },{
                    class: formInput,
                    itemId:'value',
                    placeholder:'请输入获取积分',
                    widget:'text',
                    label:'获取积分',
                },{
                    class: formInput,
                    itemId:'state',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '启用'
                    }, {
                        value: 0,
                        text: '禁用'
                    }],
                    label:'状态',
                }],
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            action: 'service',
                            // path: 'system/dictionary/config/save',
                            params: "$widget.value",
                        },{
                            action:'call',
                            target:'$widgets.grid',
                            method:'reload'
                        }]
                    }]
                }],
                flex:true
            }]
        }]
    }]
};
