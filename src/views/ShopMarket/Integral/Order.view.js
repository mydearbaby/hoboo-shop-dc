import {
    center,
    grid,
    header,
    button,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '积分订单',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search-datesearch',
                times: [{
                    label: '今日',
                    value: 'today'
                }, {
                    label: '昨日',
                    value: 'yesterday'
                },{
                    label: '近7日',
                    value: 'lately7'
                },{
                    label: '近30日',
                    value: 'lately30'
                }],
                rangeTimes: true,
                selects: [{
                    field: 'category_uuid',
                    placeholder: '请选择订单状态',
                    // dictionary: {
                    //     path: 'common/dictionary/config',
                    //     params: {
                    //         name: 'crm_contract_category',
                    //     },
                    // },
                }],
                text:{
                    field: 'keywords',
                    placeholder: '请输入商品信息'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params:'$params'
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick: [{
                        action:'dialog-show',
                        // path:'Contract/Edit',
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }],
                        cancelActions:[{
                            action:'print',
                            description:'取消对话框'
                        }]
                    }]
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            pageSize: 20,
            columns: [{
                prop: 'name',
                label: '订单号',
            },{
                prop: 'name',
                label: '用户信息',
            },{
                prop: 'name',
                label: '商品信息',
            },{
                prop: 'name',
                label: '兑换积分',
            },{
                prop: 'name',
                label: '订单状态',
            },{
                prop: 'name',
                label: '下单时间',
            }],
            buttons:[{
                value:'发送货',
                onClick:[{
                    action:'dialog-show',
                    // path:'Contract/Edit',
                    params: '$params',
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            },{
                value:'详情',
                onClick:[{
                    action:'dialog-show',
                    // path:'Contract/Details',
                    params: '$params',
                }]
            }],
            flex: true,
            enableCheckboxColumn: true,
        }]
    }]
};
