import {
    center,
    grid,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '积分日志',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search-datesearch',
                times: [{
                    label: '今日',
                    value: 'today'
                }, {
                    label: '昨日',
                    value: 'yesterday'
                },{
                    label: '近7日',
                    value: 'lately7'
                },{
                    label: '近30日',
                    value: 'lately30'
                }],
                rangeTimes: true,
                text:{
                    field: 'keywords',
                    placeholder: '请输入名称'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params:'$params'
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            pageSize: 20,
            columns: [{
                prop: 'name',
                label: '名称',
            },{
                prop: 'name',
                label: '变动前积分',
            },{
                prop: 'name',
                label: '积分变动',
            },{
                prop: 'name',
                label: '备注信息',
            },{
                prop: 'name',
                label: '用户微信名称',
            },{
                prop: 'name',
                label: '添加时间',
            }],
            flex: true,
        }]
    }]
};
