import {
    center,
    grid,
    header,
    button,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '拼团商品',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search',
                text:{
                    field: 'keywords',
                    placeholder: '请输入拼团名称'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params:'$params'
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick: [{
                        action:'dialog-show',
                        path:'Shop/Market/Collage/Edit',
                        params: {
                            title:"'新增拼团商品'"
                        },
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }],
                        cancelActions:[{
                            action:'print',
                            description:'取消对话框'
                        }]
                    }]
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            service: 'shop/market/collage/commodity/list',
            pageSize: 20,
            columns: [{
                prop: 'title',
                label: '拼团名称',
            },{
                prop: 'people',
                label: '参团人数',
            },{
                prop: 'info',
                label: '简介',
            },{
                prop: 'price',
                label: '价格',
            },{
                prop: 'sales',
                label: '销量',
            },{
                prop: 'stock',
                label: '库存',
            },{
                prop: 'start_time',
                label: '开始时间',
            },{
                prop: 'quota',
                label: '限购总数',
            }],
            buttons:[{
                value:'编辑',
                onClick:[{
                    action:'dialog-show',
                    path:'Shop/Market/Collage/Edit',
                    params: {
                        title:"'编辑拼团商品'",
                        form: '$params',
                    },
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            },{
                value:'详情',
                onClick:[{
                    action:'dialog-show',
                    path:'Shop/Market/Collage/Detail',
                    params: '$params',
                }]
            },{
                value:'删除',
                type:'danger',
                onClick:[{
                    action:'confirm',
                    title:'删除数据',
                    message:'是否删除该条数据?',
                    confirm:[{
                        action:'service',
                        path:'shop/market/collage/commodity/remove',
                        params: {
                            id:'$params.id'
                        }
                    },{
                        action:'call',
                        target:'$widget',
                        method:'reload'
                    }]
                }]
            }],
            flex: true,
            operationWidth:130,
            operationButtonCount:3,
            // enableCheckboxColumn: true,
        }]
    }]
};
