import {
    center,
    grid,
    header,
    button,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '拼团列表',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search-datesearch',
                rangeTimes: true,
                // selects: [{
                //     field: 'cid',
                //     placeholder: '请选择拼团商品',
                //     dictionary: "shop/common/dictionary/collage_product",
                // }],
                text:{
                    field: 'keywords',
                    placeholder: '请输入团长名称'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params: {
                        '...':'$params',
                        keywordsBy: '"nickname"',
                    }
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            service:'shop/market/collage/list/list',
            pageSize: 20,
            columns: [{
                prop: 'avatar',
                label: '头像',
                width: 100
            },{
                prop: 'nickname',
                label: '团长',
            },{
                prop: 'cid',
                label: '拼团商品',
                dictionary: "shop/common/dictionary/collage_product",
            },{
                prop: 'people',
                label: '几人团',
            // },{
            //     prop: 'people',
            //     label: '几人参加',
            },{
                prop: 'order_id',
                label: '订单编号',
            },{
                prop: 'create_time',
                label: '开团时间',
            },{
                prop: 'stop_time',
                label: '结束时间',
            // },{
            //     prop: 'state',
            //     label: '拼团状态',
            }],
            // buttons:[{
            //     value:'详情',
            //     onClick:[{
            //         action:'dialog-show',
            //         path:'Contract/Details',
            //         params: '$params',
            //     }]
            // }],
            flex: true,

        }]
    }]
};
