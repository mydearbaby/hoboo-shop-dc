import {
    center,
    grid,
    header,
    button,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '直播间管理',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search',
                text:{
                    field: 'keywords',
                    placeholder: '请输入直播间名称'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params: {
                        '...':'$params',
                        keywordsBy: '"name"',
                    }
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '添加直播间',
                    onClick: [{
                        action:'dialog-show',
                        path:'Shop/Market/Broadcast/Room/Add',
                        params: {
                            title:"'新增直播间'"
                        },
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }],
                        cancelActions:[{
                            action:'print',
                            description:'取消对话框'
                        }]
                    }]
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            pageSize: 20,
            service: 'shop/market/broadcast/room/list',
            columns: [{
                prop: 'name',
                label: '直播间名称',
            },{
                prop: 'anchor_name',
                label: '主播昵称',
            },{
                prop: 'anchor_wechat',
                label: '主播微信号',
            },{
                prop: 'phone',
                label: '主播手机号',
            },{
                prop: 'start_time',
                label: '直播开始时间',
            },{
                prop: 'end_time',
                label: '直播结束时间',
            },{
                prop: 'live_status',
                label: '直播状态',
                dictionary: 'shop/common/static/live_status',
            }],
            buttons:[{
            //     value:'添加商品',
            //     onClick:[{
            //         action:'dialog-show',
            //         // path:'Contract/Edit',
            //         params: '$params',
            //         applyActions: [{
            //             action: 'call',
            //             target: '$widgets.grid',
            //             method: 'reload'
            //         }],
            //         cancelActions:[{
            //             action:'print',
            //             description:'取消对话框'
            //         }]
            //     }]
            // },{
                value:'详情',
                onClick:[{
                    action:'dialog-show',
                    path:'Shop/Market/Broadcast/Room/Detail',
                    params: '$params',
                }]
            },{
                value:'删除',
                type:'danger',
                onClick:[{
                    action:'confirm',
                    title:'删除数据',
                    message:'是否删除该条数据?',
                    confirm:[{
                        action:'service',
                        path:'shop/market/broadcast/room/remove',
                        params: {
                            id:'$params.id'
                        }
                    },{
                        action:'call',
                        target:'$widget',
                        method:'reload'
                    }]
                }]
            }],
            flex: true,
            // operationWidth:130,
            // operationButtonCount:3,
            // enableCheckboxColumn: true,
        }]
    }]
};
