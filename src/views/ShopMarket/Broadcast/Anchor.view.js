import {
    formInput
} from '@/css/VueCss.module.scss' ;
export default {
    title:'主播管理',
    template:{
        path:'columns/two',
        params:{
            services:{
                save:{
                    path:'shop/market/broadcast/anchor/save',
                },
                remove:{
                    path:'shop/market/broadcast/anchor/remove',
                },
                list:{
                    path:'shop/market/broadcast/anchor/list',
                }
            },
            search:{
                selects:[
                    'state'
                    // 这里增加与原来一样的搜索 selects 配置
                ],
            },
            form:{
                fields:[{
                    class: formInput,
                    itemId:'name',
                    placeholder:'请输入主播名称',
                    widget:'text',
                    label:'主播名称',
                },{
                    class: formInput,
                    itemId:'wechat',
                    placeholder:'请输入主播微信号',
                    widget:'text',
                    label:'主播微信号',
                },{
                    class: formInput,
                    itemId:'phone',
                    placeholder:'请输入手机号',
                    widget:'text',
                    label:'手机号',
                },{
                    class: formInput,
                    itemId:'state',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '启用'
                    }, {
                        value: 0,
                        text: '禁用'
                    }],
                    label:'状态',
                }],
            },
            grid:{
                columns:[{
                    prop:'name',
                    label:'主播名称',
                },{
                    prop:'wechat',
                    label:'主播微信号',
                },{
                    prop:'phone',
                    label:'手机号',
                },'state'],
            },
            
        }
    }
} ;