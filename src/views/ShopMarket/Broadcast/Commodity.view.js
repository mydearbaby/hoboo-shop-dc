import {
    formInput
} from '@/css/VueCss.module.scss' ;
export default {
    title:'直播商品管理',
    template:{
        path:'columns/two',
        params:{
            services:{
                save:{
                    path:'shop/market/broadcast/commodity/save',
                },
                remove:{
                    path:'shop/market/broadcast/commodity/remove',
                },
                list:{
                    path:'shop/market/broadcast/commodity/list',
                }
            },
            search:{
                selects:[
                    'state'
                    // 这里增加与原来一样的搜索 selects 配置
                ],
            },
            form:{
                fields:[{
                    class: formInput,
                    itemId:'name',
                    placeholder:'请输入商品名称',
                    widget:'text',
                    label:'商品名称',
                },{
                    class: formInput,
                    itemId:'cover_img',
                    placeholder:'请输入商品图片',
                    widget:'text',
                    label:'商品图片',
                },{
                    class: formInput,
                    itemId:'price2',
                    placeholder:'请输入直播价',
                    widget:'text',
                    label:'直播价',
                },{
                    class: formInput,
                    itemId:'price',
                    placeholder:'请输入原价',
                    widget:'text',
                    label:'原价',
                },{
                    class: formInput,
                    itemId:'cost_price',
                    placeholder:'请输入差价',
                    widget:'text',
                    label:'差价',
                },{
                    class: formInput,
                    itemId:'audit_status',
                    widget:'select',
                    label: '审核状态',
                    dictionary: 'shop/common/static/audit_status',
                },{
                    class: formInput,
                    itemId:'state',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '启用'
                    }, {
                        value: 0,
                        text: '禁用'
                    }],
                    label:'状态',
                }],
            },
            grid:{
                columns: [{
                    prop: 'name',
                    label: '商品名称',
                },{
                    prop: 'price2',
                    label: '直播价',
                },{
                    prop: 'price',
                    label: '原价',
                },{
                    prop: 'cost_price',
                    label: '差价',
                },{
                    prop: 'audit_status',
                    label: '审核状态',
                    dictionary: 'shop/common/static/audit_status',
                },'state'],
            },
            
        }
    }
} ;