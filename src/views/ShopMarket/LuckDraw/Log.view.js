import {
    center,
    grid,
    header,
    button,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '中奖记录',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search-datesearch',
                times: [{
                    label: '今日',
                    value: 'today'
                }, {
                    label: '昨日',
                    value: 'yesterday'
                },{
                    label: '近7日',
                    value: 'lately7'
                },{
                    label: '近30日',
                    value: 'lately30'
                }],
                rangeTimes: true,
                selects: [{
                    field: 'category_uuid',
                    placeholder: '请选择活动类型',
                    // dictionary: {
                    //     path: 'common/dictionary/config',
                    //     params: {
                    //         name: 'crm_contract_category',
                    //     },
                    // },
                }],
                text:{
                    field: 'keywords',
                    placeholder: '请输入用户信息'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params:'$params'
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            pageSize: 20,
            columns: [{
                prop: 'name',
                label: '用户信息',
            },{
                prop: 'name',
                label: '奖品信息',
            },{
                prop: 'name',
                label: '抽奖时间',
            },{
                prop: 'name',
                label: '收货信息',
            },{
                prop: 'name',
                label: '备注信息',
            }],
            buttons:[{
                value:'备注',
                onClick:[{
                    action:'dialog-show',
                    // path:'Contract/Edit',
                    params: '$params',
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            }],
            flex: true,
            operationWidth:55,
            operationButtonCount:1,
            enableCheckboxColumn: true,
        }]
    }]
};
