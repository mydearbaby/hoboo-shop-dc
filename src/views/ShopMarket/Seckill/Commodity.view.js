import {
    center,
    grid,
    header,
    button,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '秒杀商品',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search',
                text:{
                    field: 'keywords',
                    placeholder: '请输入秒杀名称'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params:'$params'
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick: [{
                        action:'dialog-show',
                        path:'Shop/Market/Seckill/Edit',
                        params: {
                            title:"'新增秒杀商品'"
                        },
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }],
                        cancelActions:[{
                            action:'print',
                            description:'取消对话框'
                        }]
                    }]
                },{
                    class: button,
                    widget: 'button',
                    value: '导出',
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            service: 'shop/market/seckill/commodity/list',
            pageSize: 20,
            columns: [{
                prop: 'title',
                label: '活动名称',
                minWidth: 180,
            },{
                prop: 'info',
                label: '简介',
                minWidth: 200,
            },{
                prop: 'price',
                label: '价格',
            },{
                prop: 'ot_price',
                label: '原价',
            },{
                prop: 'sales',
                label: '销量',
            },{
                prop: 'num',
                label: '限购(个)',
            },{
                prop: 'start_time',
                label: '开始时间',
            }],
            buttons:[{
                value:'编辑',
                onClick:[{
                    action:'dialog-show',
                    path:'Shop/Market/Seckill/Edit',
                    params: {
                        title:"'编辑秒杀商品'",
                        form: '$params',
                    },
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            },{
                value:'详情',
                onClick:[{
                    action:'dialog-show',
                    path:'Shop/Market/Seckill/Detail',
                    params: '$params',
                }]
            },{
                value:'删除',
                type:'danger',
                onClick:[{
                    action:'confirm',
                    title:'删除数据',
                    message:'是否删除该条数据?',
                    confirm:[{
                        action:'service',
                        path:'shop/market/seckill/commodity/remove',
                        params: {
                            id:'$params.id'
                        }
                    },{
                        action:'call',
                        target:'$widget',
                        method:'reload'
                    }]
                }]
            }],
            flex: true,
            operationWidth:130,
            operationButtonCount:3,
            // enableCheckboxColumn: true,
        }]
    }]
};
