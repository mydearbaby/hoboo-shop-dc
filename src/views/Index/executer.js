export default ({
    id = 'gridData',
    grid = 'grid',
    search = 'search',
    path
}) => {

    let gridTarget = `$widgets.${grid}`,
        searchTarget = search;

    return {
        id,
        widget:'executer',
        actions:[{
            action:'service',
            params:(() => {

                let params = {
                    page_size:`${gridTarget}.pageSize`,
                    page_no:`${gridTarget}.page`
                } ;

                if(searchTarget){

                    params['...search'] = '$widgets.search.value' ;
                }

                return params ;

            })(),
            path,
            actions:[{
                action:'set',
                target:gridTarget,
                field:'value',
                value:'$params.data.items'
            },{
                action:'set',
                target:gridTarget,
                field:'total',
                value:'$params.data.total'
            }]
        }]
    } ;

};