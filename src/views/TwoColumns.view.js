import {
    region,
    left,
    center
} from './ThreeColumns.module.scss' ;

export default {
    title:'二栏布局',
    order:109,
    items:[{
        region:'left',
        class:[
            region,
            left
        ]
    },{
        region:'center',
        class:[
            region,
            center
        ]
    }]
} ;