import {
    region,
    center
} from './ThreeColumns.module.scss' ;

export default {
    title:'一栏布局',
    class:[
        region,
        center
    ],
    order:106,
    items:[{
        region:'center'
    }]
} ;