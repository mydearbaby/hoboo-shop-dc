import {
    formInput,
    itemInfo,
    marginR,
    bg_color,
    marginL,
    marginT,
    marginB,
    data_width,
    radius,
    overflow,
    contentR
} from '@/css/VueCss.module.scss';

export default {
    title:'首页',
    onMounted:[{
        var:'data',
        action:'service',
        path: 'data/report/card/statistics'
    },{
        action:'set',
        target:'$widgets.report',
        field:'value',
        value:'$vars.data'
    },{
        var:'data',
        action:'service',
        path: 'data/report/card/number'
    },{
        action:'set',
        target:'$widgets.reportnum',
        field:'value',
        value:'$vars.data'
    },{
        var:'data',
        action:'service',
        path: 'data/chart/user'
    },{
        action:'set',
        target:'$widgets.chart',
        field:'value',
        value:'$vars.data'
    },{
        var:'data',
        action:'service',
        path: 'data/chart/user/buy'
    },{
        action:'set',
        target:'$widgets.pie',
        field:'value',
        value:'$vars.data'
    }],
    items: [{
        region:'center',
        scroll:true,
        items:[{
            id:'report',
            widget:'card-report-1'
        },{
            // class: [marginT, bg_color, itemInfo, marginR, radius, contentR],
            class: marginT,
            items:[{
            //     class: formInput,
            //     id: 'title',
            //     widget: 'public-title',
            //     title: '交易数据',
            // },{
            //     class: [marginL, marginT, marginB],
                id:'reportnum',
                widget:'card-report-2'
            }]
        },{
            class: marginT,
            layout: 'horizontal',
            items:[{
                class: [data_width, bg_color, marginR, radius],
                items:[{
                    class: [formInput, marginL, marginT],
                    id: 'title',
                    widget: 'public-title',
                    title: '实时数据',
                },{
                    height:500,
                    id:'chart',
                    widget:'user-chart'
                }]
            },{
                class: [data_width, bg_color, marginR, radius],
                items:[{
                    class: [formInput, marginL, marginT],
                    id: 'title',
                    widget: 'public-title',
                    title: '实时数据',
                },{
                    class: [marginL, marginR],
                    height:500,
                    id:'pie',
                    widget:'user-buy-chart'
                }]
            }]
        }],
    }]
} ;
 
