import {
    center,
    right,
    grid,
    input,
    button,
    search,
    header,
} from '@/css/VueCss.module.scss' ; 
export default {
    title: '商品列表',
    order: 2,
    onBeforeMount: [{
        action: 'service',
        path: 'menu/status',
        clearCache: true
    }],
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: [
                    search
                ],
                selects: [{
                    field: 'is_show',
                    placeholder: '请选择状态'
                }],
                widget: 'search',
                onMounted: [{ //初始化
                    action: 'service',
                    path: 'menu/status', //字典
                    actions: [{
                        action: 'set',
                        target: '$widget.selects[0]',
                        field: 'options',
                        value: '$params.data'
                    }]
                }],
                onSearch: [{
                    action: 'service',
                    params: {
                        page_size: '$widgets.grid.pageSize',
                        keywords: '$params.keywords',
                        is_show: '$params.is_show',
                    },
                    path: 'basicManage/commodityInfo/list/list',
                    actions: [{
                        action: 'set',
                        target: '$widgets.grid',
                        field: 'value',
                        value: '$params.data.items'
                    }, {
                        action: 'set',
                        target: '$widgets.grid',
                        field: 'total',
                        value: '$params.data.total'
                    }]
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick: [{
                        action:'dialog-show',
                        path:'BasicManage/CommodityInfo/List',
                        applyActions:[{
                            action: 'service',
                            path: 'basicManage/commodityInfo/list/list',
                            params: {
                                page_size: '$widgets.grid.pageSize',
                                page_no: '$widgets.grid.page',
                                keywords: '$widgets.search.value.keywords',
                                is_show: '$widgets.search.value.is_show',
                            },
                            actions: [{
                                action: 'set',
                                target: '$widgets.grid',
                                field: 'value',
                                value: '$params.data.items'
                            }, {
                                action: 'set',
                                target: '$widgets.grid',
                                field: 'total',
                                value: '$params.data.total'
                            }]
                        }],
                        cancelActions:[{
                            action:'print',
                            description:'取消对话框'
                        }]
                    }, {
                        action: 'form-reset',
                        target: '$widgets.form'
                    }, {
                        action: 'grid-selection-clear',
                        target: '$widgets.grid'
                    }]
                }, {
                    class: button,
                    widget: 'button',
                    value: '删除',
                    onClick:[{
                        action:'confirm',
                        title:'删除数据',
                        message:'是否删除勾选数据?',
                        confirm:[{
                            var:'checkedItemIds',
                            action:'array-value',
                            target:'$widgets.grid.checkedItems',
                            key:'id'
                        },{
                            action:'print',
                            description:'列表勾选项',
                            value:'$vars.checkedItemIds'
                        },{
                            action:'service',
                            path:'basicManage/commodityInfo/list/del',
                            params:{
                                id:'$vars.checkedItemIds'
                            }
                        },{
                            action:'message',
                            message: '删除成功',
                            type: 'success'
                        },{
                            action: 'service',
                            path: 'basicManage/commodityInfo/list/list',
                            params:{
                                page_size:'$widgets.grid.pageSize',
                                page_no:'$widgets.grid.page',
                                keywords: '$widgets.search.value.keywords',
                                is_show: '$widgets.search.value.is_show',
                            },
                            actions: [{
                                action:'set',
                                target:'$widgets.grid',
                                field:'value',
                                value:'$params.data.items'
                            },{
                                action:'set',
                                target:'$widgets.grid',
                                field:'total',
                                value:'$params.data.total'
                            }]
                        }]
                    }]
                },{
                    class: button,
                    widget: 'basic-groupbutton',
                    value: '删除',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'grid/item/detail/Drawer',
                        params: {
                            value: '`第 ${$view.count ++} 次点击`'
                        }
                    }]
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            pageSize: 20,
            columns: [{
                prop: '_id',
                label: '',
                width: 80
            }, {
                prop: 'code',
                label: '商品编码',
            }, {
                prop: 'name',
                label: '商品名称',
            }, {
                prop: 'type',
                label: '商品类型',
            }, {
                prop: 'category',
                label: '商品分类',
            }, {
                prop: 'brand',
                label: '商品品牌',
            }, {
                prop: 'supplier',
                label: '供应商',
            }, {
                prop: 'description',
                label: '商品描述',
            }, {
                prop: 'unit',
                label: '商品单位',
            }, {
                prop: 'production',
                label: '商品产地',
            }, {
                prop: 'price_purchase',
                label: '采购价',
            }, {
                prop: 'price_brade',
                label: '批发价',
            }, {
                prop: 'price_retail',
                label: '零售价',
            }, {
                prop: 'remark',
                label: '备注',
            }],
            buttons:[{
                value:'编辑',
                onClick:[{
                    action:'dialog-show',
                    path:'BasicManage/CommodityInfo/List',
                    params: '$params',
                    applyActions:[{
                        action: 'service',
                        path: 'basicManage/commodityInfo/list/list',
                        params: {
                            page_size: '$widgets.grid.pageSize',
                            page_no: '$widgets.grid.page',
                            keywords: '$widgets.search.value.keywords',
                            is_show: '$widgets.search.value.is_show',
                        },
                        actions: [{
                            action: 'set',
                            target: '$widgets.grid',
                            field: 'value',
                            value: '$params.data.items'
                        }, {
                            action: 'set',
                            target: '$widgets.grid',
                            field: 'total',
                            value: '$params.data.total'
                        }]
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            },{
                value:'删除',
                type:'danger',
                onClick:[{
                    action:'confirm',
                    title:'删除数据',
                    message:'是否删除该条数据?',
                    confirm:[{
                        action:'service',
                        path:'basicManage/commodityInfo/list/del',
                        params:'$params'
                    },{
                        action:'message',
                        message: '删除成功',
                        type: 'success'
                    },{
                        action: 'service',
                        path: 'basicManage/commodityInfo/list/list',
                        params:{
                            page_size:'$widgets.grid.pageSize',
                            page_no:'$widgets.grid.page',
                            keywords: '$widgets.search.value.keywords',
                            is_show: '$widgets.search.value.is_show',
                        },
                        actions: [{
                            action:'set',
                            target:'$widgets.grid',
                            field:'value',
                            value:'$params.data.items'
                        },{
                            action:'set',
                            target:'$widgets.grid',
                            field:'total',
                            value:'$params.data.total'
                        }]
                    }]
                }]
            }],
            flex: true,
            enableCheckboxColumn:true,
            onPage:[{
                action:'service',
                params:{
                    page_size:'$widget.pageSize',
                    page_no:'$params.page',
                    keywords:'$widgets.search.value.keywords',
                    is_show: '$widgets.search.value.is_show',
                },
                path:'basicManage/commodityInfo/list/list',
                actions:[{
                    action:'set',
                    target:'$widget',
                    field:'value',
                    value:'$params.data.items'
                },{
                    action:'set',
                    target:'$widget',
                    field:'total',
                    value:'$params.data.total'
                }]
            }],
            onMounted: [{
                action: 'service',
                params: {
                    page_size: '$widget.pageSize'
                },
                path: 'basicManage/commodityInfo/list/list',
                actions: [{
                    action: 'set',
                    target: '$widget',
                    field: 'value',
                    value: '$params.data.items'
                }, {
                    action: 'set',
                    target: '$widget',
                    field: 'total',
                    value: '$params.data.total'
                }]
            }],
            // onRowselect: [{
            //     action: 'form-value',
            //     clone:true,
            //     target: '$widgets.form',
            //     value: '$params'
            // }],
        }]
    }]
} ;