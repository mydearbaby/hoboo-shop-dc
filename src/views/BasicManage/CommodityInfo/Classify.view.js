import {
    center,
    right,
    grid,
    input,
    button,
    search,
    header,
} from '@/css/VueCss.module.scss' ;
export default {
    title: '商品分类',
    order: 1,
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: [
                    search
                ],
                widget: 'search',
                onMounted: [{ //初始化
                    action: 'service',
                    path: 'menu/status', //字典
                    actions: [{
                        action: 'set',
                        target: '$widget.selects[0]',
                        field: 'options',
                        value: '$params.data'
                    }]
                }],
                onSearch: [{
                    action: 'service',
                    params: {
                        module:'"goods_category"', 
                        page_size: '$widgets.grid.pageSize',
                        keywords: '$params.keywords',
                        is_show: '$params.is_show',
                    },
                    path: 'basicManage/commodityInfo/classify/list',
                    actions: [{
                        action: 'set',
                        target: '$widgets.grid',
                        field: 'value',
                        value: '$params.data.items'
                    }, {
                        action: 'set',
                        target: '$widgets.grid',
                        field: 'total',
                        value: '$params.data.total'
                    }]
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick: [{
                        action: 'form-reset',
                        target:'$widgets.form'
                    },{
                        action:'grid-selection-clear',
                        target:'$widgets.grid'
                    }]
                }, {
                    class: button,
                    widget: 'button',
                    value: '删除',
                    onClick:[{
                        action:'confirm',
                        title:'删除数据',
                        message:'是否删除勾选数据?',
                        confirm:[{
                            var:'checkedItemIds',
                            action:'array-value',
                            target:'$widgets.grid.checkedItems',
                            key:'id'
                        },{
                            action:'print',
                            description:'列表勾选项',
                            value:'$vars.checkedItemIds'
                        },{
                            action:'service',
                            path:'basicManage/commodityInfo/classify/del',
                            params:{
                                module:'"goods_category"', 
                                id:'$vars.checkedItemIds'
                            }
                        },{
                            action:'message',
                            message: '删除成功',
                            type: 'success'
                        },{
                            action: 'service',
                            path: 'basicManage/commodityInfo/classify/list',
                            params:{
                                module:'"goods_category"', 
                                page_size:'$widgets.grid.pageSize',
                                page_no:'$widgets.grid.page',
                                keywords: '$widgets.search.value.keywords',
                                is_show: '$widgets.search.value.is_show',
                            },
                            actions: [{
                                action:'set',
                                target:'$widgets.grid',
                                field:'value',
                                value:'$params.data.items'
                            },{
                                action:'set',
                                target:'$widgets.grid',
                                field:'total',
                                value:'$params.data.total'
                            }]
                        }]
                    }]
                }]
            }]
        },{
            id:'grid',
            class: grid,
            widget:'grid',
            pageSize:20,
            columns:[{
                prop:'_id',
                label:'',
                width:80
            },{
                prop:'value',
                label:'名称',
                width: 220
            },{
                prop:'remarks',
                label:'备注'
            }],
            buttons:[{
                value:'编辑',
                onClick:[{
                    action:'print',
                    value:'$params',
                    description:'编辑数据'
                }]
            },{
                value:'删除',
                type:'danger',
                onClick:[{
                    action:'confirm',
                    title:'删除数据',
                    message:'是否删除该条数据?',
                    confirm:[{
                        action:'service',
                        path:'basicManage/commodityInfo/classify/del',
                        params:'$params'
                    },{
                        action:'message',
                        message: '删除成功',
                        type: 'success'
                    },{
                        action: 'service',
                        path: 'basicManage/commodityInfo/classify/list',
                        params:{
                            module:'"goods_category"', 
                            page_size:'$widgets.grid.pageSize',
                            page_no:'$widgets.grid.page',
                            keywords: '$widgets.search.value.keywords',
                            is_show: '$widgets.search.value.is_show',
                        },
                        actions: [{
                            action:'set',
                            target:'$widgets.grid',
                            field:'value',
                            value:'$params.data.items'
                        },{
                            action:'set',
                            target:'$widgets.grid',
                            field:'total',
                            value:'$params.data.total'
                        }]
                    }]
                }]
            }],
            flex:true,
            enableCheckboxColumn:true,
            onPage:[{
                action:'service',
                params:{
                    module:'"goods_category"', 
                    page_size:'$widget.pageSize',
                    page_no:'$params.page',
                    keywords:'$widgets.search.value.keywords'
                },
                path:'basicManage/commodityInfo/classify/list',
                actions:[{
                    action:'set',
                    target:'$widget',
                    field:'value',
                    value:'$params.data.items'
                },{
                    action:'set',
                    target:'$widget',
                    field:'total',
                    value:'$params.data.total'
                }]
            }],
            onMounted:[{
                action:'service',
                params:{
                    module:'"goods_category"', 
                    page_size:'$widget.pageSize'
                },
                path:'basicManage/commodityInfo/classify/list',
                actions:[{
                    action:'set',
                    target:'$widget',
                    field:'value',
                    value:'$params.data.items'
                },{
                    action:'set',
                    target:'$widget',
                    field:'total',
                    value:'$params.data.total'
                }]
            }],
            onRowselect:[{
                action:'form-value',
                clone:true,
                target:'$widgets.form',
                value:'$params'
            }],
        }]
    },{
        region: 'right',
        class: right,
        items:[{
            id:'form',
            widget:'form',
            fields:[{
                class: input,
                itemId:'value',
                placeholder:'请输入名称',
                widget:'text',
                label:'名称',
            },{
                class: input,
                itemId:'remarks',
                placeholder:'请输入备注',
                widget:'text',
                label:'备注',
            }],
            buttons: [{
                value: '保存',
                onClick: [{
                    action: 'confirm',
                    title: '表单保存',
                    message: '是否保存表单?',
                    confirm: [{
                        action: 'service',
                        path: 'basicManage/commodityInfo/classify/save',
                        params: {
                            values:'$widget.value',
                            module:'"goods_category"',                        
                        }
                    }, {
                        action: 'message',
                        message: '保存成功',
                        type: 'success'
                    }, {
                        action: 'service',
                        path: 'basicManage/commodityInfo/classify/list',
                        params:{
                            module:'"goods_category"', 
                            page_size:'$widgets.grid.pageSize',
                            page_no:'$widgets.grid.page',
                            keywords: '$widgets.search.value.keywords',
                            is_show: '$widgets.search.value.is_show',
                        },
                        actions: [{
                            action:'set',
                            target:'$widgets.grid',
                            field:'value',
                            value:'$params.data.items'
                        },{
                            action:'set',
                            target:'$widgets.grid',
                            field:'total',
                            value:'$params.data.total'
                        }]
                    }]
                }]
            }],
            flex:true
        }]
    }]
} ;