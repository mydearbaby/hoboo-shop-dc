import {
    formInput
} from '@/css/VueCss.module.scss' ;
export default {
    title:'用户标签',
    template:{
        path:'columns/two',
        params:{
            services:{
                save:{
                    path: 'system/dictionary/config/save',
                    params: {
                        module: 'system_module',
                        name: 'contact_label',
                    },
                },
                remove:{
                    path: 'system/dictionary/config/remove',
                    params: {
                        module: 'system_module',
                        name: 'contact_label',
                    },
                },
                list:{
                    path: 'system/dictionary/config/list',
                    params: {
                        module: 'system_module',
                        name: 'contact_label',
                    },
                }
            },
            search:{
                selects:[
                    'state'
                    // 这里增加与原来一样的搜索 selects 配置
                ],
                extraParams:{
                    // 这里添加额外参数
                    module: '"system_module"',
                    name: '"contact_label"',
                }
            },
            form:{
                fields:[{
                    class: formInput,
                    itemId:'value',
                    placeholder:'请输入名称',
                    widget:'text',
                    label:'名称',
                },{
                    class: formInput,
                    itemId:'description',
                    placeholder:'请输入备注信息',
                    widget:'text',
                    label:'备注信息',
                },{
                    class: formInput,
                    itemId:'state',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '启用'
                    }, {
                        value: 0,
                        text: '禁用'
                    }],
                    label:'状态',
                }],
            },
            grid:{
                columns: [{
                    prop:'value',
                    label:'名称',
                },{
                    prop:'description',
                    label:'备注信息',
                },'state'],
            },
            
        }
    }
} ;