import {
    left as threecolumns_left,
    center as threecolumns_center,
    body as threecolumns_body,
    grid as threecolumns_grid,
    center_top as threecolumns_center_top,
} from '@/css/layout/ThreeColumns.module.scss' ;

import {
    button,
    search,
    itemInfo,
    girdHeader,
    center_header,
    formTitle,
    contact_header,
    formInput
} from '@/css/VueCss.module.scss' ;
export default {
    // title: '用户管理',
    layout:'three-columns',
    items:[{
        region:'left',
        class:threecolumns_left,
        layout: 'vertical',
        items: [{
            id: 'search',
            class: search,
            widget: 'search',
            text:{
                field: 'keywords',
                placeholder: '请输入用户'
            },
            onSearch: [{
                action:'grid-load',
                target:'$widgets.grid',
                params: {
                    '...':'$params',
                    keywordsBy: '"nickname"',
                }
            }]
        },{
            layout: 'horizontal',
            class: contact_header,
            items:[{
                class: formTitle,
                id: 'title',
                widget: 'public-title',
                title: '用户账户',
            },{
                class: button,
                widget: 'buttonicon',
                type: 'text',
                layout: 'horizontal',
                onClick: [{
                    action:'dialog-show',
                    path: 'Shop/User/List/Edit',
                    params: {
                        title:"'新增用户'"
                    },
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions: [{
                        action: 'print',
                        description: '取消对话框'
                    }]
                }]
            }]
        },{
            id:'grid',
            widget:'grid',
            service:'system/role/user/list',
            pageSize:20,
            columns:[{
                prop:'nickname',
            }],
            flex:true,
            header:false,
            enableIndexColumn: false,
            operationButtonCount: 0,
            operationWidth: 30,
            buttons: [{
                value: '编辑',
                onClick: [{
                    action: 'dialog-show',
                    path: 'Shop/User/List/Edit',
                    params: {
                        title:"'编辑用户信息'",
                        form: '$params',
                    },
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions: [{
                        action: 'print',
                        description: '取消对话框'
                    }]
                }]
            },{
                value: '详情',
                onClick: [{
                    action: 'dialog-show',
                    path: 'Shop/User/List/Detail',
                    params: '$params',
                }]
            },{
                value: '删除',
                type: 'danger',
                onClick: [{
                    action: 'confirm',
                    title: '删除数据',
                    message: '是否删除该条数据?',
                    confirm: [{
                        action: 'service',
                        path: 'system/role/user/remove',
                        params: {
                            id: '$params.id'
                        }
                    }, {
                        action: 'call',
                        target: '$widget',
                        method: 'reload'
                    }]
                }]
            // },{
            //     value: '重置密码',
            //     onClick: [{
            //         action: 'confirm',
            //         title: '重置密码',
            //         message: '是否确认重置密码?',
            //         confirm: [{
            //             action: 'service',
            //             path: 'shop/user/reset_password',
            //             params: '$params'
            //         },{
            //             action:'dialog-hide',
            //             mode:'apply'
            //         }]
            //     }]
            }],
            onRowselect:[{
                action:'set',
                target:'$widgets.userBasic',
                field: 'value',
                value:'$params'
            },{
                action:'grid-load',
                target:'$widgets.gridRecharge',
                service:'user/recharge_list',
                params:{
                    uid: "$params.id"
                }
            },{
                action:'grid-load',
                target:'$widgets.gridLogin',
                service:'shop/user/user_login_record',
                params:{
                    uid: "$params.id"
                }
            },{
                action:'grid-load',
                target:'$widgets.gridAction',
                service:'shop/user/user_visit',
                params:{
                    uid: "$params.id"
                }
            },{
                action:'print',
                value:'$widgets'
            }],
        }]
    },{
        region:'center',
        items:[{
            class:threecolumns_center_top,
            items:[{
                layout: 'horizontal',
                flex: true,
                class: center_header,
                items:[{
                    class: formInput,
                    id: 'title',
                    widget: 'public-title',
                    title: '用户信息',
                },{
                    class: button,
                    widget: 'button',
                    value: '编辑',
                    layout: 'horizontal',
                    onClick: [{
                        action:'dialog-show',
                        path: 'Shop/User/List/Edit',
                        params:{
                            form:'$widgets.grid.selectedItem',
                            title:'"编辑用户信息"'
                        },
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        },{
                            action: 'call',
                            target: '$widgets.userBasic',
                            method: 'setValue',
                            params: '$widgets.grid.selectedItem'
                        }],
                        cancelActions: [{
                            action: 'print',
                            description: '取消对话框'
                        }]
                    }]
                },{
                    class: button,
                    widget: 'button',
                    value: '用户充值',
                    layout: 'horizontal',
                    onClick: [{
                        action: 'print',
                        value: '$widgets.grid.selectedItem'
                    },{
                        action: 'dialog-show',
                        path: 'Shop/User/List/Recharge',
                        params: '$widgets.grid.selectedItem',
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }, {
                            action: 'call',
                            target: '$widgets.userBasic',
                            method: 'setValue',
                            params: '$widgets.grid.selectedItem'
                        }, {
                            action: 'call',
                            target: '$widgets.gridRecharge',
                            method: 'reload'
                        }],
                        cancelActions: [{
                            action: 'print',
                            description: '取消对话框'
                        }]
                    }]
                },
                // {
                //     class: button,
                //     widget: 'button',
                //     value: '用户等级',
                //     layout: 'horizontal',
                //     onClick: [{
                //         action: 'dialog-show',
                //         path: 'Shop/User/List/Level',
                //         params: '$params',
                //         applyActions: [{
                //             action: 'call',
                //             target: '$widgets.grid',
                //             method: 'reload'
                //         }],
                //         cancelActions: [{
                //             action: 'print',
                //             description: '取消对话框'
                //         }]
                //     }]
                // }
                ]
            },{
                class: itemInfo,
                id:'userBasic',
                widget:'form',
                layout: 'gridform',
                fields:[{
                    itemId:'real_name',
                    widget:'form-text',
                    label:'姓名',
                },{
                    itemId:'nickname',
                    widget:'form-text',
                    label:'昵称',
                },{
                    itemId:'phone',
                    widget:'form-text',
                    label:'手机号码',
                },{
                    itemId:'now_money',
                    widget:'form-text',
                    label:'用户余额',
                },{
                    itemId:'level',
                    widget:'select',
                    print: true,
                    dictionary: "shop/common/dictionary/system_user_level",
                    label:'用户等级',
                },{
                    itemId:'state',
                    widget:'select',
                    print: true,
                    dictionary: "common/static/state",
                    label:'状态',
                }],
                flex:true,
            }]
        },{
            flex:1,
            class:[threecolumns_center, threecolumns_body],
            layout:'tab',
            items:[{
                title:'充值记录',
                layout:'vertical',
                items:[{
                    items:[{
                        class: girdHeader,
                        items:[{
                            id: 'search',
                            class: search,
                            widget: 'search',
                            onSearch: [{
                                action:'grid-load',
                                target:'$widgets.gridRecharge',
                                params: {
                                    // customer_uuid: "$widgets.grid.selectedItem.uuid",
                                    page_size: '$widgets.gridRecharge.pageSize',
                                    keywords: '$params.keywords',
                                }
                            }]
                        }]
                    }]
                },{
                    class: threecolumns_grid,
                    id: 'gridRecharge',
                    widget: 'grid',
                    pageSize: 20,
                    columns:[{
                        prop: 'order_id',
                        label: '流水号'
                    },{
                        prop:'user.nickname',
                        label:'用户',
                        width: 120,
                    },{
                        prop:'price',
                        label:'充值金额',
                        width: 160,
                    },{
                        prop:'give_price',
                        label:'赠送金额',
                        width: 120,
                    }, {
                    //     prop: 'recharge_type',
                    //     label: '充值类型',
                    //     width: 120,
                    // }, {
                        prop: 'paid',
                        label: '充值状态',
                        dictionary: 'shop/common/dictionary/paid',
                        width: 100,
                    }, {
                        prop:'pay_time',
                        label:'充值时间',
                        width: 200,
                    }
                    // ,{
                    //     prop:'remarks',
                    //     label:'备注信息',
                    // }
                    ],
                    flex: true,
                    enableEdit:true,
                }]
            },{
                title:'登录日志',
                layout:'vertical',
                items:[{
                    items:[{
                        class: girdHeader,
                        items:[{
                            id: 'search',
                            class: search,
                            widget: 'search',
                            onSearch: [{
                                action:'grid-load',
                                target:'$widgets.gridLogin',
                                params: {
                                    // customer_uuid: "$widgets.grid.selectedItem.uuid",
                                    page_size: '$widgets.gridLogin.pageSize',
                                    keywords: '$params.keywords',
                                }
                            }]
                        }]
                    }]
                },{
                    class: threecolumns_grid,
                    id: 'gridLogin',
                    widget: 'grid',
                    pageSize: 20,
                    columns:[{
                        prop:'login_type',
                        label:'登录端',
                    },{
                        prop:'create_time',
                        label:'登录时间',
                    }],
                    flex: true,
                    enableEdit:true,
                }]
            },{
                title:'操作日志',
                layout:'vertical',
                items:[{
                    items:[{
                        class: girdHeader,
                        items:[{
                            id: 'search',
                            class: search,
                            widget: 'search',
                            onSearch: [{
                                action:'grid-load',
                                target:'$widgets.gridAction',
                                params: {
                                    // customer_uuid: "$widgets.grid.selectedItem.uuid",
                                    page_size: '$widgets.gridAction.pageSize',
                                    keywords: '$params.keywords',
                                }
                            }]
                        }]
                    }]
                },{
                    class: threecolumns_grid,
                    id: 'gridAction',
                    widget: 'grid',
                    pageSize: 20,
                    columns:[{
                        prop:'url',
                        label:'访问路径',
                    },{
                        prop:'ip',
                        label:'访问Ip',
                    },{
                        prop:'channel_type',
                        label:'访问端',
                    },{
                        prop:'create_time',
                        label:'访问时间',
                    }],
                    flex: true,
                    enableEdit:true,
                }]
            }]
        }]
    }],
} ;
