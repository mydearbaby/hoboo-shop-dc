import {
    center,
    right,
    grid,
    formInput,
    header,
    button,
    search,
    rightContent,
} from '@/css/VueCss.module.scss';
export default {
    title: '等级设置',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search',
                text:{
                    field: 'keywords',
                    placeholder: '请输入名称'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params:'$params'
                    // params: {
                    //     name: '"contact_label"',
                    //     keywords: '$params.keywords',
                    //     page_size: '$widgets.grid.pageSize',
                    // }
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick:[{
                        action:'call',
                        target:'$widgets.form',
                        method:'reset'
                    },{
                        action:'call',
                        target:'$widgets.grid',
                        method:'clearSelection'
                    }]
                }, {
                    class: button,
                    widget: 'button',
                    value: '删除',
                    onClick:[{
                        action:'confirm',
                        title:'删除数据',
                        message:'是否删除勾选数据?',
                        confirm:[{
                            var:'checkedItemIds',
                            action:'array-value',
                            target:'$widgets.grid.checkedItems',
                            key:'id'
                        },{
                            action:'print',
                            description:'列表勾选项',
                            value:'$vars.checkedItemIds'
                        },{
                            action:'service',
                            // path: 'system/dictionary/config/allremove',
                            params:{
                                // module:'"system_module"',
                                // name: '"crm_contract_category"',
                                id:'$vars.checkedItemIds'
                            }
                        },{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }]
                    }]
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            // service: {
            //     path: 'system/dictionary/config/list',
            //     params: {
            //         module: 'system_module',
            //         name: 'contact_label',
            //     },
            // },
            pageSize: 20,
            columns: [{
                prop:'state',
                label:'商城会员状态',
            },{
                prop:'value',
                label:'下单赠送会员经验比例',
            },{
                prop:'value',
                label:'签到赠送会员经验值',
            },{
                prop:'description',
                label:'邀请新用户赠送会员经验值',
            }],
            buttons:[{
                value:'编辑',
            },{
                value:'删除',
                type:'danger',
                onClick:[{
                    action:'confirm',
                    title:'删除数据',
                    message:'是否删除该条数据?',
                    confirm:[{
                        action:'service',
                        // path:'system/dictionary/config/remove',
                        params: {
                            id:'$params.id'
                        }
                    },{
                        action:'call',
                        target:'$widget',
                        method:'reload'
                    }]
                }]
            }],
            flex: true,
            enableCheckboxColumn: true,
            onRowselect:[{
                action:'call',
                target:'$widgets.form',
                method:'setValue',
                params:'$params'
            }],
        }]
    },{
        region: 'right',
        class: right,
        items:[{
            id: 'title',
            widget: 'public-titleright',
            title: '编辑修改',
        },{
            class:rightContent,
            items:[{
                id:'form',
                widget:'form',
                fields:[{
                    class: formInput,
                    itemId:'state',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '启用'
                    }, {
                        value: 0,
                        text: '禁用'
                    }],
                    label:'商城会员状态',
                },{
                    class: formInput,
                    itemId:'value',
                    placeholder:'请输入赠送比例(实际支付1元赠送多少经验)',
                    widget:'text',
                    label:'下单赠送会员经验比例',
                },{
                    class: formInput,
                    itemId:'value',
                    placeholder:'请输入签到赠送会员经验值',
                    widget:'text',
                    label:'签到赠送会员经验值',
                },{
                    class: formInput,
                    itemId:'description',
                    placeholder:'请输入邀请新用户赠送会员经验值',
                    widget:'text',
                    label:'邀请新用户赠送会员经验值',
                }],
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            action: 'service',
                            // path: 'system/dictionary/config/save',
                            params: "$widget.value",
                        },{
                            action:'call',
                            target:'$widgets.grid',
                            method:'reload'
                        }]
                    }]
                }],
                flex:true
            }]
        }]
    }]
};
