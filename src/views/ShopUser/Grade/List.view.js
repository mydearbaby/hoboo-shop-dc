import {
    center,
    grid,
    header,
    button,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '会员等级',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                class: search,
                widget: 'search',
                text:{
                    field: 'keywords',
                    placeholder: '请输入会员名称'
                },
                onSearch: [{
                    action: 'grid-load',
                    target: '$widgets.grid',
                    params: '$params'
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'Shop/User/Grade/Edit',
                        params: {
                            title:"'新增会员等级'"
                        },
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }],
                        cancelActions: [{
                            action: 'print',
                            description: '取消对话框'
                        }]
                    }]
                }, {
                    class: button,
                    widget: 'button',
                    value: '删除',
                    onClick:[{
                        action:'confirm',
                        title:'删除数据',
                        message:'是否删除勾选数据?',
                        confirm:[{
                            var:'checkedItemIds',
                            action:'array-value',
                            target:'$widgets.grid.checkedItems',
                            key:'id'
                        },{
                            action:'print',
                            description:'列表勾选项',
                            value:'$vars.checkedItemIds'
                        },{
                            action:'service',
                            path:'shop/user/grade/allremove',
                            params:{
                                id:'$vars.checkedItemIds'
                            }
                        },{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }]
                    }]
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            service: 'shop/user/grade/list',
            pageSize: 20,
            columns: [{
                prop:'name',
                label:'会员名称',
            },{
            //     prop:'icon',
            //     label:'会员图标',
            // },{
                prop:'money',
                label:'购买金额',
            },{
                prop:'valid_date',
                label:'有效时间',
            },{
                prop:'discount',
                label:'享受折扣',
            },{
                prop:'explain',
                label:'等级说明',
            },{
                prop:'is_show',
                dictionary: 'shop/common/static/whether',
                label:'是否显示',
            }],
            buttons: [{
                value: '编辑',
                onClick: [{
                    action: 'dialog-show',
                    path: 'Shop/User/Grade/Edit',
                    params: {
                        form:'$params',
                        title:"'编等级信息'"
                    },
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions: [{
                        action: 'print',
                        description: '取消对话框'
                    }]
                }]
            }, {
                value: '详情',
                onClick: [{
                    action: 'dialog-show',
                    path: 'Shop/User/Grade/Detail',
                    params: '$params',
                }]
            }, {
                value: '删除',
                type: 'danger',
                onClick: [{
                    action: 'confirm',
                    title: '删除数据',
                    message: '是否删除该条数据?',
                    confirm: [{
                        action: 'service',
                        path: 'shop/user/grade/remove',
                        params: {
                            id: '$params.id'
                        }
                    }, {
                        action: 'call',
                        target: '$widget',
                        method: 'reload'
                    }]
                }]
            }],
            flex: true,
            operationWidth:130,
            operationButtonCount:3,
            enableCheckboxColumn: true,
        }]
    }]
};
