import {
    center,
    grid,
    header,
    button,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '用户留言',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                class: search,
                widget: 'search-datesearch',
                times: [{
                    label: '今日',
                    value: 'today'
                }, {
                    label: '昨日',
                    value: 'yesterday'
                },{
                    label: '近7日',
                    value: 'lately7'
                },{
                    label: '近30日',
                    value: 'lately30'
                }],
                rangeTimes: true,
                text:{
                    field: 'keywords',
                    placeholder: '请输入用户名'
                },
                onSearch:[{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params:'$params'
                }]
            }]
        },{
            id:'grid',
            class:grid,
            widget:'grid',
            // service: 'pmstandard/file/list',
            pageSize: 20,
            columns:[{
                prop:'pm_uuid',
                label:'昵称',
            },{
                prop:'name',
                label:'电话',
            },{
                prop:'size',
                label:'内容',
            },{
                prop:'ext',
                label:'状态',
            },{
                prop:'ext',
                label:'时间',
            }],
            flex:true,
            buttons:[{
                value:'编辑',
                onClick:[{
                    action:'dialog-show',
                    // path:'Contract/Edit',
                    params: '$params',
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            },{
                value:'删除',
                type:'danger',
                onClick:[{
                    action:'confirm',
                    title:'删除数据',
                    message:'是否删除该条数据?',
                    confirm:[{
                        action:'service',
                        // path:'crm/contract/remove',
                        params: {
                            id:'$params.id'
                        }
                    },{
                        action:'call',
                        target:'$widget',
                        method:'reload'
                    }]
                }]
            }],
        }]
    }]
} ;