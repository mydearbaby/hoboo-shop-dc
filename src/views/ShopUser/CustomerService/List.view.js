import {
    center,
    grid,
    header,
    button,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '客服列表',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                class: search,
                widget: 'search-datesearch',
                times: [{
                    label: '今日',
                    value: 'today'
                }, {
                    label: '昨日',
                    value: 'yesterday'
                },{
                    label: '近7日',
                    value: 'lately7'
                },{
                    label: '近30日',
                    value: 'lately30'
                }],
                text:{
                    field: 'keywords',
                    placeholder: '请输入用户名'
                },
                onSearch:[{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params:'$params'
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick: [{
                        action:'dialog-show',
                        path:'Shop/User/CustomerService/List/Edit',
                        params: {
                            title:"'新增客服'"
                        },
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }],
                        cancelActions:[{
                            action:'print',
                            description:'取消对话框'
                        }]
                    }]
                }, {
                    class: button,
                    widget: 'button',
                    value: '删除',
                    onClick:[{
                        action:'confirm',
                        title:'删除数据',
                        message:'是否删除勾选数据?',
                        confirm:[{
                            var:'checkedItemIds',
                            action:'array-value',
                            target:'$widgets.grid.checkedItems',
                            key:'id'
                        },{
                            action:'print',
                            description:'列表勾选项',
                            value:'$vars.checkedItemIds'
                        },{
                            action:'service',
                            // path: 'crm/contract/allremove',
                            params:{
                                id:'$vars.checkedItemIds'
                            }
                        },{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }]
                    }]
                }]
            }]
        },{
            id:'grid',
            class:grid,
            widget:'grid',
            // service: 'pmstandard/file/list',
            pageSize: 20,
            columns:[{
                prop:'pm_uuid',
                label:'微信用户名称',
            },{
                prop:'name',
                label:'客服名称',
            },{
                prop:'size',
                label:'账号状态',
            },{
                prop:'ext',
                label:'添加时间',
            }],
            flex:true,
            buttons:[{
                value:'编辑',
                onClick:[{
                    action:'dialog-show',
                    // path:'Contract/Edit',
                    params: '$params',
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            },{
                value:'删除',
                type:'danger',
                onClick:[{
                    action:'confirm',
                    title:'删除数据',
                    message:'是否删除该条数据?',
                    confirm:[{
                        action:'service',
                        // path:'crm/contract/remove',
                        params: {
                            id:'$params.id'
                        }
                    },{
                        action:'call',
                        target:'$widget',
                        method:'reload'
                    }]
                }]
            }],
        }]
    }]
} ;