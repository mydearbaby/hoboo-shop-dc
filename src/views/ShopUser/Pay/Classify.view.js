import {
    formInput
} from '@/css/VueCss.module.scss' ;
export default {
    title:'会员类型',
    template:{
        path:'columns/two',
        params:{
            services:{
                save:{
                    path:'shop/user/pay/classify/save',
                },
                remove:{
                    path:'shop/user/pay/classify/remove',
                },
                list:{
                    path:'shop/user/pay/classify/list',
                }
            },
            search:{
                selects:[
                    'state'
                    // 这里增加与原来一样的搜索 selects 配置
                ],
            },
            form:{
                fields:[{
                    class: formInput,
                    itemId:'title',
                    placeholder:'请输入会员名称',
                    widget:'text',
                    label:'会员名称',
                },{
                    class: formInput,
                    itemId:'vip_day',
                    placeholder:'请输入有效期(天)',
                    widget:'text',
                    label:'有效期(天)',
                },{
                    class: formInput,
                    itemId:'price',
                    placeholder:'请输入原价',
                    widget:'text',
                    label:'原价',
                },{
                    class: formInput,
                    itemId:'pre_price',
                    placeholder:'请输入优惠价',
                    widget:'text',
                    label:'优惠价',
                },{
                    class: formInput,
                    itemId:'sort',
                    placeholder:'请输入排序',
                    widget:'text',
                    label:'排序',
                },{
                    class: formInput,
                    itemId:'state',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '启用'
                    }, {
                        value: 0,
                        text: '禁用'
                    }],
                    label:'状态',
                }],
            },
            grid:{
                columns: [{
                    prop:'title',
                    label:'会员名称',
                },{
                    prop:'vip_day',
                    label:'有效期(天)',
                },{
                    prop:'price',
                    label:'原价',
                },{
                    prop:'pre_price',
                    label:'优惠价',
                },'state'],
            },
            
        }
    }
} ;