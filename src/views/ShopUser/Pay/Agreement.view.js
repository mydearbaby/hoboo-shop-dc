import {
    center, formInput,
    grid,
} from '@/css/VueCss.module.scss';
import {input} from "@/css/DialogsCss.module.scss";

export default {
    title: '会员协议',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        onMounted: [
            {
                var: 'data',
                action: 'service',
                path: 'shop/user/pay/agreement/get_item',
                params: {
                    id:1
                },
            }, {
                action: 'set',
                target: '$widgets.userAgreement',
                field: 'value',
                value: '$vars.data'
            }
        ],
        items: [{
            id: 'userAgreement',
            class: grid,
            widget: 'form',
            fields: [{
                class: input,
                itemId: 'title',
                placeholder: '请输入协议名称',
                widget: 'text',
                label: '协议名称',
            }, {
                class: input,
                itemId: 'content',
                widget: 'richeditor',
                label: '协议内容',
                height: 600,
            },{
                widget: 'switch',
                itemId: 'state',
                name: 'state',
                value: 1,
                size: 'large',
                width: 60,
                activeValue: 1,
                inActiveValue: 0,
                inlinePrompt: true,
                activeText: '开启',
                inActiveText: '关闭',
                label: '开启状态',
            }],
            buttons: [{
                value: '保存',
                type: 'primary',
                onClick: [{
                    action: 'confirm',
                    title: '表单保存',
                    message: '是否保存表单?',
                    confirm: [{
                        var: 'value',
                        target: '$widget',
                        action: 'call',
                        method: 'getValue'
                    }, {
                        action: 'service',
                        path: 'shop/user/pay/agreement/save',
                        params: {
                            id: '$vars.value.id',
                            uuid: '$vars.value.uuid',
                            value: '$vars.value.value',
                            name: '$vars.value.name',
                            state: '$vars.value.state',
                            description: '$vars.value.description',
                        }
                    }, {
                        action: 'message',
                        message: '保存成功',
                        type: 'success'
                    }]
                }]
            }],
            flex: true,
        }]
    }]
};
