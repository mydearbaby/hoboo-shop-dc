import {
    formInput
} from '@/css/VueCss.module.scss' ;
export default {
    title:'会员列表',
    template:{
        path:'columns/two',
        params:{
            services:{
                save:{
                    path:'system/role/user/save',
                    params: {
                        is_vip: 1,
                    },
                },
                remove:{
                    path:'system/role/user/remove',
                    params: {
                        is_vip: 1,
                    },
                },
                list:{
                    path:'system/role/user/list',
                    params: {
                        is_vip: 1,
                    },
                }
            },
            search:{
                selects:[
                    'state'
                    // 这里增加与原来一样的搜索 selects 配置
                ],
                extraParams:{
                    is_vip: 1,
                }
            },
            form:{
                fields:[{
                    class: formInput,
                    itemId:'real_name',
                    placeholder:'请输入姓名',
                    widget:'text',
                    label:'姓名',
                },{
                    class: formInput,
                    itemId:'nickname',
                    placeholder:'请输入昵称',
                    widget:'text',
                    label:'昵称',
                },{
                    class: formInput,
                    itemId:'birthday',
                    placeholder:'请选择出生日期',
                    widget:'form-datepicker',
                    type: 'date',
                    label:'出生日期',
                },{
                    class: formInput,
                    itemId:'phone',
                    placeholder:'请输入手机号码',
                    widget:'text',
                    label:'手机号码',
                },{
                    class: formInput,
                    itemId:'address',
                    placeholder:'请输入家庭地址',
                    widget:'text',
                    label:'家庭地址',
                },{
                    class: formInput,
                    itemId:'state',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '启用'
                    }, {
                        value: 0,
                        text: '禁用'
                    }],
                    label:'状态',
                }],
            },
            grid:{
                columns:[{
                    prop:'real_name',
                    label:'姓名',
                },{
                    prop:'nickname',
                    label:'昵称',
                },{
                    prop:'birthday',
                    label:'出生日期',
                },{
                    prop:'phone',
                    label:'手机号码',
                },{
                    prop:'address',
                    label:'家庭地址',
                },'state'],
            },
            
        }
    }
} ;