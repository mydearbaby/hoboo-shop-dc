import {
    formInput
} from '@/css/VueCss.module.scss' ;
export default {
    title:'会员权益',
    template:{
        path:'columns/two',
        params:{
            services:{
                save:{
                    path:'shop/user/pay/interests/save',
                },
                remove:{
                    path:'shop/user/pay/interests/remove',
                },
                list:{
                    path:'shop/user/pay/interests/list',
                }
            },
            search:{
                selects:[
                    'state'
                    // 这里增加与原来一样的搜索 selects 配置
                ],
            },
            form:{
                fields:[
                    {
                        class: formInput,
                        itemId:'title',
                        placeholder:'请输入权益名称',
                        widget:'text',
                        label:'权益名称',
                    },{
                        class: formInput,
                        itemId:'show_title',
                        placeholder:'请输入展示名称',
                        widget:'text',
                        label:'展示名称',
                    },{
                        class: formInput,
                        itemId:'image',
                        placeholder:'请输入权益图标',
                        widget:'text',
                        label:'权益图标',
                    },{
                        class: formInput,
                        itemId:'explain',
                        placeholder:'请输入权益简介',
                        widget:'text',
                        label:'权益简介',
                    },{
                        class: formInput,
                        itemId:'state',
                        widget: 'form-radio',
                        value: 1,
                        options: [{
                            value: 1,
                            text: '启用'
                        }, {
                            value: 0,
                            text: '禁用'
                        }],
                        label:'状态',
                    }
                ]
            },
            grid:{
                columns:[
                    {
                        prop:'title',
                        label:'权益名称',
                    },{
                        prop:'show_title',
                        label:'展示名称',
                    },{
                        prop:'explain',
                        label:'权益简介',
                    },'state'
                ]
            },
            
        }
    }
} ;