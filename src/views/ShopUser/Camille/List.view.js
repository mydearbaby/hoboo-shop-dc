import {
    formInput
} from '@/css/VueCss.module.scss' ;
export default {
    title:'会员列表',
    template:{
        path:'columns/two',
        params:{
            services:{
                save:{
                    path:'shop/user/camille/list/save',
                },
                remove:{
                    path:'shop/user/camille/list/remove',
                },
                list:{
                    path:'shop/user/camille/list/list',
                }
            },
            search:{
                selects:[
                    'state'
                    // 这里增加与原来一样的搜索 selects 配置
                    
                ],
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params: {
                        '...':'$params',
                        keywordsBy: '"order_id"',
                    }
                }]
                
            },
            form:{
                fields:[
                    {
                        class: formInput,
                        itemId:'card_number',
                        widget:'text',
                        placeholder:'请输入卡号',
                        label:'卡号',
                    },{
                        class: formInput,
                        itemId:'use_uid',
                        placeholder:'请选择使用用户',
                        widget:'select',
                        dictionary: 'shop/common/dictionary/user',
                        label:'使用用户',
                    },{
                        class: formInput,
                        itemId:'use_time',
                        placeholder:'请选择使用时间',
                        widget:'form-datepicker',
                        type: 'date',
                        label:'使用时间',
                    },{
                        class: formInput,
                        itemId:'status',
                        widget: 'form-radio',
                        options: [{
                            value: 1,
                            text: '激活'
                        }, {
                            value: 0,
                            text: '冻结'
                        }],
                        label:'卡状态',
                    }
                ]
            },
            grid:{
                columns:[
                    {
                        prop:'card_number',
                        label:'卡号',
                    },{
                        prop:'use_uid',
                        label:'使用用户',
                        dictionary: 'shop/common/dictionary/user'
                    },{
                        prop:'use_time',
                        label:'使用时间',
                    },{
                        prop: 'status',
                        label: '卡状态',
                        dictionary: 'shop/common/static/card_state'
                    }
                ]
            },
        }
    }
} ;