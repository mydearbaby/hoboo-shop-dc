import {
    center,
    grid,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '会员记录',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                class: search,
                widget: 'search-datesearch',
                times: [{
                    label: '今日',
                    value: 'today'
                }, {
                    label: '昨日',
                    value: 'yesterday'
                },{
                    label: '近7日',
                    value: 'lately7'
                },{
                    label: '近30日',
                    value: 'lately30'
                }],
                text:{
                    field: 'keywords',
                    placeholder: '请输入用户名'
                },
                onSearch:[{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params:'$params'
                }]
            }]
        },{
            id:'grid',
            class:grid,
            widget:'grid',
            // service: 'pmstandard/file/list',
            pageSize: 20,
            columns:[{
                prop:'pm_uuid',
                label:'订单号',
            },{
                prop:'name',
                label:'用户名',
            },{
                prop:'size',
                label:'手机号码',
            },{
                prop:'ext',
                label:'会员类型',
            },{
                prop:'path',
                label:'有效期限(天)',
            },{
                prop:'create_time',
                label:'支付金额(元)',
            },{
                prop:'ext',
                label:'支付方式',
            },{
                prop:'ext',
                label:'购买时间',
            },{
                prop:'ext',
                label:'到期时间',
            }],
            flex:true,
        }]
    }]
} ;
