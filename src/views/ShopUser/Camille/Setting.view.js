import {
    center,
    right,
    grid,
    formInput,
    search,
    rightContent,
} from '@/css/VueCss.module.scss';
export default {
    title: '会员设置',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            id: 'grid',
            class: grid,
            widget: 'grid',
            // service: {
            //     path: 'system/dictionary/config/list',
            //     params: {
            //         module: 'system_module',
            //         name: 'contact_label',
            //     },
            // },
            pageSize: 20,
            columns: [{
                prop:'state',
                label:'付费会员状态',
            },{
                prop:'value',
                label:'付费会员价',
            }],
            buttons:[{
                value:'编辑',
            }],
            flex: true,
            onRowselect:[{
                action:'call',
                target:'$widgets.form',
                method:'setValue',
                params:'$params'
            }],
        }]
    },{
        region: 'right',
        class: right,
        items:[{
            id: 'title',
            widget: 'public-titleright',
            title: '编辑修改',
        },{
            class:rightContent,
            items:[{
                id:'form',
                widget:'form',
                fields:[{
                    class: formInput,
                    itemId:'state',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '启用'
                    }, {
                        value: 0,
                        text: '禁用'
                    }],
                    label:'是否开启付费会员',
                },{
                    class: formInput,
                    itemId:'state',
                    widget: 'form-radio',
                    value: 0,
                    options: [{
                        value: 1,
                        text: '启用'
                    }, {
                        value: 0,
                        text: '禁用'
                    }],
                    label:'付费会员价展示',
                }],
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            action: 'service',
                            // path: 'system/dictionary/config/save',
                            params: "$widget.value",
                        },{
                            action:'call',
                            target:'$widgets.grid',
                            method:'reload'
                        }]
                    }]
                }],
                flex:true
            }]
        }]
    }]
};
