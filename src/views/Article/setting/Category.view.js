import {
    formInput
} from '@/css/VueCss.module.scss' ;

export default {
    title: '文章分类',
    template: {
        path: 'columns/two',
        params: {
            services: {
                save: {
                    path: 'shop/market/article/classify/save',
                },
                remove: {
                    path: 'shop/market/article/classify/remove',
                },
                allremove: {
                    path: 'shop/market/article/classify/allremove',
                },
                list: {
                    path: 'shop/market/article/classify/list',
                }
            },
            search: {
                selects: [
                    'state'
                ],
                extraParams: {
                    keywordsBy: '"title"',
                }
            },
            form: {
                fields: [
                    {
                        class: formInput,
                        itemId: 'title',
                        placeholder: '请输入分类名称',
                        widget: 'text',
                        label: '分类名称',
                    }, {
                        class: formInput,
                        itemId: 'intr',
                        placeholder: '请输入分类简介',
                        widget: 'text',
                        label: '分类简介',
                    }, {
                        class: formInput,
                        itemId: 'hidden',
                        widget: 'form-radio',
                        value: 0,
                        options: [
                            {
                                value: 1,
                                text: '是'
                            }, {
                                value: 0,
                                text: '否'
                            }
                        ],
                        label: '是否隐藏',
                    }
                ],
            },
            grid: {
                columns: [
                    {
                        prop: 'title',
                        label: '分类名称',
                    }, {
                        prop: 'intr',
                        label: '分类简介',
                    }, 'state'
                ],
            },

        }
    }
};
