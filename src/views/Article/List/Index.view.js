import {
    center,
    grid,
    header,
    button,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '文章列表',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search',
                text:{
                    field: 'keywords',
                    placeholder: '请输入文章名称'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params: {
                        '...':'$params',
                        keywordsBy: '"title"',
                    }
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick: [{
                        action:'dialog-show',
                        path:'Shop/Market/Article/Edit',
                        params: {
                            title:"'新增文章'"
                        },
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }],
                        cancelActions:[{
                            action:'print',
                            description:'取消对话框'
                        }]
                    }]
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            pageSize: 20,
            service: 'shop/market/article/list/list',
            columns: [{
                prop: 'title',
                label: '文章标题',
            },{
                prop: 'author',
                label: '作者',
            },{
                prop: 'synopsis',
                label: '文章简介',
            },{
                prop: 'share_title',
                label: '分享标题',
            },{
                prop: 'share_synopsis',
                label: '分享简介',
            },{
                prop: 'visit',
                label: '浏览次数',
            }],
            buttons:[{
                value:'编辑',
                onClick:[{
                    action:'dialog-show',
                    path:'Shop/Market/Article/Edit',
                    params: {
                        title:"'编辑文章信息'",
                        form: '$params',
                    },
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            },{
                value:'删除',
                type:'danger',
                onClick:[{
                    action:'confirm',
                    title:'删除数据',
                    message:'是否删除该条数据?',
                    confirm:[{
                        action:'service',
                        path:'shop/market/article/list/remove',
                        params: {
                            id:'$params.id'
                        }
                    },{
                        action:'call',
                        target:'$widget',
                        method:'reload'
                    }]
                }]
            }],
            flex: true,
            operationWidth:100,
        }]
    }]
};
