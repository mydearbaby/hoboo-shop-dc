import {
    red,
    yellow,
    blue
} from './Vertical.module.scss' ;

export default {
    title:'垂直布局',
    order:1,
    layout:'vertical',
    items:[{
        class:red
    },{
        class:yellow,
        flex:1
    },{
        class:blue
    }]
} ;