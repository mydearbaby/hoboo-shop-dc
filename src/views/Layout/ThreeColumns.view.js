import {
    left,
    right,
    center,
    header,
    body
} from './ThreeColumns.module.scss' ;

export default {
    title:'三栏布局',
    order:3,
    items:[{
        region:'left',
        class:left
    },{
        region:'center',
        items:[{
            class:[
                center,
                header
            ]
        },{
            flex:1,
            class:[
                center,
                body
            ]
        }]
    },{
        region:'right',
        class:right
    }]
} ;