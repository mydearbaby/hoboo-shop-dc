import {
    red as vertical_ved,
    yellow as vertical_yellow,
    blue as vertical_blue
} from './Vertical.module.scss' ;

import {
    red as horizontal_red,
    yellow as horizontal_yellow,
    blue as horizontal_blue
} from './Horizontal.module.scss' ;

import {
    left as threecolumns_left,
    right as threecolumns_right,
    center as threecolumns_center,
    header as threecolumns_header,
    body as threecolumns_body
} from './ThreeColumns.module.scss' ;

export default {
    title:'选项卡布局',
    order:4,
    layout:'tab',
    items:[{
        title:'垂直布局',
        layout:'vertical',
        items:[{
            class:vertical_ved
        },{
            class:vertical_yellow,
            flex:1
        },{
            class:vertical_blue
        }],
        onMounted:[{
            action:'print',
            description:'垂直布局初始化'
        }]
    },{
        title:'水平布局',
        order:2,
        layout:'horizontal',
        items:[{
            class:horizontal_red
        },{
            class:horizontal_yellow,
            flex:1
        },{
            class:horizontal_blue
        }],
        onMounted:[{
            action:'print',
            description:'水平布局初始化'
        }]
    },{
        title:'三栏布局',
        layout:'three-columns',
        items:[{
            region:'left',
            class:threecolumns_left
        },{
            region:'center',
            items:[{
                class:[
                    threecolumns_center,
                    threecolumns_header
                ]
            },{
                flex:1,
                class:[
                    threecolumns_center,
                    threecolumns_body
                ]
            }]
        },{
            region:'right',
            class:threecolumns_right
        }],
        onMounted:[{
            action:'print',
            description:'三栏布局初始化'
        }]
    }]
} ;