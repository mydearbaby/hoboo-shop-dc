import {
    red,
    yellow,
    blue
} from './Horizontal.module.scss' ;

export default {
    title:'水平布局',
    order:2,
    layout:'horizontal',
    items:[{
        class:red
    },{
        class:yellow,
        flex:1
    },{
        class:blue
    }]
} ;