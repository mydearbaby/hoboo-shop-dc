import {
    center,
    grid,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '充值记录',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search-datesearch',
                times: [{
                    label: '今日',
                    value: 'today'
                }, {
                    label: '昨日',
                    value: 'yesterday'
                },{
                    label: '近7日',
                    value: 'lately7'
                },{
                    label: '近30日',
                    value: 'lately30'
                }],
                rangeTimes: false,
                text:{
                    field: 'keywords',
                    placeholder: '请输入订单号'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params: {
                        '...':'$params',
                        keywordsBy: '"order_id"',
                    }
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            service: 'shop/finance/recharge/list',
            pageSize: 20,
            columns: [{
                prop: 'order_id',
                label: '订单号',
            },{
                prop: 'price',
                label: '充值金额',
            },{
                prop: 'give_price',
                label: '赠送金额',
            },{
                prop: 'paid',
                label: '充值状态',
                dictionary: 'shop/common/static/paid'
            },'recharge_type',{
                prop: 'pay_time',
                label: '充值时间',
            }],
            flex: true,
            // enableCheckboxColumn: true,
        }]
    }]
};

