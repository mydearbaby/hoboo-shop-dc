import {
    center,
    grid,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '充值记录',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search-datesearch',
                times: [{
                    label: '今日',
                    value: 'today'
                }, {
                    label: '昨日',
                    value: 'yesterday'
                },{
                    label: '近7日',
                    value: 'lately7'
                },{
                    label: '近30日',
                    value: 'lately30'
                }],
                rangeTimes: false,
                text:{
                    field: 'keywords',
                    placeholder: '请输入账单标题'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params: {
                        '...':'$params',
                        keywordsBy: '"title"',
                    }
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            service: 'shop/finance/fund/list',
            pageSize: 20,
            columns: [{
                prop: 'pm',
                label: '账单类型',
                dictionary: 'shop/common/static/fund_type'
            },{
                prop: 'title',
                label: '账单标题',
            },{
                prop: 'category',
                label: '明细种类',
            },{
                prop: 'number',
                label: '明细数字',
            },{
                prop: 'balance',
                label: '剩余',
            },{
                prop: 'mark',
                label: '备注信息',
            }],
            flex: true,
            // enableCheckboxColumn: true,
        }]
    }]
};

