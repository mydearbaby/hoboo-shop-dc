import {
    center,
    grid,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '提现申请',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search-datesearch',
                times: [{
                    label: '今日',
                    value: 'today'
                }, {
                    label: '昨日',
                    value: 'yesterday'
                },{
                    label: '近7日',
                    value: 'lately7'
                },{
                    label: '近30日',
                    value: 'lately30'
                }],
                selects: [{
                    field: 'category_uuid',
                    placeholder: '请选择提现方式',
                    // dictionary: {
                    //     path: 'common/dictionary/config',
                    //     params: {
                    //         name: 'crm_contract_category',
                    //     },
                    // },
                }],
                text:{
                    field: 'keywords',
                    placeholder: '请输入姓名'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params:'$params'
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            pageSize: 20,
            columns: [{
                prop: 'name',
                label: '姓名',
            },{
                prop: 'name',
                label: '提现到账金额',
            },{
                prop: 'name',
                label: '手续费',
            },{
                prop: 'name',
                label: '提现方式',
            },{
                prop: 'name',
                label: '添加时间',
            },{
                prop: 'name',
                label: '备注信息',
            },{
                prop: 'name',
                label: '审核状态',
            }],
            buttons:[{
                value:'编辑',
                onClick:[{
                    action:'dialog-show',
                    // path:'Contract/Edit',
                    params: '$params',
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            }],
            flex: true,
            operationWidth:55,
            operationButtonCount:1,
            enableCheckboxColumn: true,
        }]
    }]
};

