import {
    center,
    grid,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '佣金记录',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search-datesearch',
                times: [{
                    label: '今日',
                    value: 'today'
                }, {
                    label: '昨日',
                    value: 'yesterday'
                },{
                    label: '近7日',
                    value: 'lately7'
                },{
                    label: '近30日',
                    value: 'lately30'
                }],
                rangeTimes: true,
                text:{
                    field: 'keywords',
                    placeholder: '请输入昵称'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params:'$params'
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            pageSize: 20,
            columns: [{
                prop: 'name',
                label: 'ID',
            },{
                prop: 'name',
                label: '用户昵称',
            },{
                prop: 'name',
                label: '姓名',
            },{
                prop: 'name',
                label: '总佣金金额',
            },{
                prop: 'name',
                label: '账户余额',
            },{
                prop: 'name',
                label: '账户佣金',
            },{
                prop: 'name',
                label: '提现到账佣金',
            },{
                prop: 'name',
                label: '时间',
            }],
            buttons:[{
                value:'详情',
                onClick:[{
                    action:'dialog-show',
                    // path:'Contract/Details',
                    params: '$params',
                }]
            }],
            flex: true,
            operationWidth:55,
            operationButtonCount:1,
        }]
    }]
};

