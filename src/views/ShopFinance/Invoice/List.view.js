import {
    center,
    grid,
    search,
} from '@/css/VueCss.module.scss';
export default {
    title: '发票列表',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search-datesearch',
                times: [{
                    label: '今日',
                    value: 'today'
                }, {
                    label: '昨日',
                    value: 'yesterday'
                },{
                    label: '近7日',
                    value: 'lately7'
                },{
                    label: '近30日',
                    value: 'lately30'
                }],
                rangeTimes: false,
                text:{
                    field: 'keywords',
                    placeholder: '请输入开票人手机号'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params: {
                        '...':'$params',
                        keywordsBy: '"drawer_phone"',
                    }
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            service: 'shop/finance/invoice/list',
            pageSize: 20,
            columns: [{
                prop: 'i_type',
                label: '发票类型',
                dictionary: 'shop/common/static/type',
                width: '80'
            },{
                prop: 'header_type',
                label: '抬头类型',
                dictionary: 'shop/common/static/header_type',
                width: '80'
            },{
                prop: 'name',
                label: '发票抬头',
            },{
                prop: 'duty_number',
                label: '税号',
            },{
                prop: 'drawer_phone',
                label: '开票人手机号',
                width: '110'
            },{
                prop: 'email',
                label: '开票人邮箱',
            },{
                prop: 'tel',
                label: '注册电话',
                width: '110'
            },{
                prop: 'address',
                label: '注册地址',
            },{
                prop: 'bank',
                label: '注册开户银行',
            },{
                prop: 'card_number',
                label: '银行卡号',
            }],
            flex: true,
            // enableCheckboxColumn: true,
        }]
    }]
};

