
import {
    center,
    grid,
    search
} from '@/css/VueCss.module.scss' ;

export default {
    title: '登录日志',
    order: 1,
    items: [{
        region: 'center',
        class: center,
        items: [{
            items: [{
                id: 'search',
                class: search,
                buttons: ["今天", "昨天", "近7天", "近30天"],
                widget: 'search-datesearch',
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params:'$params'
                }]
            }]
        },{
            id:'grid',
            class: grid,
            widget:'grid',
            service: {
                path: 'system/log/list',
                params: {
                    type: 'login',
                },
            },
            pageSize:20,
            columns:[{
                prop:'admin_name',
                label:'管理员名称',
                minWidth: 80
            },{
                prop:'method',
                label:'访问类型',
                minWidth: 60
            },{
                prop:'path',
                label:'链接',
                minWidth: 180
            },{
                prop:'page',
                label:'行为',
                minWidth: 180
            },{
                prop:'ip',
                label:'登录IP',
                minWidth: 150
            },{
                prop:'create_time',
                label:'记录时间',
            }],
            flex:true,
            onPage:[{
                action:'service',
                params:{
                    page_size:'$widget.pageSize',
                    page_no:'$params.page',
                    keywords:'$widgets.search.value.keywords'
                },
                path:'system/log/list',
                actions:[{
                    action:'set',
                    target:'$widget',
                    field:'value',
                    value:'$params.data.items'
                },{
                    action:'set',
                    target:'$widget',
                    field:'total',
                    value:'$params.data.total'
                }]
            }],
        }]
    }]
} ;
