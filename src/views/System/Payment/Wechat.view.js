import {
    center,
    button,
    search,
    input,
    title,
    margin_left
} from '@/css/VueCss.module.scss';
export default {
    // title: '微信支付',
    items: [{
        region: 'center',
        class: center,
        items:[{
            items:[{
                layout: 'horizontal',
                class: search,
                items:[{
                    class: title,
                    id: 'title',
                    widget: 'public-title',
                    title: '支付状态:',
                },{
                    widget: 'switch',
                    name: '',
                    value: true,
                    size: 'large',
                    width: 60,
                    activeValue: 1,
                    inActiveValue: 0,
                    inlinePrompt: true,
                    activeText: '开启',
                    inActiveText: '关闭',
                }]
            },{
                layout: 'horizontal',
                class: search,
                items:[{
                    class: title,
                    id: 'title',
                    widget: 'public-title',
                    title: 'Mchid:',
                },{
                    class: input,
                    widget: 'text',
                    placeholder:'受理商ID、身份标识',
                }]
            },{
                layout: 'horizontal',
                class: search,
                items:[{
                    class: title,
                    id: 'title',
                    widget: 'public-title',
                    title: 'Key:',
                },{
                    class: input,
                    widget: 'text',
                    placeholder:'商户支付密钥Key。审核通过后，在微信发送的邮件中查看。',
                }]
            },{
                layout: 'horizontal',
                class: search,
                items:[{
                    class: title,
                    id: 'title',
                    widget: 'public-title',
                    title: '微信支付证书:',
                },{
                    class: input,
                    widget: 'text',
                    placeholder:'上传',
                }]
            },{
                layout: 'horizontal',
                class: search,
                items:[{
                    class: title,
                    id: 'title',
                    widget: 'public-title',
                    title: '微信支付证书密钥:',
                },{
                    class: input,
                    widget: 'text',
                    placeholder:'上传',
                }]
            },{
                layout: 'horizontal',
                class: search,
                items:[{
                    class: title,
                    id: 'title',
                    widget: 'public-title',
                    title: '配置目录:',
                },{
                    class: input,
                    widget: 'text',
                    placeholder:'支付目录配置系统不调用提示作用',
                }]
            },{
                layout: 'horizontal',
                items: [{
                    class: [button, margin_left],
                    widget: 'button',
                    type: 'primary',
                    size: 'large',
                    value: '保存提交',
                }]
            }]
        }]
    }]
};
