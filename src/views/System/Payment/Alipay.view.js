import {
    center,
    button,
    search,
    input,
    title,
    margin_left
} from '@/css/VueCss.module.scss';
export default {
    title: '支付宝支付',
    items: [{
        region: 'center',
        class: center,
        items:[{
            items:[{
                layout: 'horizontal',
                class: search,
                items:[{
                    class: title,
                    id: 'title',
                    widget: 'public-title',
                    title: '支付状态:',
                },{
                    widget: 'switch',
                    name: '',
                    value: true,
                    size: 'large',
                    width: 60,
                    activeValue: 1,
                    inActiveValue: 0,
                    inlinePrompt: true,
                    activeText: '开启',
                    inActiveText: '关闭',
                }]
            },{
                layout: 'horizontal',
                class: search,
                items:[{
                    class: title,
                    id: 'title',
                    widget: 'public-title',
                    title: '支付应用Appid:',
                },{
                    class: input,
                    widget: 'text',
                    placeholder:'支付应用Appid',
                }]
            },{
                layout: 'horizontal',
                class: search,
                items:[{
                    class: title,
                    id: 'title',
                    widget: 'public-title',
                    title: '支付私钥:',
                },{
                    class: input,
                    widget: 'text',
                    placeholder:'支付私钥',
                }]
            },{
                layout: 'horizontal',
                class: search,
                items:[{
                    class: title,
                    id: 'title',
                    widget: 'public-title',
                    title: '支付公钥:',
                },{
                    class: input,
                    widget: 'text',
                    placeholder:'支付公钥',
                }]
            },{
                layout: 'horizontal',
                items: [{
                    class: [button, margin_left],
                    widget: 'button',
                    type: 'primary',
                    size: 'large',
                    value: '保存提交',
                }]
            }]
        }]
    }]
};
