import {
    center,
    title
} from '@/css/VueCss.module.scss';
export default {
    // title: '余额支付',
    items: [{
        region: 'center',
        class: center,
        items:[{
            layout: 'horizontal',
            items:[{
                class: title,
                id: 'title',
                widget: 'public-title',
                title: '余额支付状态:',
            },{
                widget: 'switch',
                name: '',
                value: true,
                size: 'large',
                width: 60,
                activeValue: 1,
                inActiveValue: 0,
                inlinePrompt: true,
                activeText: '开启',
                inActiveText: '关闭',
            }]
        }]
            
    }]
};
