import {
    center,
    right,
    grid,
    formInput,
    header,
    button,
    search,
    rightContent
} from '@/css/VueCss.module.scss' ;

export default {
    title: '子管理员',
    icon: "assets/images/menu/system/admin/sub.svg",
    order: 2,
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                selects: [
                    'state'
                ],
                widget: 'search',
                onSearch: [{
                    action: 'grid-load',
                    target: '$widgets.grid',
                    params: '$params',
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick: [{
                        action:'call',
                        target:'$widgets.form',
                        method:'reset'
                    }, {
                        action:'call',
                        target:'$widgets.grid',
                        method:'clearSelection'
                    }]
                }, {
                    class: button,
                    widget: 'button',
                    value: '删除',
                    onClick: [{
                        action: 'confirm',
                        title: '删除数据',
                        message: '是否删除勾选数据?',
                        confirm: [{
                            var: 'checkedItemIds',
                            action: 'array-value',
                            target: '$widgets.grid.checkedItems',
                            key: 'id'
                        }, {
                            action: 'print',
                            description: '列表勾选项',
                            value: '$vars.checkedItemIds'
                        }, {
                            action: 'service',
                            path: 'system/admin/remove',
                            params: {
                                id: '$vars.checkedItemIds'
                            }
                        }, {
                            action: 'message',
                            message: '删除成功',
                            type: 'success'
                        }, {
                            action: 'service',
                            path: 'system/admin/list',
                            params: {
                                page_size: '$widgets.grid.pageSize',
                                page_no: '$widgets.grid.page',
                                keywords: '$widgets.search.value.keywords',
                                status: '$widgets.search.value.status',
                            },
                            actions: [{
                                action: 'set',
                                target: '$widgets.grid',
                                field: 'value',
                                value: '$params.data.items'
                            }, {
                                action: 'set',
                                target: '$widgets.grid',
                                field: 'total',
                                value: '$params.data.total'
                            }]
                        }]
                    }]
                }, {
                    class: button,
                    widget: 'basic-groupbutton',
                    value: '删除',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'grid/item/detail/Drawer',
                        params: {
                            value: '`第 ${$view.count ++} 次点击`'
                        }
                    }]
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            service: {
                path: 'system/admin/list',
                params: {
                    is_sub_admin: 1,
                },
            },
            pageSize: 20,
            columns: [{
                prop: 'account',
                label: '账号',
                minWidth: 120
            }, {
                prop: 'real_name',
                label: '真实姓名',
                minWidth: 150
            }, {
                prop: 'last_ip',
                label: '最后登录IP',
                minWidth: 150
            }, {
                prop: 'last_time',
                label: '最后登录时间',
                width: 180
            }, {
                prop: 'add_time',
                label: '添加时间',
                width: 180
            }, {
                prop: 'login_count',
                label: '登录次数',
            }, 'state'],
            buttons: [{
                value: '编辑',
                onClick: [{
                    action: 'print',
                    value: '$params',
                    description: '编辑数据'
                }]
            }, {
                value: '删除',
                type: 'danger',
                onClick: [{
                    action: 'confirm',
                    title: '删除数据',
                    message: '是否删除该条数据?',
                    confirm: [{
                        action: 'service',
                        path: 'system/admin/remove',
                        params: {
                            id:'$params.id'
                        }
                    },{
                        action:'call',
                        target:'$widget',
                        method:'reload'
                    }]
                }]
            }, {
                value: '重置密码',
                type: 'danger',
                onClick: [{
                    action: 'dialog-show',
                    path: 'System/Department/AdminPopup',
                    params: '$params',
                    applyActions: [{
                    //     action: 'call',
                    //     target: '$widgets.form',
                    //     method: 'setValue',
                    //     params: '$params'
                    // }, {
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions: [{
                        action: 'print',
                        description: '取消对话框'
                    }]
                }]
            }],
            flex: true,
            enableCheckboxColumn: true,
            onPage: [{
                action: 'service',
                params: {
                    page_size: '$widget.pageSize',
                    page_no: '$params.page',
                    keywords: '$widgets.search.value.keywords'
                },
                path: 'system/admin/list',
                actions: [{
                    action: 'set',
                    target: '$widget',
                    field: 'value',
                    value: '$params.data.items'
                }, {
                    action: 'set',
                    target: '$widget',
                    field: 'total',
                    value: '$params.data.total'
                }]
            }],
            onRowselect: [{
                action:'call',
                target:'$widgets.form',
                method:'setValue',
                params:'$params'
            }],
        }]
    }, {
        region: 'right',
        class: right,
        items: [{
            id: 'title',
            widget: 'public-titleright',
            title: '编辑修改',
        }, {
            class: rightContent,
            items: [{
                id: 'form',
                widget: 'form',
                fields: [
                    {
                        itemId: 'is_sub_admin',
                        value: 1,
                    }, {
                        class: formInput,
                        itemId: 'account',
                        placeholder: '请输入账号',
                        widget: 'text',
                        label: '账号',
                    }, {
                        class: formInput,
                        itemId: 'real_name',
                        placeholder: '请输入真实姓名',
                        widget: 'text',
                        label: '真实姓名',
                    }, {
                        class: formInput,
                        itemId: 'state',
                        widget: 'select',
                        label: '管理员状态',
                        placeholder: '请选择管理员状态',
                        dictionary:'common/static/state',
                    }],
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            var:'value',
                            target:'$widget',
                            action:'call',
                            method:'getValue'
                        },{
                            action: 'service',
                            path: 'system/admin/save',
                            params:"$vars.value"
                        }, {
                            action:'call',
                            target:'$widgets.grid',
                            method:'reload'
                        }]
                    }]
                }],
                flex: true
            }]
        }]
    }]
};
