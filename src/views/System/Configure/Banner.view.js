import {
    center,
    right,
    grid,
    header,
    button,
    search,
    rightContent,
    formInput
} from '@/css/VueCss.module.scss' ;
export default {
    title: '职位管理',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick: [{
                        action:'dialog-show',
                        path:'System/Banner/Edit',
                        params: {
                            title:"'新增轮播图'"
                        },
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.table',
                            method: 'reload'
                        }],
                        cancelActions:[{
                            action:'print',
                            description:'取消对话框'
                        }]
                    }]
                }]
            }]
        },{
            id:'table',
            class: grid,
            widget:'table',
            border:true,
            services: {
                read: 'system/banner/list',
                remove: 'system/banner/remove'
            },
            pageSize:20,
            columns:[{
                prop:'title',
                label:'图片标题',
                width: 220
            },{
                prop:'img',
                label:'图片',
                type:'image',
                width: 100
            },{
                prop:'url',
                label:'链接地址'
            },{
                prop:'sort',
                label:'排序'
            },{
                label:'状态',
                prop:'state',
                type:'switch',
                active:{
                    text:'启用',
                    value:1
                },
                inactive:{
                    text:'禁用',
                    value:0
                }
            },{
                label:'操作',
                type:'operation',
                width:100,
                buttons:[{
                    value:'编辑',
                    onClick:[{
                        action:'dialog-show',
                        path:'System/Banner/Edit',
                        params: {
                            form:'$params',
                            title:"'编辑轮播图信息'"
                        },
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.table',
                            method: 'reload'
                        }],
                        cancelActions:[{
                            action:'print',
                            description:'取消对话框'
                        }]
                    }]
                },{
                    value:'删除',
                    type:'delete'
                }]
            }],
            flex: true,
        }]
    }]
} ;
