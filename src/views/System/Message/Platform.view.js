import {
    center,
    grid,
    header,
    button,
} from '@/css/VueCss.module.scss';
export default {
    title: '通知平台',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick: [{
                        action:'dialog-show',
                        // path:'Contract/Edit',
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }],
                        cancelActions:[{
                            action:'print',
                            description:'取消对话框'
                        }]
                    }]
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            pageSize: 20,
            columns: [{
                prop: 'name',
                label: '通知类型',
            },{
                prop: 'name',
                label: '通知场景说明',
            },{
                prop: 'name',
                label: '站内信',
            },{
                prop: 'name',
                label: '公众号模板',
            },{
                prop: 'name',
                label: '小程序订阅',
            },{
                prop: 'name',
                label: '发送短信',
            },{
                prop: 'name',
                label: '企业微信',
            }],
            buttons:[{
                value:'设置',
                onClick:[{
                    action:'dialog-show',
                    // path:'Contract/Edit',
                    params: '$params',
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            }],
            flex: true,
            operationWidth:55,
            operationButtonCount:1,
        }]
    }]
};
