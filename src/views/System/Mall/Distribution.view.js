import {
    center,
    right,
    grid,
    header,
    button,
    search,
    rightContent,
    formInput,
    background,
    center_left
} from '@/css/VueCss.module.scss' ;
export default {
    title: '配送设置',
    items: [{
        region: 'center',
        items:[{
            class:right,
            flex:1,
            layout:'tab',
            items:[{
                title:'运费模版',
                // class:right,
                layout:'three-columns',
                items:[{
                    region: 'center',
                    class: center_left,
                    items: [{
                        items: [{
                            id: 'search',
                            class: search,
                            widget: 'search',
                            text:{
                                field: 'keywords',
                                placeholder: '请输入模版名称'
                            },
                            onSearch: [{
                                action:'grid-load',
                                target:'$widgets.gridTemplate',
                                params: {
                                    keywords: '$params.keywords',
                                }
                            }]
                        }, {
                            class: header,
                            layout: 'horizontal',
                            items: [{
                                class: button,
                                widget: 'button',
                                value: '新增',
                                onClick:[{
                                    action:'call',
                                    target:'$widgets.formTemplate',
                                    method:'reset'
                                },{
                                    action:'call',
                                    target:'$widgets.gridTemplate',
                                    method:'clearSelection'
                                }]
                            }]
                        }]
                    },{
                        id:'gridTemplate',
                        class: grid,
                        widget:'grid',
                        // service: {
                        //     path: 'system/dictionary/config/list',
                        //     params: {
                        //         name: 'pm_standard_label',
                        //         module: 'connect',
                        //     },
                        // },
                        pageSize:20,
                        columns:[{
                            prop:'value',
                            label:'模版名称',
                        },{
                            prop:'description',
                            label:'计费方式'
                        },{
                            prop:'description',
                            label:'指定包邮'
                        },{
                            prop:'description',
                            label:'计费方式'
                        },{
                            prop:'description',
                            label:'计费方式'
                        }],
                        buttons:[{
                            value:'添加配送区域',
                            onClick:[{
                                action:'dialog-show',
                                // path:'Contract/Edit',
                                params: '$params',
                                applyActions: [{
                                    action: 'call',
                                    target: '$widgets.grid',
                                    method: 'reload'
                                }],
                                cancelActions:[{
                                    action:'print',
                                    description:'取消对话框'
                                }]
                            }]
                        },{
                            value:'编辑',
                        },{
                            value:'删除',
                            type:'danger',
                            onClick:[{
                                action:'confirm',
                                title:'删除数据',
                                message:'是否删除该条数据?',
                                confirm:[{
                                    action:'service',
                                    // path:'system/dictionary/config/remove',
                                    params: {
                                        id:'$params.id'
                                    }
                                },{
                                    action:'call',
                                    target:'$widget',
                                    method:'reload'
                                }]
                            }]
                        }],
                        flex:true,
                        enableCheckboxColumn:true,
                        onRowselect:[{
                            action:'call',
                            target:'$widgets.formTemplate',
                            method:'setValue',
                            params:'$params'
                        }],
                    }]
                },{
                    region: 'right',
                    class: background,
                    items:[{
                        id: 'title',
                        widget: 'public-titleright',
                        title: '编辑修改',
                    },{
                        class:rightContent,
                        items:[{
                            id:'formTemplate',
                            widget:'form',
                            fields:[{
                                layout: 'vertical',
                                class: formInput,
                                itemId:'value',
                                placeholder:'请输入模版名称',
                                widget:'text',
                                label:'模版名称',
                            },{
                                class: formInput,
                                itemId:'description',
                                placeholder:'请选择计费方式',
                                widget:'select',
                                label:'计费方式',
                            },{
                                class: formInput,
                                itemId:'state',
                                widget: 'form-radio',
                                value: 0,
                                options: [{
                                    value: 1,
                                    text: '启用'
                                }, {
                                    value: 0,
                                    text: '禁用'
                                }],
                                label:'指定包邮',
                            },{
                                class: formInput,
                                itemId:'state',
                                widget: 'form-radio',
                                value: 0,
                                options: [{
                                    value: 1,
                                    text: '启用'
                                }, {
                                    value: 0,
                                    text: '禁用'
                                }],
                                label:'指定不送达',
                            }],
                            buttons: [{
                                value: '保存',
                                type: 'primary',
                                onClick: [{
                                    action: 'confirm',
                                    title: '表单保存',
                                    message: '是否保存表单?',
                                    confirm: [{
                                        action: 'service',
                                        // path: 'system/dictionary/config/save',
                                        params: "$widget.value",
                                    },{
                                        action:'call',
                                        target:'$widgets.gridTemplate',
                                        method:'reload'
                                    }]
                                }]
                            }],
                            flex:true
                        }]
                    }]
                }]
            },{
                title:'物流公司',
                class:right,
                layout:'three-columns',
                items:[{
                    region: 'center',
                    class: center_left,
                    items: [{
                        items: [{
                            id: 'search',
                            class: search,
                            widget: 'search',
                            text:{
                                field: 'keywords',
                                placeholder: '请输入物流公司'
                            },
                            onSearch: [{
                                action:'grid-load',
                                target:'$widgets.gridExpress',
                                params: {
                                    keywords: '$params.keywords',
                                }
                            }]
                        }, {
                            class: header,
                            layout: 'horizontal',
                            items: [{
                                class: button,
                                widget: 'button',
                                value: '新增',
                                onClick:[{
                                    action:'call',
                                    target:'$widgets.formExpress',
                                    method:'reset'
                                },{
                                    action:'call',
                                    target:'$widgets.gridExpress',
                                    method:'clearSelection'
                                }]
                            }]
                        }]
                    },{
                        id:'gridExpress',
                        class: grid,
                        widget:'grid',
                        // service: {
                        //     path: 'system/dictionary/config/list',
                        //     params: {
                        //         name: 'pm_standard_statue',
                        //         module: 'connect',
                        //     },
                        // },
                        pageSize:20,
                        columns:[{
                            prop:'value',
                            label:'物流公司',
                        },{
                            prop:'description',
                            label:'编码'
                        },{
                            prop:'description',
                            label:'状态'
                        }],
                        buttons:[{
                            value:'编辑',
                        },{
                            value:'删除',
                            type:'danger',
                            onClick:[{
                                action:'confirm',
                                title:'删除数据',
                                message:'是否删除该条数据?',
                                confirm:[{
                                    action:'service',
                                    path:'system/dictionary/config/remove',
                                    params: {
                                        id:'$params.id'
                                    }
                                },{
                                    action:'call',
                                    target:'$widget',
                                    method:'reload'
                                }]
                            }]
                        }],
                        flex:true,
                        enableCheckboxColumn:true,
                        onRowselect:[{
                            action:'call',
                            target:'$widgets.formExpress',
                            method:'setValue',
                            params:'$params'
                        }],
                    }]
                },{
                    region: 'right',
                    class: background,
                    items:[{
                        id: 'title',
                        widget: 'public-titleright',
                        title: '编辑修改',
                    },{
                        class:rightContent,
                        items:[{
                            id:'formExpress',
                            widget:'form',
                            fields:[{
                                layout: 'vertical',
                                class: formInput,
                                itemId:'value',
                                placeholder:'请输入物流公司',
                                widget:'text',
                                label:'物流公司',
                            },{
                                class: formInput,
                                itemId:'state',
                                widget: 'form-radio',
                                value: 0,
                                options: [{
                                    value: 1,
                                    text: '启用'
                                }, {
                                    value: 0,
                                    text: '禁用'
                                }],
                                label:'状态',
                            }],
                            buttons: [{
                                value: '保存',
                                type: 'primary',
                                onClick: [{
                                    action: 'confirm',
                                    title: '表单保存',
                                    message: '是否保存表单?',
                                    confirm: [{
                                        action: 'service',
                                        path: 'system/dictionary/config/save',
                                        params: "$widget.value",
                                    },{
                                        action:'call',
                                        target:'$widgets.gridExpress',
                                        method:'reload'
                                    }]
                                }]
                            }],
                            flex:true
                        }]
                    }]
                }]
            },{
                title:'城市数据',
                class:right,
                layout:'three-columns',
                items:[{
                    region: 'center',
                    class: center_left,
                    items: [{
                        items: [{
                            class: header,
                            layout: 'horizontal',
                            items: [{
                                class: button,
                                widget: 'button',
                                value: '新增',
                                onClick:[{
                                    action:'call',
                                    target:'$widgets.formExpress',
                                    method:'reset'
                                },{
                                    action:'call',
                                    target:'$widgets.gridStatue',
                                    method:'clearSelection'
                                }]
                            }]
                        }]
                    },{
                        id:'gridStatue',
                        class: grid,
                        widget:'grid',
                        // service: {
                        //     path: 'system/dictionary/config/list',
                        //     params: {
                        //         name: 'pm_standard_statue',
                        //         module: 'connect',
                        //     },
                        // },
                        pageSize:20,
                        columns:[{
                            prop:'value',
                            label:'名称',
                            width: 220
                        },{
                            prop:'description',
                            label:'备注'
                        }],
                        buttons:[{
                            value:'编辑',
                        },{
                            value:'删除',
                            type:'danger',
                            onClick:[{
                                action:'confirm',
                                title:'删除数据',
                                message:'是否删除该条数据?',
                                confirm:[{
                                    action:'service',
                                    // path:'system/dictionary/config/remove',
                                    params: {
                                        id:'$params.id'
                                    }
                                },{
                                    action:'call',
                                    target:'$widget',
                                    method:'reload'
                                }]
                            }]
                        }],
                        flex:true,
                        enableCheckboxColumn:true,
                        onRowselect:[{
                            action:'call',
                            target:'$widgets.formExpress',
                            method:'setValue',
                            params:'$params'
                        }],
                    }]
                },{
                    region: 'right',
                    class: background,
                    items:[{
                        id: 'title',
                        widget: 'public-titleright',
                        title: '编辑修改',
                    },{
                        class:rightContent,
                        items:[{
                            id:'formExpress',
                            widget:'form',
                            fields:[{
                                itemId:'name',
                                value: 'pm_standard_statue',
                            },{
                                itemId:'module',
                                value: 'connect',
                            },{
                                layout: 'vertical',
                                class: formInput,
                                itemId:'value',
                                placeholder:'请输入名称',
                                widget:'text',
                                label:'名称',
                            },{
                                class: formInput,
                                itemId:'description',
                                placeholder:'请输入备注',
                                widget:'text',
                                label:'备注',
                            }],
                            buttons: [{
                                value: '保存',
                                type: 'primary',
                                onClick: [{
                                    action: 'confirm',
                                    title: '表单保存',
                                    message: '是否保存表单?',
                                    confirm: [{
                                        action: 'service',
                                        path: 'system/dictionary/config/save',
                                        params: "$widget.value",
                                    },{
                                        action:'call',
                                        target:'$widgets.gridStatue',
                                        method:'reload'
                                    }]
                                }]
                            }],
                            flex:true
                        }]
                    }]
                }]
            },{
                title:'配送员管理',
                class:right,
                layout:'three-columns',
                items:[{
                    region: 'center',
                    class: center_left,
                    items: [{
                        items: [{
                            id: 'search',
                            class: search,
                            widget: 'search',
                            text:{
                                field: 'keywords',
                                placeholder: '请输入名称'
                            },
                            onSearch: [{
                                action:'grid-load',
                                target:'$widgets.gridStatue',
                                params: {
                                    keywords: '$params.keywords',
                                }
                            }]
                        }, {
                            class: header,
                            layout: 'horizontal',
                            items: [{
                                class: button,
                                widget: 'button',
                                value: '新增',
                                onClick:[{
                                    action:'call',
                                    target:'$widgets.formExpress',
                                    method:'reset'
                                },{
                                    action:'call',
                                    target:'$widgets.gridStatue',
                                    method:'clearSelection'
                                }]
                            }]
                        }]
                    },{
                        id:'gridStatue',
                        class: grid,
                        widget:'grid',
                        // service: {
                        //     path: 'system/dictionary/config/list',
                        //     params: {
                        //         name: 'pm_standard_statue',
                        //         module: 'connect',
                        //     },
                        // },
                        pageSize:20,
                        columns:[{
                            prop:'value',
                            label:'姓名',
                        },{
                            prop:'description',
                            label:'手机号码'
                        },{
                            prop:'description',
                            label:'状态'
                        }],
                        buttons:[{
                            value:'编辑',
                        },{
                            value:'删除',
                            type:'danger',
                            onClick:[{
                                action:'confirm',
                                title:'删除数据',
                                message:'是否删除该条数据?',
                                confirm:[{
                                    action:'service',
                                    path:'system/dictionary/config/remove',
                                    params: {
                                        id:'$params.id'
                                    }
                                },{
                                    action:'call',
                                    target:'$widget',
                                    method:'reload'
                                }]
                            }]
                        }],
                        flex:true,
                        enableCheckboxColumn:true,
                        onRowselect:[{
                            action:'call',
                            target:'$widgets.formExpress',
                            method:'setValue',
                            params:'$params'
                        }],
                    }]
                },{
                    region: 'right',
                    class: background,
                    items:[{
                        id: 'title',
                        widget: 'public-titleright',
                        title: '编辑修改',
                    },{
                        class:rightContent,
                        items:[{
                            id:'formExpress',
                            widget:'form',
                            fields:[{
                                layout: 'vertical',
                                class: formInput,
                                itemId:'value',
                                placeholder:'请输入姓名',
                                widget:'text',
                                label:'姓名',
                            },{
                                class: formInput,
                                itemId:'description',
                                placeholder:'请输入手机号码',
                                widget:'text',
                                label:'手机号码',
                            },{
                                class: formInput,
                                itemId:'state',
                                widget: 'form-radio',
                                value: 0,
                                options: [{
                                    value: 1,
                                    text: '启用'
                                }, {
                                    value: 0,
                                    text: '禁用'
                                }],
                                label:'状态',
                            }],
                            buttons: [{
                                value: '保存',
                                type: 'primary',
                                onClick: [{
                                    action: 'confirm',
                                    title: '表单保存',
                                    message: '是否保存表单?',
                                    confirm: [{
                                        action: 'service',
                                        // path: 'system/dictionary/config/save',
                                        params: "$widget.value",
                                    },{
                                        action:'call',
                                        target:'$widgets.gridStatue',
                                        method:'reload'
                                    }]
                                }]
                            }],
                            flex:true
                        }]
                    }]
                }]
            },{
                title:'发货设置',
                class:right,
                layout:'three-columns',
                
            }]
        }]
    }]
} ;