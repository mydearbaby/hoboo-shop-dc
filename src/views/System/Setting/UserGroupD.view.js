import {
    center,
    right,
    grid,
    input,
    button,
    search,
    header,
} from '@/css/VueCss.module.scss' ;

export default {
    title: '用户组管理-Dialogs',
    order: 2,
    // 加载关联项
    onBeforeMount: [{
        action: 'service',
        path: 'menu/status',
        clearCache: true
    }],
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: [
                    search
                ],
                selects: [{
                    field: 'is_show',
                    placeholder: '请选择状态'
                }],
                widget: 'search',
                onMounted: [{ //初始化
                    action: 'service',
                    path: 'menu/status', //字典
                    actions: [{
                        action: 'set',
                        target: '$widget.selects[0]',
                        field: 'options',
                        value: '$params.data'
                    }]
                }],
                onSearch: [{
                    action: 'service',
                    params: {
                        page_size: '$widgets.grid.pageSize',
                        keywords: '$params.keywords',
                        is_show: '$params.is_show',
                    },
                    path: 'system/setting/user_group/list',
                    actions: [{
                        action: 'set',
                        target: '$widgets.grid',
                        field: 'value',
                        value: '$params.data.items'
                    }, {
                        action: 'set',
                        target: '$widgets.grid',
                        field: 'total',
                        value: '$params.data.total'
                    }]
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick: [{
                        action:'dialog-show',
                        path:'System/SettingManage/UserGroupManageDrawer',
                        applyActions:[{
                            action: 'service',
                            path: 'system/setting/user_group/list',
                            params: {
                                page_size: '$widgets.grid.pageSize',
                                page_no: '$widgets.grid.page',
                                keywords: '$widgets.search.value.keywords',
                                is_show: '$widgets.search.value.is_show',
                            },
                            actions: [{
                                action: 'set',
                                target: '$widgets.grid',
                                field: 'value',
                                value: '$params.data.items'
                            }, {
                                action: 'set',
                                target: '$widgets.grid',
                                field: 'total',
                                value: '$params.data.total'
                            }]
                        }],
                        cancelActions:[{
                            action:'print',
                            description:'取消对话框'
                        }]
                    }, {
                        action: 'form-reset',
                        target: '$widgets.form'
                    }, {
                        action: 'grid-selection-clear',
                        target: '$widgets.grid'
                    }]

                }, {
                    class: button,
                    widget: 'button',
                    value: '删除',
                    // onClick: [{
                    //     action: 'dialog-show',
                    //     path: 'grid/item/detail/Drawer',
                    //     params: {
                    //         value: '`第 ${$view.count ++} 次点击`'
                    //     }
                    // }]
                    onClick:[{
                        action:'confirm',
                        title:'删除数据',
                        message:'是否删除勾选数据?',
                        confirm:[{
                            var:'checkedItemIds',
                            action:'array-value',
                            target:'$widgets.grid.checkedItems',
                            key:'id'
                        },{
                            action:'print',
                            description:'列表勾选项',
                            value:'$vars.checkedItemIds'
                        },{
                            action:'service',
                            path:'system/setting/user_group/del',
                            params:{
                               // id:'$widgets.grid.checkedSelectedIds'
                                id:'$vars.checkedItemIds'
                            }
                        },{
                            action:'message',
                            message: '删除成功',
                            type: 'success'
                        },{
                            action: 'service',
                            path: 'system/setting/user_group/list',
                            params:{
                                page_size:'$widgets.grid.pageSize',
                                page_no:'$widgets.grid.page',
                                keywords: '$widgets.search.value.keywords',
                                is_show: '$widgets.search.value.is_show',
                            },
                            actions: [{
                                action:'set',
                                target:'$widgets.grid',
                                field:'value',
                                value:'$params.data.items'
                            },{
                                action:'set',
                                target:'$widgets.grid',
                                field:'total',
                                value:'$params.data.total'
                            }]
                        }]
                    }]
                }, {
                    class: button,
                    widget: 'basic-groupbutton',
                    value: '删除',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'grid/item/detail/Drawer',
                        params: {
                            value: '`第 ${$view.count ++} 次点击`'
                        }
                    }]
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            service: 'system/usergroup/list',
            pageSize: 10,
            columns: [ {
                prop: 'group_name',
                label: '用户组',
            }, {
                prop: 'mark',
                label: '用户备注',
            }, {
                prop: 'status',
                label: '状态',
                width: 80
            }],
            onPage:[{
                action: 'service',
                path: 'system/setting/user_group/list',
                params: {
                    page_size: '$widget.pageSize',
                    page_no: '$widget.page',
                    keywords: '$widgets.search.value.keywords',
                    is_show: '$widgets.search.value.is_show',
                },
                actions: [{
                    action: 'set',
                    target: '$widget',
                    field: 'value',
                    value: '$params.data.items'
                }, {
                    action: 'set',
                    target: '$widget',
                    field: 'total',
                    value: '$params.data.total'
                }]
            }],
            buttons: [{
                value: '编辑',
                onClick:[{
                    action:'dialog-show',
                    path:'System/SettingManage/UserGroupManageDrawer',
                    params: '$params',
                    applyActions:[{
                        action: 'service',
                        path: 'system/setting/user_group/list',
                        params: {
                            page_size: '$widgets.grid.pageSize',
                            page_no: '$widgets.grid.page',
                            keywords: '$widgets.search.value.keywords',
                            is_show: '$widgets.search.value.is_show',
                        },
                        actions: [{
                            action: 'set',
                            target: '$widgets.grid',
                            field: 'value',
                            value: '$params.data.items'
                        }, {
                            action: 'set',
                            target: '$widgets.grid',
                            field: 'total',
                            value: '$params.data.total'
                        }]
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            }, {
                widget: 'button',
                value: '详情',
                class: button,
                onClick:[{
                    action:'dialog-show',
                    path:'System/SettingManage/UserGroupManageDrawer',
                    value: '$params',
                    applyActions:[{
                        action:'print',
                        description:'确定对话框',
                        value:'$params'
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            }, {
                value: '删除',
                type: 'danger',
                onClick: [{
                    action: 'confirm',
                    title: '删除数据',
                    message: '是否删除该条数据?',
                    confirm: [{
                        action: 'service',
                        path: 'system/setting/user_group/del',
                        params: '$params'
                    }, {
                        action: 'message',
                        message: '删除成功',
                        type: 'success'
                    }, {
                        action: 'service',
                        path: 'system/setting/user_group/list',
                        params: {
                            page_size: '$widgets.grid.pageSize',
                            page_no: '$widgets.grid.page',
                            keywords: '$widgets.search.value.keywords',
                            is_show: '$widgets.search.value.is_show',
                        },
                        actions: [{
                            action: 'set',
                            target: '$widgets.grid',
                            field: 'value',
                            value: '$params.data.items'
                        }, {
                            action: 'set',
                            target: '$widgets.grid',
                            field: 'total',
                            value: '$params.data.total'
                        }]
                    }]
                }]
            }],
            flex: true,
            enableCheckboxColumn:true,
            enableIndex:true,
            // onMounted: [{
            //     action: 'service',
            //     params: {
            //         page_size: '$widget.pageSize'
            //     },
            //     path: 'system/setting/user_group/list',
            //     actions: [{
            //         action: 'set',
            //         target: '$widget',
            //         field: 'value',
            //         value: '$params.data.items'
            //     }, {
            //         action: 'set',
            //         target: '$widget',
            //         field: 'total',
            //         value: '$params.data.total'
            //     }]
            // }]
        }]
    }]
};
