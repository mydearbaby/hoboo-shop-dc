import {
    center,
    right,
    search,
    grid,
    input,
    button,
    title,
    red,
    header,
} from '@/css/VueCss.module.scss' ;

export default {
    title: '菜单管理-Dialogs',
    order: 5,
    onBeforeMount: [{
        action: 'service',
        path: 'menu/status',
        clearCache: true
    }, {
        action: 'service',
        path: 'menu/attribute',
        clearCache: true
    }],
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
                id: 'search',
                class: [
                    search
                ],
                selects: [{
                    field: 'is_show',
                    placeholder: '请选择状态'
                }, {
                    field: 'header',
                    placeholder: '顶部菜单'
                }],
                widget: 'search',
                onMounted: [{ //初始化
                        action: 'service',
                        path: 'menu/status', //字典
                        actions: [{
                            action: 'set',
                            target: '$widget.selects[0]',
                            field: 'options',
                            value: '$params.data'
                        }]
                    },
                    { //初始化
                        action: 'service',
                        path: 'menu/attribute', //字典
                        actions: [{
                            action: 'set',
                            target: '$widget.selects[1]',
                            field: 'options',
                            value: '$params.data'
                        }]
                    }
                ],
                onSearch: [{
                    action: 'service',
                    params: {
                        page_size: '$widgets.grid.pageSize',
                        keywords: '$params.keywords',
                        is_show: '$params.is_show',
                        header: '$params.header',
                    },
                    path: 'system/setting/menu/list',
                    actions: [{
                        action: 'set',
                        target: '$widgets.tree',
                        field: 'value',
                        value: '$params.data.items'
                    }, {
                        action: 'set',
                        target: '$widgets.grid',
                        field: 'total',
                        value: '$params.data.total'
                    }]
                }]
                // }]

            },
            {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增菜单',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'grid/item/detail/Drawer',
                        params: {
                            value: '`第 ${$view.count ++} 次点击`'
                        }
                    }]
                }, {
                    class: button,
                    widget: 'button',
                    value: '展开',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'grid/item/detail/Drawer',
                        params: {
                            value: '`第 ${$view.count ++} 次点击`'
                        }
                    }]
                }, {
                    class: button,
                    widget: 'button',
                    value: '刷新',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'grid/item/detail/Drawer',
                        params: {
                            value: '`第 ${$view.count ++} 次点击`'
                        }
                    }]
                }, {
                    class: button,
                    widget: 'button',
                    value: '收起',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'grid/item/detail/Drawer',
                        params: {
                            value: '`第 ${$view.count ++} 次点击`'
                        }
                    }]
                }, {
                    class: button,
                    widget: 'button',
                    value: '删除',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'grid/item/detail/Drawer',
                        params: {
                            value: '`第 ${$view.count ++} 次点击`'
                        }
                    }]
                },{
                    class: button,
                    widget: 'basic-groupbutton',
                    value: '删除',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'grid/item/detail/Drawer',
                        params: {
                            value: '`第 ${$view.count ++} 次点击`'
                        }
                    }]
                }]
            }, {
                id: 'grid',
                class: grid,
                widget: 'grid-treeedit',
                pageSize: 20,
                columns: [{
                    label: '',
                    width: 80
                }, {
                    prop: 'icon',
                    label: '图标',
                    width: 100
                }, {
                    prop: 'name',
                    label: '名称'
                }, {
                    prop: 'module',
                    label: '模块名'
                }, {
                    prop: 'is_show',
                    dictionary: true,
                    label: '是否启用',
                    width: 100
                }],
                buttons:[{
                    value:'编辑',
                    onClick:[{
                        action:'print',
                        value:'$params',
                        description:'编辑数据'
                    }]
                },{
                    value:'删除',
                    type:'danger',
                    onClick:[{
                        action:'print',
                        value:'$params',
                        description:'删除数据'
                    }]
                }],
                flex: true,
                enableCheckboxColumn:true,
                onMounted: [{
                    action: 'service',
                    params: {
                        page_size: '$widget.pageSize'
                    },
                    path: 'system/setting/menu/treeList',
                    actions: [{
                        action: 'set',
                        target: '$widget',
                        field: 'value',
                        value: '$params.data.items'
                    }, {
                        action: 'set',
                        target: '$widget',
                        field: 'total',
                        value: '$params.data.total'
                    }]
                }],
                onRowselect: [{
                    action: 'form-value',
                    target: '$widgets.form',
                    value: '$params.row'
                }],
            }
        ]
    }]
};