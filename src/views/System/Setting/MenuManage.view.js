import {
    center,
    right,
    search,
    grid,
    input,
    button,
    title,
    header,
} from '@/css/VueCss.module.scss' ;

export default {
    title: '菜单管理-Tree',
    order: 3,
    onBeforeMount: [{
        action: 'service',
        path: 'menu/status',
        clearCache: true
    }, {
        action: 'service',
        path: 'menu/attribute',
        clearCache: true
    }],
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            class: header,
            layout:'horizontal',
            items: [{
                    class: button,
                    widget: 'button',
                    value: '新增菜单',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'grid/item/detail/Drawer',
                        params: {
                            value: '`第 ${$view.count ++} 次点击`'
                        }
                    }]
                },
                {
                    class: button,
                    widget: 'button',
                    value: '展开',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'grid/item/detail/Drawer',
                        params: {
                            value: '`第 ${$view.count ++} 次点击`'
                        }
                    }]
                },
                {
                    class: button,
                    widget: 'button',
                    value: '刷新',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'grid/item/detail/Drawer',
                        params: {
                            value: '`第 ${$view.count ++} 次点击`'
                        }
                    }]
                },
                {
                    class: button,
                    widget: 'button',
                    value: '收起',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'grid/item/detail/Drawer',
                        params: {
                            value: '`第 ${$view.count ++} 次点击`'
                        }
                    }]
                }, {
                    id: 'search',
                    class: [
                        search
                    ],
                    selects: [{
                        field: 'is_show',
                        placeholder: '请选择状态'
                    }, {
                        field: 'header',
                        placeholder: '顶部菜单'
                    }],
                    widget: 'search',
                    onMounted: [{ //初始化
                            action: 'service',
                            path: 'menu/status', //字典
                            actions: [{
                                action: 'set',
                                target: '$widget.selects[0]',
                                field: 'options',
                                value: '$params.data'
                            }]
                        },
                        { //初始化
                            action: 'service',
                            path: 'menu/attribute', //字典
                            actions: [{
                                action: 'set',
                                target: '$widget.selects[1]',
                                field: 'options',
                                value: '$params.data'
                            }]
                        }
                    ],
                    onSearch: [{
                        action: 'service',
                        params: {
                            page_size: '$widgets.grid.pageSize',
                            keywords: '$params.keywords',
                            is_show: '$params.is_show',
                            header: '$params.header',
                        },
                        path: 'system/setting/menu/list',
                        actions: [{
                            action: 'set',
                            target: '$widgets.tree',
                            field: 'value',
                            value: '$params.data.items'
                        }, {
                            action: 'set',
                            target: '$widgets.grid',
                            field: 'total',
                            value: '$params.data.total'
                        }]
                    }]
                }]
            },{
                id: 'tree',
                class: grid,
                widget: 'tree-basic',
                pageSize:20,
                columns: [{
                    prop: '_id',
                    label: '菜单ID',
                    width: 80
                }, {
                    prop: 'icon',
                    label: '图标',
                    width: 100
                }, {
                    prop: 'name',
                    label: '名称'
                }, {
                    prop: 'module',
                    label: '模块名'
                }, {
                    prop: 'is_show',
                    label: '是否启用',
                    width: 100
                }],
                flex: true,
                onMounted: [{
                    action: 'service',
                    params: {
                        page_size: '$widget.pageSize'
                    },
                    path: 'system/setting/menu/treeList',
                    actions: [{
                        action: 'set',
                        target: '$widget',
                        field: 'value',
                        value: '$params.data.items'
                    }, {
                        action: 'set',
                        target: '$widget',
                        field: 'total',
                        value: '$params.data.total'
                    }]
                }],
                onRowselect: [{
                    action: 'form-value',
                    target: '$widgets.form',
                    value: '$params'
                }],
            }
        ]
    }, {
        region: 'right',
        class: right,
        span:10,
        items: [{
        //     id: 'title',
        //     title: '内容编辑',
        //     widget: 'public-title'
        // }, {
            id: 'form',
            widget: 'form',
            fields: [{
                itemId: "pid",
                widget: 'form-cascader',
                disabled: true,
                placeholder: '请选择',
                label: '上级菜单',

            }, {
                class: input,
                itemId: 'name',
                placeholder: '请输入名称',
                widget: 'text',
                label: '名称',
            }, {
                class: input,
                itemId: 'icon',
                placeholder: '请设置图标',
                widget: 'text',
                label: '图标',
            }, 
            {
                class: input,
                itemId: 'sort',
                placeholder: '请输入排序',
                widget: 'text',
                label: '排序',
            },
            {
                class: input,
                id: 'module',
                placeholder: '请输入模块名',
                widget: 'text',
                label: '模块名',
            },
            {
                class: input,
                itemId: 'path',
                placeholder: '请输入path',
                widget: 'text',
                label: '路由地址',
            },{
                itemId: 'is_show',
                widget: 'form-radio',
                options: [{
                    value: 1,
                    text: '启用'
                }, {
                    value: 2,
                    text: '禁用'
                }],
                label: '是否启用',
            },
            {
                itemId: 'is_head',
                widget: 'form-radio',
                options: [{
                    value: 1,
                    text: '是'
                }, {
                    value: 2,
                    text: '否'
                }],
                label: '顶部菜单',
            }],
            buttons: [{
                value: '保存',
                onClick: [{
                    action: 'confirm',
                    title: '表单保存',
                    message: '是否保存表单?',
                    confirm: [{
                        action: 'service',
                        path: 'system/setting/menu/save',
                        params: '$widgets.form.value'
                    }, {
                        action: 'message',
                        message: '保存成功',
                        type: 'success'
                    }, {
                        action: 'service',
                        path: 'system/setting/menu/list',
                        actions: [{
                            action: 'set',
                            target: '$widgets.grid',
                            field: 'value',
                            value: '$params.data'
                        }]
                    }]
                }]
            }],
            flex: true,
            onMounted:[{
                action:'service',
                path:'system/setting/menu/treeList',
                actions:[{
                    action:'set',
                    target:'$widget.form-cascader',
                    field:'value',
                    value:'$params.data.items'
                }]
            }],
        }]
    }]
};