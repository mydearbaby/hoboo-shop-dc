import {
    center,
    right,
    grid,
    formInput,
    header,
    button,
    search,
    rightContent
} from '@/css/VueCss.module.scss' ;

export default {
    title: '参数管理',
    order: 6,
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: [
                    search
                ],
                selects: [
                    'state'
                ],
                widget: 'search',
                onSearch: [
                    {
                        action: 'grid-load',
                        target: '$widgets.grid',
                        params: {
                            '...': '$params',
                            keywordsBy: 'name',
                        }
                    }
                ]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick: [
                        {
                            action: 'call',
                            target: '$widgets.form',
                            method: 'reset'
                        }, {
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'clearSelection'
                        }
                    ]
                }, {
                    class: button,
                    widget: 'button',
                    value: '删除',
                    onClick: [{
                        action: 'confirm',
                        title: '删除数据',
                        message: '是否删除勾选数据?',
                        confirm: [{
                            var: 'checkedItemIds',
                            action: 'array-value',
                            target: '$widgets.grid.checkedItems',
                            key: 'id'
                        }, {
                            action: 'print',
                            description: '列表勾选项',
                            value: '$vars.checkedItemIds'
                        }, {
                            action: 'service',
                            path: 'system/setting/parameter/del',
                            params: {
                                id: '$vars.checkedItemIds'
                            }
                        }, {
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }]
                    }]
                }]
            }]
        }, {
            id: 'grid',
            class: grid,
            widget: 'grid',
            service: {
                path: 'system/setting/parameter/list',
                params: {},
            },
            pageSize: 20,
            columns: [
                {
                    prop: 'name',
                    label: '配置名称',
                    minWidth: 150
                }, {
                    prop: 'value',
                    label: '配置数据',
                    minWidth: 100
                }, {
                    prop: 'type',
                    label: '配置类型',
                    minWidth: 120
                }, {
                    prop: 'module',
                    label: '模块名称',
                    minWidth: 150
                }, {
                    prop: 'description',
                    label: '配置描述',
                    minWidth: 150
                }, 'state'
            ],
            buttons: [{
                value: '编辑',
                onClick: [{
                    action: 'print',
                    value: '$params',
                    description: '编辑数据'
                }]
            }, {
                value: '删除',
                type: 'danger',
                onClick: [{
                    action: 'confirm',
                    title: '删除数据',
                    message: '是否删除该条数据?',
                    confirm: [
                        {
                            action: 'service',
                            path: 'system/setting/parameter/del',
                            params: {
                                id: '$params.id'
                            }
                        }, {
                            action: 'call',
                            target: '$widget',
                            method: 'reload'
                        }
                    ]
                }]
            }],
            flex: true,
            enableCheckboxColumn: true,
            onRowselect: [
                {
                    action: 'call',
                    target: '$widgets.form',
                    method: 'setValue',
                    params: '$params'
                }
            ],
        }]
    }, {
        region: 'right',
        class: right,
        items: [{
            id: 'title',
            widget: 'public-titleright',
            title: '编辑修改',
        }, {
            class: rightContent,
            items: [{
                id: 'form',
                widget: 'form',
                fields: [
                    {
                        class: formInput,
                        itemId: 'module',
                        placeholder: '请输入模块名称',
                        widget: 'text',
                        label: '模块名称',
                    }, {
                        class: formInput,
                        itemId: 'name',
                        placeholder: '请输入配置名称',
                        widget: 'text',
                        label: '配置名称',
                    }, {
                        class: formInput,
                        itemId: 'value',
                        placeholder: '请输入配置数据',
                        widget: 'text',
                        label: '配置数据',
                    }, {
                        class: formInput,
                        itemId: 'description',
                        placeholder: '请输入配置描述',
                        widget: 'text',
                        label: '配置描述',
                    }, {
                        class: formInput,
                        itemId: 'state',
                        widget: 'select',
                        label: '配置状态',
                        placeholder: '请选择配置状态',
                        dictionary: 'common/static/state',
                    }
                ],
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [
                            {
                                var: 'value',
                                target: '$widget',
                                action: 'call',
                                method: 'getValue'
                            }, {
                                action: 'service',
                                path: 'system/setting/parameter/save',
                                params: "$vars.value"
                            }, {
                                action: 'call',
                                target: '$widgets.grid',
                                method: 'reload'
                            }
                        ]
                    }]
                }],
                flex: true
            }]
        }]
    }]
};
