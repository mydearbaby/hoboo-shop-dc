import {
    center,
    right,
    grid,
    header,
    button,
    search,
    rightContent,
    formInput
} from '@/css/VueCss.module.scss' ;
export default {
    title: '菜单管理',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search',
                text:{
                    field: 'keywords',
                    placeholder: '请输入名称'
                },
                onSearch: [{
                    action: 'grid-load',
                    target: '$widgets.grid',
                    params: '$params'
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick:[{
                        action:'call',
                        target:'$widgets.form',
                        method:'reset'
                    },{
                        action:'call',
                        target:'$widgets.grid',
                        method:'clearSelection'
                    }]
                },{
                    class: button,
                    widget: 'button',
                    value: '展开',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'grid/item/detail/Drawer',
                        params: {
                            value: '`第 ${$view.count ++} 次点击`'
                        }
                    }]
                },{
                    class: button,
                    widget: 'button',
                    value: '收起',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'grid/item/detail/Drawer',
                        params: {
                            value: '`第 ${$view.count ++} 次点击`'
                        }
                    }]
                },{
                    class: button,
                    widget: 'button',
                    value: '刷新',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'grid/item/detail/Drawer',
                        params: {
                            value: '`第 ${$view.count ++} 次点击`'
                        }
                    }]
                },{
                    class: button,
                    widget: 'button',
                    value: '删除',
                    onClick: [{
                        action: 'dialog-show',
                        path: 'grid/item/detail/Drawer',
                        params: {
                            value: '`第 ${$view.count ++} 次点击`'
                        }
                    }]
                }]
            }]
        },{
            id:'grid',
            class: grid,
            widget:'grid',
            service:'dictionary/routes',
            columns:[{
                label: '',
                width: 80
            }, {
                prop: 'title',
                label: '名称'
             },
             {
                prop:'path',
                label: '路径',
            }],
            buttons:[{
                value:'编辑',
                onClick:[{
                    action:'print',
                    value:'$params',
                    description:'编辑数据'
                }]
            },{
                value:'删除',
                type:'danger',
                onClick:[{
                    action:'confirm',
                    title:'删除数据',
                    message:'是否删除该条数据?',
                    confirm:[{
                        action:'service',
                        path:'system/setting/menu/treeList',
                        params: {
                            id:'$params.id'
                        }
                    },{
                        action:'call',
                        target:'$widget',
                        method:'reload'
                    }]
                }]
            }],
            flex:true,
            enableCheckboxColumn:true,
            onRowselect:[{
                action:'call',
                target:'$widgets.form',
                method:'setValue',
                params:'$params'
            }],
        }]
    },{
        region: 'right',
        class: right,
        items:[{
            id: 'title',
            widget: 'public-titleright',
            title: '编辑修改',
        },{
            class:rightContent,
            items:[{
                id:'form',
                widget:'form',
                fields:[{
                    class: formInput,
                    itemId:'pid',
                    placeholder:'请选择行政区划',
                    // widget: 'form-cascader',
                    widget: 'select',
                    label:'上级菜单',
                    onMounted:[{
                        action:'service',
                        path:'system/setting/user_group/list',
                        actions:[{
                            action:'set',
                            target:'$widget',
                            field:'options',
                            value:'$params.data'
                        }]
                    }]
                },{
                    class: formInput,
                    itemId: 'title',
                    placeholder: '请输入名称',
                    widget: 'text',
                    label: '名称',
                },{
                    class: formInput,
                    itemId: 'icon',
                    placeholder: '请设置图标',
                    widget: 'text',
                    label: '图标',
                },{
                    class: formInput,
                    itemId: 'sort',
                    placeholder: '请输入排序',
                    widget: 'text',
                    label: '排序',
                },{
                    class: formInput,
                    itemId: 'module',
                    placeholder: '请输入模块名',
                    widget: 'text',
                    label: '模块名',
                },{
                    class: formInput,
                    itemId: 'path',
                    placeholder: '请输入path',
                    widget: 'text',
                    label: '路由地址',
                },{
                    class: formInput,
                    itemId: 'state',
                    widget: 'select',
                    label: '状态',
                    placeholder: '请选择状态',
                    dictionary:'common/static/state',
                // },{
                //     class: formInput,
                //     itemId: 'is_head',
                //     widget: 'form-radio',
                //     options: [{
                //         value: 1,
                //         text: '是'
                //     }, {
                //         value: 2,
                //         text: '否'
                //     }],
                //     label: '顶部菜单',
                }],
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            action: 'service',
                            path: 'system/dictionary/config/save',
                            params: "$widget.value",
                        },{
                            action:'call',
                            target:'$widgets.grid',
                            method:'reload'
                        }]
                    }]
                }],
                flex:true
            }]
        }]
    }]
} ;
