import {
    left as three_columns_left,
    center as three_columns_center,
    grid as three_columns_grid,
} from '@/css/layout/ThreeColumns.module.scss' ;

import {
    button,
    search,
    contact_right,
    right,
    header,
    formTitle,
    contact_header,
    itemInfo,
} from '@/css/VueCss.module.scss' ;
export default {
    title: '用户组管理',
    layout:'three-columns',
    items:[{
        region:'left',
        layout: 'vertical',
        class: contact_right,
        items:[{
            class:three_columns_left,
            layout: 'vertical',
            flex: true,
            items:[{
                id: 'search',
                class: search,
                widget: 'search',
                text:{
                    field: 'keywords',
                    placeholder: '请输入用户组名称'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params:'$params'
                }]
            },{
                layout: 'horizontal',
                class: contact_header,
                items:[{
                    class: formTitle,
                    id: 'title',
                    widget: 'public-title',
                    title: '用户组名称',
                },{
                    class: button,
                    widget: 'buttonicon',
                    type: 'text',
                    layout: 'horizontal',
                    onClick: [{
                        action:'dialog-show',
                        path: 'System/SettingManage/UserGroup/AddUserGroup',
                        params: {
                            title:"'新增用户组'"
                        },
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }],
                        cancelActions: [{
                            action: 'print',
                            description: '取消对话框'
                        }]
                    }]
                }]
            },{
                id: 'grid',
                widget: 'grid',
                pageSize:20,
                service: {
                    path: 'system/dictionary/config/list',
                    params: {
                        module: 'system_module',
                        name: 'user_group',
                    },
                },
                columns: [{
                    prop: 'value',
                }],
                header:false,
                enableIndexColumn: false,
                operationButtonCount: 0,
                operationWidth: 30,
                flex:true,
                buttons: [{
                    value: '编辑',
                    onClick:[{
                        action:'dialog-show',
                        path: 'System/SettingManage/UserGroup/AddUserGroup',
                        params: {
                            title:"'编辑用户组信息'",
                            form: '$params',
                        },
                        applyActions: [{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }],
                        cancelActions:[{
                            action:'print',
                            description:'取消对话框'
                        }]
                    }]
                },{
                    value: '删除',
                    type:'danger',
                    onClick:[{
                        action:'confirm',
                        title:'删除数据',
                        message:'是否删除该条数据?',
                        confirm:[{
                            action:'service',
                            path:'system/dictionary/config/remove',
                            params: {
                                id: '$params.id'
                            }
                        },{
                            action: 'call',
                            target: '$widget',
                            method: 'reload'
                        }]
                    }]
                }],
                onRowselect: [{
                    action:'print',
                    value:'$params'
                },{
                    action: 'grid-load',
                    target: '$widgets.gridUser',
                    params: {
                        group_uuid: "$params.uuid"
                    }
                }]
            }]
        }]
    },{
        region:'center',
        items:[{
            flex:1,
            class:[three_columns_center, right],
            layout:'tab',
            items:[{
                title:'用户',
                layout:'vertical',
                items:[{
                    items:[{
                        class: itemInfo,
                        items:[{
                            layout: 'vertical',
                            items: [{
                                items: [{
                                    class: search,
                                    widget: 'search',
                                    text:{
                                        field: 'keywords',
                                        placeholder: '请输入姓名'
                                    },
                                    onSearch: [{
                                        action: 'grid-load',
                                        target: '$widgets.gridUser',
                                        params:{
                                            // role_id: "$widgets.grid.selectedItem.id",
                                            page_size: '$widgets.gridUser.pageSize',
                                            keywords: '$params.keywords',
                                        }
                                    }]
                                }, {
                                    class: header,
                                    layout: 'horizontal',
                                    items: [{
                                        class: button,
                                        widget: 'button',
                                        value: '新增',
                                        onClick: [{
                                            action:'dialog-show',
                                            path: 'System/SettingManage/UserGroup/UserEdit',
                                            params: {
                                                title:"'新增用户'",
                                                form: {
                                                    group_uuid:'$widgets.grid.selectedItem.uuid'
                                                }
                                            },
                                            applyActions: [{
                                                action: 'call',
                                                target: '$widgets.gridUser',
                                                method: 'clearSelection'
                                            }, {
                                                action: 'call',
                                                target: '$widgets.gridUser',
                                                method: 'reload'
                                            }],
                                            cancelActions:[{
                                                action:'print',
                                                description:'取消对话框'
                                            }]
                                        }]
                                    },{
                                        class: button,
                                        widget: 'button',
                                        value: '删除',
                                        onClick:[{
                                            action:'confirm',
                                            title:'删除数据',
                                            message:'是否删除勾选数据?',
                                            confirm:[{
                                                var:'checkedItemIds',
                                                action:'array-value',
                                                target:'$widgets.gridUser.checkedItems',
                                                key:'id'
                                            },{
                                                action:'print',
                                                description:'列表勾选项',
                                                value:'$vars.checkedItemIds'
                                            },{
                                                action:'service',
                                                path: 'system/usergroup/user/remove',
                                                params:{
                                                    id:'$vars.checkedItemIds'
                                                }
                                            },{
                                                action: 'call',
                                                target: '$widgets.gridUser',
                                                method: 'reload'
                                            }]
                                        }]
                                    }]
                                }]
                            }]
                        }]
                    }]
                },{
                    class: three_columns_grid,
                    id: 'gridUser',
                    widget: 'grid',
                    service: 'system/usergroup/user/list',
                    pageSize: 20,
                    columns: [{
                        prop: 'account',
                        label: '登陆账户',
                        minWidth: 120
                    }, {
                        prop: 'real_name',
                        label: '真实姓名',
                        minWidth: 150
                    // }, {
                    //     prop: 'roles',
                    //     label: '权限',
                    //     minWidth: 150
                    }, 'state'],
                    flex: true,
                    operationWidth:155,
                    operationButtonCount:3,
                    enableCheckboxColumn: true,
                    buttons: [{
                        value: '编辑',
                        onClick:[{
                            action:'dialog-show',
                            path: 'System/SettingManage/UserGroup/UserEdit',
                            params: {
                                title:"'编辑用户信息'",
                                form: '$params',
                            },
                            applyActions: [{
                                action: 'call',
                                target: '$widgets.gridUser',
                                method: 'reload'
                            }],
                            cancelActions:[{
                                action:'print',
                                description:'取消对话框'
                            }]
                        }]
                    },{
                        value: '删除',
                        onClick:[{
                            action:'confirm',
                            title:'删除数据',
                            message:'是否删除该条数据?',
                            confirm:[{
                                action:'service',
                                path: 'system/usergroup/user/remove',
                                params: {
                                    id: '$params.id'
                                }
                            },{
                                action: 'call',
                                target: '$widget',
                                method: 'reload'
                            }]
                        }]
                    },{
                        value: '重置密码',
                        type: 'danger',
                        onClick: [{
                            action: 'dialog-show',
                            path: 'System/SettingManage/UserGroup/UserPassword',
                            params: '$params',
                            applyActions: [{
                                action: 'call',
                                target: '$widgets.gridUser',
                                method: 'reload'
                            }],
                            cancelActions: [{
                                action: 'print',
                                description: '取消对话框'
                            }]
                        }]
                    }],
                }]
            }]
        }]
    }]
}
