import {
    center,
    right,
    grid,
    header,
    button,
    search,
    rightContent,
    formInput
} from '@/css/VueCss.module.scss' ;
export default {
    title: '组织管理',
    items: [{
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search',
                text:{
                    field: 'keywords',
                    placeholder: '请输入组织名称'
                },
                onSearch: [{
                    action: 'grid-load',
                    target: '$widgets.grid',
                    params: '$params'
                }]
            }, {
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '新增',
                    onClick:[{
                        action:'call',
                        target:'$widgets.form',
                        method:'reset'
                    },{
                        action:'call',
                        target:'$widgets.grid',
                        method:'clearSelection'
                    }]
                }, {
                    class: button,
                    widget: 'button',
                    value: '删除',
                    onClick:[{
                        action:'confirm',
                        title:'删除数据',
                        message:'是否删除勾选数据?',
                        confirm:[{
                            var:'checkedItemIds',
                            action:'array-value',
                            target:'$widgets.grid.checkedItems',
                            key:'id'
                        },{
                            action:'print',
                            description:'列表勾选项',
                            value:'$vars.checkedItemIds'
                        },{
                            action:'service',
                            path:'system/organization/mechanism/list/allremove',
                            params:{
                                id:'$vars.checkedItemIds'
                            }
                        },{
                            action: 'call',
                            target: '$widgets.grid',
                            method: 'reload'
                        }]
                    }]
                }]
            }]
        },{
            id:'grid',
            class: grid,
            widget:'grid',
            service: 'system/organization/mechanism/list/list',
            pageSize:20,
            columns:[{
                prop:'name',
                label:'组织名称',
            },{
                prop:'abbreviation',
                label:'组织简称'
            },{
                prop:'profile',
                label:'组织简介'
            },'state'],
            buttons:[{
                value:'编辑',
                onClick:[{
                    action:'print',
                    value:'$params',
                    description:'编辑数据'
                }]
            },{
                value:'删除',
                type:'danger',
                onClick:[{
                    action:'confirm',
                    title:'删除数据',
                    message:'是否删除该条数据?',
                    confirm:[{
                        action:'service',
                        path:'system/organization/mechanism/list/remove',
                        params: {
                            id:'$params.id'
                        }
                    },{
                        action:'call',
                        target:'$widget',
                        method:'reload'
                    }]
                }]
            }],
            flex:true,
            enableCheckboxColumn:true,
            onRowselect:[{
                action:'call',
                target:'$widgets.form',
                method:'setValue',
                params:'$params'
            }],
        }]
    },{
        region: 'right',
        class: right,
        items:[{
            id: 'title',
            widget: 'public-titleright',
            title: '编辑修改',
        },{
            class:rightContent,
            items:[{
                id:'form',
                widget:'form',
                fields:[{
                    class: formInput,
                    itemId: 'name',
                    placeholder: '请输入组织名称',
                    widget: 'text',
                    label: '组织名称',
                }, {
                    class: formInput,
                    itemId: 'abbreviation',
                    placeholder: '请输入组织简称',
                    widget: 'text',
                    label: '组织简称',
                }, {
                    class: formInput,
                    itemId: 'profile',
                    placeholder: '请输入组织简介',
                    widget: 'text',
                    label: '组织简介',
                }, {
                    class: formInput,
                    itemId: 'state',
                    widget: 'select',
                    label: '状态',
                    placeholder: '请选择状态',
                    dictionary:'common/static/state',
                }],
                buttons: [{
                    value: '保存',
                    type: 'primary',
                    onClick: [{
                        action: 'confirm',
                        title: '表单保存',
                        message: '是否保存表单?',
                        confirm: [{
                            action: 'service',
                            path: 'system/organization/mechanism/list/save',
                            params: "$widget.value",
                        },{
                            action:'call',
                            target:'$widgets.grid',
                            method:'reload'
                        }]
                    }]
                }],
                flex:true
            }]
        }]
    }]
} ;
