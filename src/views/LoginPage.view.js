import {
    wrapper,
    loginbox,
    formTitle,
    formInput,
    captchaInput,
    captchaImage,
    loginBtn
} from './Login.module.scss';

export default {
    title: '登陆',
    layout: 'fit',
    items: [
        {
            region: 'center',
            class: wrapper,
            items: [
                {
                    id: 'form',
                    widget: 'form',
                    class: loginbox,
                    fields: [
                        {
                            class: formTitle,
                            widget: 'form-text',
                            value: '欢迎登陆',
                        }, {
                            itemId: 'type',
                            value: 'admin_login',
                        }, {
                            class: formInput,
                            itemId: 'account',
                            placeholder: '用户名',
                            widget: 'input'
                        }, {
                            class: formInput,
                            itemId: 'password',
                            placeholder: '密码',
                            widget: 'password'
                        }, {
                            id: 'key',
                            class: formInput,
                            itemId: 'code',
                            placeholder: '验证码',
                            widget: 'validInput',
                            service: 'login/captcha'
                        }
                    ],
                    buttons: [
                        {
                            class: loginBtn,
                            value: '登陆',
                            type: 'primary',
                            size: 'large',
                            onClick: [
                                {
                                    var: 'values',
                                    action: 'call',
                                    target: '$widgets.form',
                                    method: 'getValue'
                                }, {
                                    action: 'print',
                                    value: '$vars.values',
                                }, {
                                    action: 'print',
                                    value: '$widgets',
                                }, {
                                    var: 'loginedValues',
                                    action: 'service',
                                    path: 'login/login',
                                    params: {
                                        '...': '$vars.values',
                                        key: '$vars.values.code.key',
                                        code: '$vars.values.code.code',
                                    },
                                    failure: [{
                                        action: 'call',
                                        target: '$widgets.key',
                                        method: 'refresh'
                                    }]
                                }, {
                                    action: 'login',
                                    data: '$vars.loginedValues'
                                }
                            ],
                        },
                    ]
                }
            ]
        }
    ]

};
