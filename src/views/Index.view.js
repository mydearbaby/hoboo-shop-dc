import {
    search,
    grid
} from './Index.module.scss' ;

export default {
    title:'首页',
    items:[{
        region:'center',
        items:[{
            class:search,
            // widget:'search',
            widget: 'search-datesearch',
            text: {
                field:'keywords',
                placeholder:'请输入搜索菜单名称'
            },
            times: [{
                label: '今日',
                value: 'today'
            }, {
                label: '昨日',
                value: 'yesterday'
            },{
                label: '近7日',
                value: 'lately7'
            },{
                label: '近30日',
                value: 'lately30'
            }],
            rangeTimes: true,
            selects:[
                'state'
            ],
            onSearch:[{
                action:'grid-load',
                target:'$widgets.grid',
                params:'$params'
            }]
        },{
            widget:'button',
            value:'创建菜单',
            onClick:[{
                action:'call',
                target:'$widgets.form',
                method:'reset'
            },{
                action:'call',
                target:'$widgets.grid',
                method:'clearSelection'
            }]
        },{
            id:'grid',
            class:grid,
            widget:'grid',
            service:'manager/menu/list',
            pageSize:10,
            operationTitle:'',
            columns:[{
                prop:'name',
                label:'名称',
                width:100
            },'state'],
            onRowselect:[{
                action:'call',
                target:'$widgets.form',
                method:'set',
                params:'$params'
            }],
            buttons:[{
                value:'修改'
            },{
                value:'删除',
                onClick:[{
                    action: 'confirm',
                    title: '删除菜单',
                    message: '是否删除菜单?',
                    confirm: [{
                        action: 'service',
                        path: 'manager/menu/item/remove',
                        params: {
                            // id:'$params.id'
                            uuid:'$params.uuid'
                        }
                    },{
                        action:'call',
                        target:'$widget',
                        method:'reload'
                    }]
                }]
            }],
            flex:true
        }]
    },{
        region:'right',
        items:[{
            widget:'form',
            id:'form',
            layout:{
                type:'gridform',
                column:1
            },
            fields:[{
                itemId:'name',
                widget:'text',
                required:true,

                label:'名称'
            },{
                itemId:'state',
                widget:'select',
                dictionary:'dictionary/state',
                placeholder:'请选择状态',
                label:'状态',
                required:true
            }],
            buttons:[{
                value:'确定',
                onClick:[{
                    var:'value',
                    action:'call',
                    target:'$widget',
                    method:'get'
                } , {
                    action: 'service',
                    path: 'manager/menu/item/save',
                    params:"$vars.value"
                },{
                    action:'call',
                    target:'$widgets.grid',
                    method:'reload'
                }]
            }]
        }]
    }]
} ;