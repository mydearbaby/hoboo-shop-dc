import {
    center,
    grid,
    search,
    header,
    button
} from '@/css/VueCss.module.scss';
export default {
    // title: '回收站',
    items: [{ 
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search-datesearch',
                times: [{
                    label: '今日',
                    value: 'today'
                }, {
                    label: '昨日',
                    value: 'yesterday'
                },{
                    label: '近7日',
                    value: 'lately7'
                },{
                    label: '近30日',
                    value: 'lately30'
                }],
                rangeTimes: true,
                text:{
                    field: 'keywords',
                    placeholder: '请输入订单号'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params: {
                        '...':'$params',
                        keywordsBy: '"order_id"',
                    }
                }]
            },{
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '手动批量发货',
                }, {
                    class: button,
                    widget: 'button',
                    value: '导出发货单',
                }, {
                    class: button,
                    widget: 'button',
                    value: '导出订单',
                }, {
                    class: button,
                    widget: 'button',
                    value: '订单核销',
                }]
            }]
        },{
            id:'grid',
            class: grid,
            widget:'grid',
            service: 'shop/order/list/delete_list',
            pageSize:20,
            columns:[{
                prop:'order_id',
                label:'订单号',
            },{
                prop:'name',
                label:'订单类型',
            },{
                prop:'real_name',
                label:'用户信息',
            },{
                prop:'cart_id',
                label:'商品信息',
            },{
                prop:'pay_price',
                label:'实际支付',
            },{
                prop:'pay_time',
                label:'支付时间',
            },{
                prop:'paid',
                label:'支付状态',
            }],
            buttons:[{
                value:'详情',
                onClick:[{
                    action:'dialog-show',
                    path:'Shop/Order/List/Detail',
                    params: '$params',
                }]
            }],
            flex:true,
            operationWidth:55,
        }]
    }]
} ;
