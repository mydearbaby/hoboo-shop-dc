import {
    center,
    grid,
    search,
    header,
    button
} from '@/css/VueCss.module.scss';
export default {
    // title: '待付款',
    items: [{ 
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search-datesearch',
                times: [{
                    label: '今日',
                    value: 'today'
                }, {
                    label: '昨日',
                    value: 'yesterday'
                },{
                    label: '近7日',
                    value: 'lately7'
                },{
                    label: '近30日',
                    value: 'lately30'
                }],
                rangeTimes: true,
                text:{
                    field: 'keywords',
                    placeholder: '请输入订单号'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params: {
                        '...':'$params',
                        paid:0,
                        pink_id:0,
                        combination_id:0,
                        seckill_id:0,
                        keywordsBy: '"order_id"',
                    }
                }]
            },{
                class: header,
                layout: 'horizontal',
                items: [{
                    class: button,
                    widget: 'button',
                    value: '手动批量发货',
                }, {
                    class: button,
                    widget: 'button',
                    value: '导出发货单',
                }, {
                    class: button,
                    widget: 'button',
                    value: '导出订单',
                }, {
                    class: button,
                    widget: 'button',
                    value: '订单核销',
                }]
            }]
        },{
            id:'grid',
            class: grid,
            widget:'grid',
            service: {
                path: 'shop/order/list/list',
                params: {
                    paid:0,
                    pink_id:0,
                    combination_id:0,
                    seckill_id:0
                },
            },
            pageSize:20,
            columns:[{
                prop:'order_id',
                label:'订单号',
            },{
                prop:'real_name',
                label:'用户姓名',
            },{
                prop:'user_phone',
                label:'用户电话',
            },{
                prop:'pay_price',
                label:'实际支付',
            },{
                prop:'pay_type',
                label:'支付方式',
                dictionary: 'shop/common/static/pay_type',
            },{
                prop:'pay_time',
                label:'支付时间',
            }],
            buttons:[{
                value:'立即支付',
                onClick:[{
                    action:'dialog-show',
                    path:'Shop/Order/List/Pay',
                    params: '$params',
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            },{
                value:'详情',
                onClick:[{
                    action:'dialog-show',
                    path:'Shop/Order/List/Detail',
                    params: '$params',
                }]
            }],
            flex:true,
            operationWidth:120,
            // operationButtonCount:3,
            // enableCheckboxColumn:true,
        }]
    }]
} ;
