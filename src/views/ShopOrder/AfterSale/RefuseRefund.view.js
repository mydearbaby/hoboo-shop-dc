import {
    center,
    grid,
    search,
} from '@/css/VueCss.module.scss';
export default {
    // title: '拒绝退款',
    items: [{ 
        region: 'center',
        class: center,
        layout: 'vertical',
        items: [{
            items: [{
                id: 'search',
                class: search,
                widget: 'search-datesearch',
                times: [{
                    label: '今日',
                    value: 'today'
                }, {
                    label: '昨日',
                    value: 'yesterday'
                },{
                    label: '近7日',
                    value: 'lately7'
                },{
                    label: '近30日',
                    value: 'lately30'
                }],
                rangeTimes: true,
                text:{
                    field: 'keywords',
                    placeholder: '请输入订单号'
                },
                onSearch: [{
                    action:'grid-load',
                    target:'$widgets.grid',
                    params: {
                        '...':'$params',
                        keywordsBy: '"order_id"',
                    }
                }]
            }]
        },{
            id:'grid',
            class: grid,
            widget:'grid',
            service: {
                path:'shop/order/list/list',
                params:{
                    state:-1,
                    refund_type:1,
                    refund_status:1
                }
            },
            pageSize:20,
            columns:[{
                prop:'order_id',
                label:'订单号',
            },{
                prop:'real_name',
                label:'用户姓名',
            },{
                prop:'user_phone',
                label:'用户电话',
            },{
                prop:'pay_price',
                label:'实际支付',
            },{
                prop:'refund_reason_time',
                label:'退款时间',
            },{
                prop:'refund_reason',
                label:'不退款原因',
            }],
            buttons:[{
                value:'立即退款',
                onClick:[{
                    action:'dialog-show',
                    path:'Shop/Order/AfterSale/Refund',
                    params: '$params',
                    applyActions: [{
                        action: 'call',
                        target: '$widgets.grid',
                        method: 'reload'
                    }],
                    cancelActions:[{
                        action:'print',
                        description:'取消对话框'
                    }]
                }]
            },{
                value:'详情',
                onClick:[{
                    action:'dialog-show',
                    path:'Shop/Order/AfterSale/Detail',
                    params: '$params',
                }]
            }],
            flex:true,
            operationWidth:120,
        }]
    }]
} ;
