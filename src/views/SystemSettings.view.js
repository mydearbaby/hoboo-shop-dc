import {search, grid, searchWidth,css} from "@/views/SystemSettings.module.scss";

/**
 * @desc    页面配置
 * @type    {any}
 * @param   {String}    title           页面名称，作用于导航菜单显示
 * @param   {Number}    order           排序，生序（数值越小越靠前）
 * @param   {Object}    onBeforeMount   前置字典数据
 * @param   {Object}    items           页面布局组件
 */
export default {
    title: '系统设置-JS',
   order: 100,
   count:1,
    /**
     * @desc    前置字典数据规则方法配置
     * @type {Object}
     */
    onBeforeMount: [
        /**
         * @desc    单个字典数据定义规则
         * @type    {Object}
         * @param   {String}  action      字典名称
         * @param   {String}  path        字典数据相对路径（目录+文件名）
         * @param   {Boolean} clearCache  是否清除缓存
         */
    ],
    /**
     * @desc    页面布局
     * @type    {Object}
     */
    items: [
        /**
         * @desc    组件（页面布局）
         * @type    {Object}
         * @param   {String}    region  位置定义，可选值：left, center, right
         */
        {
            region: 'center',
            items: [
                /**
                 * @desc    搜索组件
                 * @type    {Object}
                 * @param   {String}    id          组件命名（用于页面内数据绑定）
                 * @param      {String}    class       调用样式定义名称
                 * @param   {String}    widget      组件名称（'@src/widgets/'目录下），调用时组件名称全小写，以'-'间隔大些字母
                 * @param   {String}    placeholder (可选) 占位提示文字
                 */
                {
                    id: 'search',
                    class: searchWidth,
                    widget: 'search',
                    placeholder: '请输入搜索名称',
                    onSearch: [
                        /**
                         * @desc    搜索事件
                         * @type    {Object}
                         * @param   {String}    action  ？事件类型
                         * @param   {String}    path    事件请求路径，'@src/service/'目录下
                         * @param   {Object}    params  事件携带参数
                         * @param   {Object}    actions 页面内数据绑定操作
                         */
                        {
                            action: 'service',
                            path: 'user/group',
                            params: {
                                page_size: '$widgets.grid.pageSize',
                                keywords: '$params.value'
                            },
                            /**
                             * @desc    页面内数据绑定
                             * @param   {String}    action  set = 数据绑定; ？还有其他操作类型？
                             * @param   {String}    target  数据操作目标组件（对应组件中id定义值）
                             * @param   {String}    field   数据操作目标字段
                             * @param   {String}    value   搜索回调数据中对应数据
                             */
                            actions: [{
                                action: 'set',
                                target: '$widgets.grid',
                                field: 'value',
                                value: '$params.data.items'
                            }, {
                                action: 'set',
                                target: '$widgets.grid',
                                field: 'total',
                                value: '$params.data.total'
                            }]
                        }
                    ]
                },
                /**
                 * @desc    数据列表
                 * @type    {Object}
                 * @param   {String}    id          组件命名（用于页面内数据绑定）
                 * @param   {String}    class       调用样式定义名称
                 * @param   {String}    widget      组件名称（'@src/widgets/'目录下），调用时组件名称全小写，以'-'间隔大些字母
                 * @param   {Boolean}   flex        是否用使用 flex 布局
                 * @param   {Number}    pageSize    页面数据列表大小（每页数量）
                 * @param   {Object}    columns     数据列表列属性定义
                 */
                {
                    id: 'grid',
                    class: grid,
                    widget: 'grid',
                    flex: true,
                    pageSize: 15,
                    /**
                     * @desc    数据列表列属性定义
                     * @type    {Object}
                     */
                    columns: [
                        /**
                         * @desc   列属性，定义规则
                         * @type    {Object}
                         * @param   {String}    label       列属性，显示名称
                         * @param   {String}    prop        （可选）列属性，绑定属性
                         * @param   {String}    width       （可选）列属性，宽度定义（不定义宽度时自适应显示）
                         * @param   {Boolean}   dictionary  （可选）是否为字典属性
                         */
                        {
                            label: '编号',
                            prop: '_id',
                            width: 80
                        }, {
                            label: '部门名称',
                            prop: 'group_name',
                        }, {
                            label: '负责人',
                            prop: 'director',
                            width: 220
                        }, {
                            label: '联系方式',
                            prop: 'mobile',
                            width: 220
                        }, {
                            label: '操作',
                            width: 220
                        }
                    ],
                    /**
                     *  @desc   页面方法定义
                     * @type    {Object}
                     */
                    onPage: [
                        /**
                         * @desc    方法定义规则
                         * @type    {Object}
                         * @param   {String}    action  ？事件类型
                         * @param   {String}    path    事件请求路径，'@src/service/'目录下
                         * @param   {Object}    params  事件携带参数
                         * @param   {Object}    actions 页面内数据绑定操作
                         */
                        {
                            action: 'service',
                            params: {
                                page_size: '$widget.pageSize',
                                page_no: '$params.page',
                                keywords: '$widgets.search.value'
                            },
                            path: 'user/group',
                            actions: [{
                                action: 'set',
                                target: '$widget',
                                field: 'value',
                                value: '$params.data.items'
                            }, {
                                action: 'set',
                                target: '$widget',
                                field: 'total',
                                value: '$params.data.total'
                            }]
                        }
                    ],
                    /**
                     * @desc    定义页面钩子函数
                     * @type    {Object}
                     */
                    onMounted: [
                        /**
                         * @desc    钩子函数定义
                         * @type    {Object}
                         * @param   {String}    action  ？事件类型
                         * @param   {String}    path    事件请求路径，'@src/service/{$path}'目录下
                         * @param   {Object}    params  事件携带参数
                         * @param   {Object}    actions 页面内数据绑定操作
                         */
                        {
                            action: 'service',
                            path: 'user/group',
                            params: {
                                page_size: '$widget.pageSize'
                            },
                            actions: [
                                {
                                    action: 'set',
                                    target: '$widget',
                                    field: 'value',
                                    value: '$params.data.items'
                                }, {
                                    action: 'set',
                                    target: '$widget',
                                    field: 'total',
                                    value: '$params.data.total'
                                }
                            ]
                        }
                    ],
                }
            ]
        }
    ],
};
